// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { Component, createRef } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  SafeAreaView,
  FlatList,
  Text,
  ScrollView,
  Modal,
  Image,
  TouchableHighlight,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';


import { FONTS, images } from '../../assets/styles/theme';
import AsyncStorage from '@react-native-community/async-storage';
import ApiConstants from '../../services/APIConstants';
import Loader from '../Components/Loader';
import NetInfo from "@react-native-community/netinfo";
import { Root, Toast } from 'react-native-popup-confirm-toast'
import { SIZES, COLORS } from '../../assets/styles/theme';
import font from '../styles/font';






export default class settings extends Component {
  constructor(props) {

    super(props);
    this.state = {
      modalVisible: false,
      loader_visible: false,
      description: ""
    }
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }

  componentDidMount = () => {
    this.getData()
  }

  getData = async () => {
    
    var token = await AsyncStorage.getItem("user_id")

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "privacy-and-policy", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
            this.setState({ loader_visible: false })
            console.log("yyyy", responseJson)
            let status = responseJson.status

            if (status == "success") {

              let value = responseJson.value[0].description
              this.setState({ description: value })

            }



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",
          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })

  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

        </Root>
        <View style={[styles.rowgroup, styles.item, styles.centercontent]}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={{ position: "absolute", left: 5, height: 40, width: 40, justifyContent: 'center' }}>
            <Image source={images.prevarrow_icon}
              style={{
                resizeMode: "contain", height: 15,
                width: 9
              }}
            />

          </TouchableOpacity>
          <View style={{ position: "relative", justifyContent: "center" }}>
            <Text style={[font.bold, font.textblue, font.sizeLarge]}>Privacy Policy</Text>
          </View>



        </View>
        <ScrollView contentContainerStyle={{ paddingBottom: 30 }}>
          <View style={{ flex: 1, }}>


            <View>
              <Text style={[font.regular, font.graycolor, font.sizeRegular, styles.bottomspace, styles.marginspace]}>
                {this.state.description}
              </Text>

            </View>
          </View>
        </ScrollView>

        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  marginspace: {
    marginBottom: 0,
  },
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 5,
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 10,
    marginHorizontal: 20
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 81,
    width: 81,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: -7,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 20,
    marginTop: 0,
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
