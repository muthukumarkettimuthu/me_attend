// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  Text,
  ScrollView,
  Modal,
  Image,
  TouchableOpacity,
} from 'react-native';

import ApiConstants from '../../services/APIConstants';

import AsyncStorage from '@react-native-community/async-storage';

import font from '../styles/font';
import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';

import NetInfo from "@react-native-community/netinfo";


export default class settings extends Component {
  constructor(props) {

    super(props);
    this.state = {
      modalVisible: false,
      dialog_visible: false,
      logout_dialog_visible: false,
      dialog_message: '',
      promoter_label: ""
    }
  }

  componentDidMount = async () => {
    var promoter_label = await AsyncStorage.getItem("promoter_label")
    if (promoter_label != null) {
      this.setState({ promoter_label: promoter_label })
    } else {
      this.setState({ promoter_label: "Be Promoter" })
    }

    this.getProfile()

  }


  getProfile = async () => {
    
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {


           
            console.debug("pp", responseJson)
            let is_promoter = responseJson.data.is_promoter
            if (is_promoter == 1) {
              this.setState({ promoter_label: "Undo Promoters role" })
              AsyncStorage.setItem("promoter_label", "Undo Promoters role")
            } else {
              this.setState({ promoter_label: "Be Promoter" })
              AsyncStorage.setItem("promoter_label", "Be Promote")
            }
           
          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  changePromoter = async () => {
    this.setState({ dialog_visible: false })
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "promoter-change", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("country", responseJson)
        const status = responseJson.status
        if (status == "success") {
          let is_promoter = responseJson.is_promoter
          if (is_promoter == 1) {
            this.setState({ promoter_label: "Undo Promoters role" })
            AsyncStorage.setItem("promoter_label", "Undo Promoters role")
          } else {
            this.setState({ promoter_label: "Be Promoter" })
            AsyncStorage.setItem("promoter_label", "Be Promote")
          }


        } else {

        }
      })
  }

  openDialog = () => {
    this.setState({ dialog_visible: true });
  }

  renderItem = ({ item }) => {
   
  }
  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }
  doLogout =()=>{
    this.props.navigation.reset({
      index: 0,
      routes: [
        {
          name: 'LoginScreen',
          
        },
      ],
    })
    AsyncStorage.clear()
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1}}>
        <View style={{height:SIZES.height,backgroundColor:'white'}}>

          <View style={[styles.rowgroup, styles.item, styles.centercontent]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{ position: "absolute", left: 5, top: 15, width: 40, height: 40 }}>
              <Image source={images.prevarrow_icon}
                style={{ resizeMode: "contain",height:15,width:9 }}
              />

            </TouchableOpacity>
            <View style={{ position: "relative", justifyContent: "center" }}>
              <Text style={[font.bold, font.textblue, font.sizeLarge]}>Settings</Text>
            </View>



          </View>
          
           <View>
           <View style={{paddingTop:10}}>
        <View style={[styles.rowgroup, styles.button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center' }} onPress={() => this.props.navigation.navigate('accountinfo')}>
           
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Account Information</Text>
           
          </TouchableOpacity>
        </View>
        <View style={[styles.rowgroup, styles.button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center' }} onPress={this.openDialog}>
            
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>{this.state.promoter_label}</Text>
           
          </TouchableOpacity>
        </View>
        <View style={[styles.rowgroup, styles.button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center' }} onPress={() => this.props.navigation.navigate('notification')}>
           
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Notification Settings</Text>
           
          </TouchableOpacity>
        </View>
        <View style={[styles.rowgroup, styles.button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center'}} onPress={() => this.props.navigation.navigate('contact')}>
          
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Contact Us</Text>
           
          </TouchableOpacity>
        </View>
        <View style={[styles.rowgroup, styles.logout_button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center'}} 
          onPress={() => this.props.navigation.navigate('privacypolicy')}>
            
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Privacy Policy</Text>
           
          </TouchableOpacity>
        </View>
        <View style={[styles.rowgroup, styles.button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height:50,paddingLeft:16,justifyContent:'center' }} onPress={() => this.props.navigation.navigate('termscondition')}>
            
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Terms & Conditions</Text>
            
          </TouchableOpacity>
          
        </View>
        <View style={[styles.rowgroup, styles.logout_button_item, styles.shadowbox]}>
          <TouchableOpacity style={{ width: "100%", height: 50,paddingLeft:16,justifyContent:'center' }} 
          onPress={() => {
            // this.props.navigation.navigate('LoginScreen')
              this.setState({logout_dialog_visible:true})
            }}>
            
              <Text style={[font.sizeMedium, font.textblue, font.bold]}>Logout</Text>
           
          </TouchableOpacity>
          
        </View>
      </View>
           </View>
         

        </View>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 150,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',
                  justifyContent: 'center',
                 
                 
                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20,paddingTop:40, textAlign: 'center', ...FONTS.regularsizeRegular }}>
                Do you want to swich it ?
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent:'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={this.changePromoter}
                  >
                    <Text style={{
                      marginTop: 10,
                      padding: 10,
                      textAlign: 'justify', ...FONTS.regularsizeRegular, color: COLORS.textblue.color
                    }}>
                      Yes
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.setState({ dialog_visible: false })}
                  >
                    <Text style={{
                      marginTop: 10,
                      padding: 10,
                      marginLeft:30,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.graycolor.color
                    }}>
                      No
                    </Text>
                  </TouchableOpacity>


                </View>


              </ScrollView>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 10,
                  width: 16,
                  height: 16,
                  
                 
                }}
                onPress={() => this.setState({ dialog_visible: false })}
              >
                <Image source={images.cancel_icon} style={{
                  width: 15,
                  height: 15,
                  tintColor: COLORS.textblue,
                  alignSelf: 'flex-end',
                 
                }} />
              </TouchableOpacity>
            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.logout_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ logout_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems:'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 150,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              

                <Text style={{ marginBottom: 20,paddingTop:40,  ...FONTS.regularsizeRegular }}>Do you want to log out?
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent:'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={this.doLogout}
                  >
                    <Text style={{
                      marginTop: 10,
                      padding: 10,
                      textAlign: 'justify', ...FONTS.regularsizeRegular, color: COLORS.textblue.color
                    }}>
                      Yes
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.setState({ logout_dialog_visible: false })}
                  >
                    <Text style={{
                      marginTop: 10,
                      padding: 10,
                      marginLeft:30,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.graycolor.color
                    }}>
                      No
                    </Text>
                  </TouchableOpacity>


                </View>


              
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 10,
                  width: 16,
                  height: 16,
                }}
                onPress={() => this.setState({ logout_dialog_visible: false })}
              >
                <Image source={images.cancel_icon} style={{
                  width: 15,
                  height: 15,
                  tintColor: COLORS.textblue,
                  alignSelf: 'flex-end',
                  
                }} />
              </TouchableOpacity>
            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    
    shadowOffset: { width: 1, height: 1 },
   
    shadowRadius: 8,
    elevation: 24,
    shadowRadius: 5,
    shadowOpacity: .08,
    shadowColor: "#444444"
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 81,
    width: 81,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: -7,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 20,
    marginTop: 0,
    
    
  },
  button_item: {
    backgroundColor: '#fff',
    
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 20,
    marginTop: 0,
    
  },
  logout_button_item: {
    backgroundColor: '#fff',
    
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 20,
    marginTop: 0,
    
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
