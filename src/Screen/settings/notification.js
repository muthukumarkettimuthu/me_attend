// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import React, { Component, useState } from 'react';

import font from '../styles/font';
import {
  StyleSheet,

  View,
  Switch,
  Text,
  Image,

  FlatList,
  SafeAreaView,




  TouchableOpacity,

} from 'react-native';

import ToggleSwitch from 'toggle-switch-react-native'
import { FONTS, images, SIZES } from '../../assets/styles/theme';









const notification = ({navigation}) => {

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);





  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: "#fff" }}>

        <View style={[styles.rowgroup, styles.item, styles.centercontent]}>
          <TouchableOpacity 
            onPress={()=>navigation.goBack()}
            style={{ position: "absolute", left: 5,height:40,width:40,justifyContent:'center' }}>
            <Image source={images.prevarrow_icon}
              style={{ resizeMode: "contain",height:15,
              width:9 }}
            />

          </TouchableOpacity>
          <View style={{ position: "relative", justifyContent: "center" }}>
            <Text style={[font.bold, font.textblue, font.sizeLarge]}>Notification</Text>
          </View>



        </View>
        {/* <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>
        <View style={[styles.rowgroup, styles.item, styles.between]}>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Option 1
          </Text>
          <ToggleSwitch
            isOn={true}
            onColor="#293272"
            offColor="#fff"
            labelStyle={{ color: "#293272", fontWeight: "900" }}
            size="small"
            onToggle={isOn => console.log("changed to : ", isOn)}
          />
        </View>*/}
        <View style={{
            height: 50,
           
            marginTop: 20,
            width:SIZES.width/1.1,
           marginHorizontal:20,
            backgroundColor: "#FFF6C6",
            borderRadius: 5,
            borderColor: "#FEF1AF",

            justifyContent: 'center',
            paddingLeft: 8
          }}>
            <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No option available now</Text>
          </View>

      </View> 

    </SafeAreaView>
  );
}
export default notification


const styles = StyleSheet.create({
  between: {
    justifyContent: "space-between",
    marginHorizontal: 20
  },
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 81,
    width: 81,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 15,
    width: 100,
    marginHorizontal: 20
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 10,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
