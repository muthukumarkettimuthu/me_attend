
import React, { useState, useRef, useEffect } from 'react';

import font from '../styles/font';

import RBSheet from "react-native-raw-bottom-sheet";

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';
import ApiConstants from '../../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import { Dropdown } from 'react-native-element-dropdown';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import Loader from '../Components/Loader';

const data = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
];






const accountinfo = ({ navigation }) => {
  const refRBSheet = useRef();
  const [city, setCity] = useState(0);
  const [gender, setGender] = useState("");
  const [loader_visible, setLoaderVisibility] = useState(false);
  const [country, setCountry] = useState(0);
  const [country_list, setCountryList] = useState([]);
  const [city_list, setCityList] = useState([]);
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [lastname, setLastName] = useState("");
  const [id, setId] = useState("");
  const [mobile_number, setMobileNumber] = useState("");
  const [profileImage, setProfileImage] = useState("");;
  const [country_name, setCountryName] = useState("");
  const [city_name, setCityName] = useState("");
  const [current_password, setCurrentPassword] = useState("");
  const [new_password, setNewPassword] = useState("");
  const [confirm_password, setConfirmPassword] = useState("");
  const [password_validation_msg, setPasswordValidationVisible] = useState(false)
  const [new_password_validation_msg, setNewPasswordValidationVisible] = useState(false)
  const [confirm_password_validation_msg, setConfirmPasswordValidationVisible] = useState(false)
  useEffect(() => {

    getProfile()
  }, []);


  const _renderItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>

      </View>
    );
  };

  const _renderCountryItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.country}</Text>

      </View>
    );
  };

  const _renderCityItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.city}</Text>

      </View>
    );
  };


  const getProfile = async () => {
   
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        // this.setState({loader_visible:true})

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
            console.debug("prof", responseJson)

            let age = responseJson.data.age;
            let city = responseJson.data.city;
            let country = responseJson.data.country;
            let name = responseJson.data.first_name
            let last_name = responseJson.data.last_name
            let email_id = responseJson.data.email_id;
            let gender = responseJson.data.gender;
            let profile_image = responseJson.data.profile_image;
            let id = responseJson.data.id;
            let mobile_number = responseJson.data.mobile_number;

            setAge("" + age);
            setCity(city);
            setCountry(country);
            setName(name);
            setLastName(last_name)
            setEmail(email_id);
            setGender(gender);
            setProfileImage(profile_image);
            setId(id);
            setMobileNumber(mobile_number)
            getCountry(Number.parseInt(country))
            if (city.length != 0) {
              getCity(country, "0")
            }

          }).catch(error => {

            // this.setState({loader_visible:false})


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  const changePassword = async () => {

    console.log("req" + current_password + "," + new_password + "," + confirm_password)
    var password_reg = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (password_reg.test(current_password) == false) {
      setPasswordValidationVisible(true)
    } else if (password_reg.test(new_password) == false) {
      setNewPasswordValidationVisible(true)
    } else if (new_password != confirm_password) {
      setConfirmPasswordValidationVisible(true)
    } else {
      setLoaderVisibility(true)
      var token = await AsyncStorage.getItem("user_id")
      fetch(ApiConstants.url + "change-profile-password", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer' + token
        },
        body: JSON.stringify({
          old_password: current_password,
          password: new_password,
          password_confirmation: confirm_password,

        })
      }).then(response => response.json())
        .then(responseJson => {
          console.debug("save", responseJson)
          const status = responseJson.status
          if (status == "success") {
            refRBSheet.current.close()
            setLoaderVisibility(false)
            Toast.show({
              title: 'Success',
              text: responseJson.value,

              color: '#000',
              timeColor: COLORS.textgreen.color,
              backgroundColor: COLORS.textblue.color,
              timing: 5000,
              position: 'bottom',
            })

          } else {
            refRBSheet.current.close()
            setLoaderVisibility(false)
            Toast.show({
              title: 'Error!',
              text: responseJson.value,

              color: '#000',
              timeColor: 'red',
              backgroundColor: COLORS.textblue.color,
              timing: 5000,
              position: 'bottom',
            })
          }
        })
    }


  }


  const saveUser = async () => {
    setLoaderVisibility(true)
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "update-user", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({
        first_name: name,
        last_name: lastname,
        age: age,
        city: city,
        country: country,
        email_id: email,
        mobile_number: mobile_number,
        gender: gender,
        id: id

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("save", responseJson)
        const status = responseJson.status
        if (status == "success") {
          setLoaderVisibility(false)
          Toast.show({
            title: 'Success',
            text: responseJson.value,

            color: '#000',
            timeColor: COLORS.textgreen.color,
            backgroundColor: COLORS.textblue.color,
            timing: 5000,
            position: 'bottom',
          })

        } else {
          setLoaderVisibility(false)
          Toast.show({
            title: 'Error!',
            text: responseJson.value,

            color: '#000',
            timeColor: 'red',
            backgroundColor: COLORS.textblue.color,
            timing: 5000,
            position: 'bottom',
          })
        }
      })

  }

  const getCountry = (country) => {
    // alert("work")
    fetch(ApiConstants.url + "country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {
          const list = responseJson.value;
          if (country_list.length == 0) {
            setCountryList(responseJson.value)
          }




          var i;
          for (i = 0; i < list.length; i++) {
            if (list[i].country_id == country) {

              setCountryName(list[i].country)

            }
          }









        }
      })


  }
  const selectCountry = (country_id) => {
    console.log(country_id)
    setCountry(parseInt(country_id))
    getCity("" + country_id, "1")

  }

  const test = () => {
    console.log(work)
  }

  const getCity = (country_id, type) => {
    // alert("work"+"city")
    fetch(ApiConstants.url + "city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        country_id: country_id

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("city", responseJson.value[0].city_id)
        const status = responseJson.status
        if (status == "success") {


          const list = responseJson.value;
          setCityList(responseJson.value)

          if (type == 1) {
            setCity(responseJson.value[0].city_id)
          }






        } else {

        }
      })

  }


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

      </Root>
      <ScrollView>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>

          <View style={[styles.rowgroup, styles.item, styles.centercontent]}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ position: "absolute", left: 5, top: 15, width: 40, height: 40, alignSelf: 'center' }}>
              <Image source={images.prevarrow_icon}
                style={{
                  resizeMode: "contain", height: 15,
                  width: 9
                }}
              />

            </TouchableOpacity>
            <View style={{ position: "relative", justifyContent: "center" }}>
              <Text style={[font.bold, font.textblue, font.sizeLarge]}>Account Information</Text>
            </View>



          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium
            ]}
          >
            First Name
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              value={name}
              onChangeText={(text) => setName(text)}
              style={[styles.inputStyle, font.regular]}
              placeholder="First Name" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit={false}
              underlineColorAndroid="#f000"
              returnKeyType="next"
              editable={false}
            />
          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Last Name
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              value={lastname}
              onChangeText={(text) => setLastName(text)}
              style={[styles.inputStyle, font.regular]}
              placeholder="Last Name" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit={false}
              underlineColorAndroid="#f000"
              returnKeyType="next"
              editable={false}
            />
          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Age
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              value={age}
              onChangeText={(text) => setAge(text)}
              style={[styles.inputStyle, font.regular]}
              placeholder="Age" //12345
              placeholderTextColor="#747474"
              keyboardType="name-phone-pad"
              blurOnSubmit={false}
              underlineColorAndroid="#f000"
              returnKeyType="next"
            />
          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Gender
          </Text>

          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={data}
              value={gender}
              searchPlaceholder="Search"
              labelField="label"
              valueField="value"
              placeholder="Select item"
              maxHeight={150}
              onChange={item => {
                setGender(item.value);
                console.log('selected', item);
              }}


            />

          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Country
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={country_list}
              value={parseInt(country)}
              labelField="country"
              valueField="country_id"
              searchPlaceholder="Search"
              placeholder="Select item"
              onChange={item => {
                selectCountry(item.country_id)
                console.log('selected', item);
              }}

              renderItem={item => _renderCountryItem(item)}
            />
          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            City
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={city_list}
              value={parseInt(city)}
              labelField="city"
              valueField="city_id"
              searchPlaceholder="Search"
              placeholder="Select item"
              onChange={item => {
                setCity(item.city_id);
                console.log('selected', item);
              }}

              renderItem={item => _renderCityItem(item)}
            />
          </View>

          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium
            ]}
          >
            Email
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={[styles.inputStyle, font.regular]}
              placeholder="Email Id" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit={false}
              editable={false}
              underlineColorAndroid="#f000"
              returnKeyType="next"
            />
          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium
            ]}
          >
            Phone Number
          </Text>
          <View >
            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <TextInput
                value={mobile_number}
                onChangeText={(text) => setMobileNumber(text)}
                style={[styles.inputStyle, font.regular]}
                placeholder="Phone Number" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit={false}
                editable={false}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
            </View>
          </View>
          <RBSheet
            ref={refRBSheet}
            height={160}
            openDuration={250}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25
              }
            }}
          >
            <Text
              style={[
                font.bold,
                styles.HeadText,
                styles.noleft,
                font.textblue,
                font.sizeMedium
              ]}
            >
              Verify OTP
            </Text>
            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <TextInput
                style={[styles.inputStyle, font.regular]}

                placeholder="Enter OTP" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit={false}
                secureTextEntry={true}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
            </View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Save</Text>
            </TouchableOpacity>

          </RBSheet>


          <RBSheet
            ref={refRBSheet}
            height={380}
            openDuration={250}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25
              }
            }}
          >

            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <TextInput
                value={current_password}
                onChangeText={(text) => setCurrentPassword(text)}
                style={[styles.inputStyle, font.regular]}
                placeholder="Current Pasword" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit={false}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
            </View>
            {
              password_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', marginTop: 10, ...FONTS.regularfont11 }}>
                    Password must has at least 8 characters that
                    include atleast 1 lowercase character, 1 uppercase
                    character,1 number and 1 special character
                  </Text>
                </View> : <View></View>
            }

            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <TextInput
                value={new_password}
                onChangeText={(text) => setNewPassword(text)}
                style={[styles.inputStyle, font.regular]}
                placeholder="New Pasword" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit={false}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
            </View>

            {
              new_password_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', marginTop: 10, ...FONTS.regularfont11 }}>
                    Password must has at least 8 characters that
                    include atleast 1 lowercase character, 1 uppercase
                    character,1 number and 1 special character
                  </Text>
                </View> : <View></View>
            }

            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <TextInput
                value={confirm_password}
                onChangeText={(text) => setConfirmPassword(text)}
                style={[styles.inputStyle, font.regular]}
                placeholder="Confirm Pasword" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit={false}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
            </View>
            {
              confirm_password_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', ...FONTS.regularfont11 }}>
                    Password Mismatch
                  </Text>
                </View> :
                <View></View>
            }
            <TouchableOpacity
              onPress={() => changePassword()}
              style={styles.buttonStyle}
              activeOpacity={0.5}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Save</Text>
            </TouchableOpacity>
          </RBSheet>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              style={styles.buttonStyle}
              onPress={() => refRBSheet.current.open()}>

              <View >

                <Text style={[styles.buttonTextStyle, font.bold]}>Change Password</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              onPress={() => saveUser()}
              style={{ backgroundColor: "#293272" }}
              style={styles.savebuttonStyle}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Save</Text>
            </TouchableOpacity>
          </View>

        </View>
        {
          loader_visible == true ? <Loader /> : <View></View>
        }

      </ScrollView>


    </SafeAreaView>
  );
}

export default accountinfo;




const styles = StyleSheet.create({
  noleft: {
    marginLeft: 0
  },
  validationText: {
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',

  },
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 24,
    shadowColor: "#444444"
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  dropdown: {
    backgroundColor: 'white',
    borderBottomColor: 'gray',
    borderBottomWidth: 0.5,
    marginTop: 20,
  },
  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 20
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 81,
    width: 81,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    width: SIZES.width,
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 15,

    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 15,
    width: "80%",
    marginHorizontal: 20
  },
  savebuttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: 50,
    marginTop: 15,
    width: "80%",
    marginHorizontal: 20
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 10,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,

    width: "100%"
  },
  chengePasswordStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
    alignSelf: 'center'
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    width: SIZES.width / 1.1,
    alignSelf: 'center',
    marginTop: 10
  },
  dropdown1BtnStyle: {
    width: "100%",
    height: 40,
    backgroundColor: "#FFF",
    borderRadius: 8,

  },

});
