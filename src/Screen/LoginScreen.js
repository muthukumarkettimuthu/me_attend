// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, createRef, useEffect, useRef } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Modal,Alert,
  BackHandler,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar
} from 'react-native';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import PushNotification from 'react-native-push-notification';
import Loader from './Components/Loader';
import TimeZone from 'react-native-timezone';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';

const LoginScreen = ({ navigation }) => {

  const [loader_visible, setLoaderVisibility] = useState(false);
  const [email, setEmail] = useState("");
  const [timeZone, setTimeZone] = useState("");
  const [password, setPassword] = useState("");
  const [dialog_visible, setDialogVisible] = useState(false);
  const [dialog_message, setDialogMessage] = useState("");
  const [email_validation_msg, setEmailValidationVisible] = useState(false)
  const [password_validation_msg, setPasswordValidationVisible] = useState(false)
  const ref_input2 = useRef();
  console.log(new Date().getTimezoneOffset())
  useEffect(() => {

    getTimeZone()

    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
        AsyncStorage.setItem("os", token.os);
        AsyncStorage.setItem("device_token", token.token);

      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        if (notification) {
          
          
          // navigation.navigate("notiify")
        }

        // setLoading(false);
        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {

        console.log("NOTIFICATION:", notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });

   
   
  }, []);
  
 const getTimeZone = async() => {
    const timeZone = await TimeZone.getTimeZone().then(zone => {
      setTimeZone(zone)
      console.log( zone );
    });
    
   }
//   const getLocation = async () => {
//     try {
//         const granted = await PermissionsAndroid.request(
//             PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//             {
//                 title: "Permission",
//                 message:
//                     "App needs access to your location ",

//                 buttonNeutral: "Ask Me Later",
//                 buttonNegative: "Cancel",
//                 buttonPositive: "OK"
//             }
//         );
//         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//             console.log("granted");

          

//         } else {
//             console.log("denied" + granted);
//         }
//     } catch (err) {
//         console.warn(err);
//     }
// }
  
  const login = async () => {

    
    
    const os = await AsyncStorage.getItem("os");
    const device_token = await AsyncStorage.getItem("device_token")
   
    const email_value = email;
    const password_value = password;
    console.log(email_value + password_value)
    var status = "";

    var email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var password_reg = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (email_reg.test(email_value) == false) {

      setEmailValidationVisible(true)
    }
    else if (password_reg.test(password_value) == false) {
      setPasswordValidationVisible(true)
    }
    else {

      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          setLoaderVisibility(true)

          fetch(ApiConstants.url + "login", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              email_id: email_value,
              password: password_value,
              device_token:device_token,
              os:os,
              timeZone:timeZone
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.debug("ppp", responseJson)

              status = responseJson.access_token
              if (responseJson.hasOwnProperty("access_token")) {
                setLoaderVisibility(false)
                setEmail("");
                setPassword("")
                storeToken(responseJson.access_token)
                setEmailValidationVisible(false)
                setPasswordValidationVisible(false)
                navigation.replace('tab')
              } else {
                setLoaderVisibility(false)
                
                setDialogMessage(responseJson.error)
                Toast.show({
                  title: 'Error!',
                  text: responseJson.error,
                  
                  color: '#000',
                  timeColor: 'red',
                  backgroundColor:COLORS.textbluedark.color,
                  timing: 5000,
                  position: 'bottom',
                })
              }
            }).catch(error => {

              setLoaderVisibility(false)
              Toast.show({
                title: 'Please check your internet connection!',
                text: "",
                
                color: '#000',
                timeColor: 'red',
                backgroundColor:COLORS.textbluedark.color,
                timing: 5000,
                position: 'bottom',
              })
              console.log(error)
            })
        } else {

          setDialogMessage("No Internet")
        }
      })


    }






  }
  const storeToken = (token) => {
    AsyncStorage.setItem("user_id", token)
  }
  return (
    <View style={styles.mainBody}>
      <StatusBar
        animated={true}
        backgroundColor={COLORS.textblue.color}
      />
      <Root style={{ position: 'absolute', height: SIZES.height,width:SIZES.width }}>
          
      </Root>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
        }}><View style={{ alignItems: 'center', position: 'absolute', top: 0, flex: 1, left: 0, right: 0 }}>
          <Image
            source={images.large_logo_icon}
            style={{
              resizeMode: "contain",
              margin: 30,
              height: 30
            }}
          />
        </View>

        <View>
          <KeyboardAvoidingView enabled>
            <Text
              style={{
                ...FONTS.boldsizeLarge,
                ...COLORS.white,
                marginLeft: 35
              }}
            >
              Sign In
            </Text>
            <View style={styles.SectionStyle}>
              <TextInput
                style={[styles.inputStyle, FONTS.regularsizeRegular]}
                value={email}
                onSubmitEditing={() => ref_input2.current.focus()}
                onChangeText={(text) => {
                  setEmail(text)
                  setEmailValidationVisible(false)
                }}
                placeholder="Email" //dummy@abc.com
                placeholderTextColor="#747474"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            {
              email_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Please Enter A Valid Email Address</Text>
                </View> :
                <View></View>
            }
            <View style={[styles.SectionStyle, styles.bottomspace]}>
              <TextInput
                style={[styles.inputStyle, FONTS.regularsizeRegular]}
                ref={ref_input2}
                value={password}
                onChangeText={(text) => {
                  setPassword(text)
                  setPasswordValidationVisible(false)
                }}
                placeholder="Password" //12345
                placeholderTextColor="#747474"
                keyboardType="default"
                blurOnSubmit
                secureTextEntry={true}
                underlineColorAndroid="#f000"

              />
            </View>
            {
              password_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', marginTop: 10, ...FONTS.regularfont11 }}>
                    Password must has at least 8 characters that 
                    include atleast 1 lowercase character, 1 uppercase
                    character,1 number and 1 special character
                  </Text>
                </View> : <View></View>
            }
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={() => login()}
            >
              <Text style={[styles.buttonTextStyle, FONTS.boldsizeMedium]}>Sign In</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5}>
              <Text
                style={[styles.registerTextStyle, styles.forgot, FONTS.boldsizeRegular]}
                onPress={() => navigation.navigate('forgotpassword')}>
                Forgot Password?
              </Text>
            </TouchableOpacity>
            <Text
              style={[FONTS.boldsizeRegular, styles.registerTextStyle]}
              onPress={() => navigation.navigate('RegisterScreen')}
            >
              New to Me Attend? <Text style={{ color: "#56D6BE" }}>Sign Up</Text>
            </Text>
          </KeyboardAvoidingView>
        </View>
        {
          loader_visible == true ? <Loader /> : <View></View>
        }

        <Modal
          activeOpacity={1}
          transparent={true}
          visible={dialog_visible}
        >
          <TouchableOpacity
            onPress={() => setDialogVisible(false)}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                borderWidth: 1,
                borderColor: COLORS.textblue.color,
                minHeight: 150,
                maxHeight: 150,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  height: "100%"

                }}
              >

                <Text style={{ marginTop: 10, padding: 10, textAlign: 'justify', ...FONTS.regularsizeRegular }}>
                  {dialog_message}
                </Text>



              </ScrollView>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0
                }}
                onPress={() => setDialogVisible(false)}
              >
                <Image source={images.cancel_icon} style={{
                  width: 20,
                  height: 20,
                  tintColor: COLORS.textblue,
                  alignSelf: 'flex-end',
                  marginTop: 10,
                  marginRight: 10
                }} />
              </TouchableOpacity>
            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        
      </ScrollView>


    </View>
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  HeadText: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 35
  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  validationText: {
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',

  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    borderWidth: 1,
    borderColor:'white',
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    justifyContent:'center',
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#56D6BE',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 46,
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,

  },
  inputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    paddingRight: 15,
    
    borderRadius: 8,
    
    height:39
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',

    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
});
