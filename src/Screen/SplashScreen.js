
import React, { useState, useEffect,useRef } from 'react';
import { StatusBar, View, StyleSheet, Image } from 'react-native';

import PushNotification from 'react-native-push-notification';
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, images } from '../assets/styles/theme';






const SplashScreen = ({ navigation }) => {
  //State for ActivityIndicator animation
  const [animating, setAnimating] = useState(true);
  let [loading, setLoading] = useState(true);
  const mountedRef = useRef(true)
  useEffect(() => {

    

    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
        AsyncStorage.setItem("os", token.os);
        AsyncStorage.setItem("device_token", token.token);

      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        if (notification) {
          setLoading(false)
          loading=false
          console.log(loading)
         
          navigation.navigate('tab', { screen: 'notiify' });
      
        }

       
        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {

        console.log("NOTIFICATION:", notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });

    if (loading) {
      setTimeout(() => {
        if (!mountedRef.current) return null
        setAnimating(false);
        //Check if user_id is set or not
        //If not then send for Authentication
        //else send to Home Screen
        if (loading) {
          console.log(loading)
          AsyncStorage.getItem('user_id').then((value) =>
          navigation.replace(value === null ? 'LoginScreen' : 'tab'
          ),
        );
        }
        
      }, 5000);
    }
    return () => { 
      mountedRef.current = false
    }
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor={COLORS.textblue.color}
      />
      <Image
        source={images.gps4_icon}
        style={{ tintColor: COLORS.textgreen.color }}
      />
      <Image
        source={images.logo2_icon}
        style={{ resizeMode: 'contain', }}
      />
     
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#293272',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});
