// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,

} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import ApiConstants from '../services/APIConstants';

import { Root, Toast } from 'react-native-popup-confirm-toast'
import NetInfo from "@react-native-community/netinfo";

import Loader from './Components/Loader';
import font from './styles/font';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';


const PromoterScreen = ({ navigation, route }) => {

    const [event_id, setEventId] = useState(route.params.event_id)
    const [event_image, setEventImage] = useState(route.params.event_image)
    const [loader_visible, setLoader] = useState(false)
    const [attend_event_list, setAttendList] = useState([])
    const [tag_list, setTagList] = useState([])
    const [description, setDescription] = useState("")


    useEffect(() => {
        getAttendEvent()
    }, []);

    const promoteEvent = async () => {
        console.log(tag_list + "," + description.length)
        if (description.length != 0) {
            var token = await AsyncStorage.getItem("user_id")
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    setLoader(true)

                    fetch(ApiConstants.url + "store_event_promotion", {
                        method: "POST",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer' + token
                        },
                        body: JSON.stringify({
                            event_id: event_id,
                            description: description,
                            tag_event_id: tag_list
                        })
                    }).then(response => response.json())
                        .then(responseJson => {

                            let status = responseJson.status.toLowerCase();
                            console.log(status)
                            if (status == "success") {
                                navigation.reset({
                                    index: 0,
                                    routes: [
                                        {
                                            name: 'tab',

                                        },
                                    ],
                                })
                            }

                            setLoader(false)


                        }).catch(error => {

                            setLoader(false)


                        })
                } else {

                    Toast.show({
                        title: 'Network Error!',
                        text: "",
                        bottom: 100,
                        color: '#000',
                        timeColor: 'red',
                        backgroundColor: COLORS.textblue.color,
                        timing: 5000,
                        position: 'bottom',
                    })
                }
            })
        } else {
            Toast.show({
                title: 'Error!',
                text: "Please Add Description",

                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
            })
        }

    }

    const getList = (item, index) => {

        tag_list[index] = item.id
        console.log(tag_list)
        const newArrData = attend_event_list.map((e, index) => {

            if (item.id == e.id) {
                item.isSelected = true

            }
            return {
                ...e

            }
        })

        setAttendList(newArrData)


    }

    const getAttendEvent = async () => {
        var token = await AsyncStorage.getItem("user_id")
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                setLoader(true)

                fetch(ApiConstants.url + "tag-attend-event", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer' + token
                    },
                    body: JSON.stringify({
                        event_id: event_id
                    })
                }).then(response => response.json())
                    .then(responseJson => {


                        setLoader(false)


                        let arr = responseJson.data.map((item, index) => {
                            item.isSelected = false
                            return { ...item }
                        })
                        console.log(arr)
                        setAttendList(arr)
                    }).catch(error => {

                        setLoader(false)


                    })
            } else {

                Toast.show({
                    title: 'Network Error!',
                    text: "",

                    color: '#000',
                    timeColor: 'red',
                    backgroundColor: COLORS.textblue.color,
                    timing: 5000,
                    position: 'bottom',
                })
            }
        })
    }




    return (
        <View style={{ flex: 1, backgroundColor: "#fff", paddingHorizontal: 15, height: SIZES.height }}>
            <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

            </Root>
            <View style={{ backgroundColor: "#fff", width: "100%", height: 30, marginBottom: 30, marginTop: 30 }}>
                <TouchableOpacity
                    style={{ position: 'absolute', left: 6 }}
                    onPress={() => {
                        navigation.goBack()
                    }}
                >
                    <Image style={{ height: 15, width: 15, marginTop: 20 }} source={images.cancel_icon} />
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: "row", marginTop: 30 }}>
                <View style={{ flex: 0.5 }}>
                    <Image
                        source={{
                            uri: ApiConstants.path + "images/events/" + event_image
                        }}
                        style={{

                            height: 120,
                            width: 120,
                            borderRadius: 5,
                            borderWidth: 1,
                            borderColor: 'black'
                        }}
                    />
                </View>
                <View style={[styles.SectionStyle, styles.shadowbox, styles.minhet]}>
                    <TextInput
                        value={description}
                        style={[styles.inputStyle, FONTS.regularsizeRegular, styles.minhet2]}
                        placeholder="Enter Description" //12345
                        placeholderTextColor="#747474"
                        keyboardType="default"
                        blurOnSubmit={false}
                        multiline={true}
                        onChangeText={(text) => setDescription(text)}
                        underlineColorAndroid="#f000"
                        textAlign="left"
                        autoFocus={true}
                    />
                </View>
            </View>
            <Text style={[font.bold, font.sizeMedium, font.textblue, styles.space]}>Tag from the below  events you are attending</Text>

            <ScrollView style={{ flex: 1, marginBottom: 90 }}>


                {
                    attend_event_list.map((item, index) => (

                        <TouchableOpacity
                            activeOpacity={1}
                            // onPress={() => navigation.navigate('SettingScreen', { event_id: item.id })}
                            style={[styles.rowgroup, styles.item, styles.shadowbox]}>

                            <Image source={{ uri: ApiConstants.path + "images/events/" + item.event_first_image.image }}
                                style={styles.itemimg}
                            />

                            <View style={{ marginRight: 20, position: "relative", flex: 1 }}>
                                <Text numberOfLines={1} style={{ color: "#333333", fontSize: 16, marginBottom: 4, ...FONTS.regularsizeMedium, fontWeight: 'bold' }}>
                                    {item.event_name}
                                </Text>
                                <Text style={{ color: "#747474", fontSize: 11, marginBottom: 8 }} numberOfLines={1}>
                                    {item.from_time}-{item.to_time} | {item.date_time.split("-").reverse().join("/")}
                                </Text>
                                <Text numberOfLines={1} style={{ color: "#747474", fontSize: 14, fontWeight: 'bold', ...FONTS.regularsizeRegular }} > {item.company_name}</Text>

                                <View style={{ paddingTop: 9, flexDirection: "row", paddingRight: 30 }} >
                                    <Image
                                        source={images.gps1_icon}
                                        resizeMethod="scale"
                                        resizeMode="stretch"
                                        style={{
                                            height: 13,
                                            width: 16,
                                            marginRight: 2,
                                            alignSelf: "center",
                                            tintColor: COLORS.textgreen.color
                                        }}
                                    />

                                    <Text style={{ color: "#747474", fontSize: 12, }, font.regular} numberOfLines={1}> {item.event_address}</Text>

                                </View>
                                <View>
                                    <TouchableOpacity
                                        onPress={() => {

                                            getList(item, index)
                                        }}
                                        style={styles.buttonStyle}
                                    >
                                        {!item.isSelected ?
                                            <Text style={[styles.buttonTextStyle, font.bold]}>Tag</Text> :
                                            <Text style={[styles.buttonTextStyle, font.bold]}>Tagged</Text>
                                        }
                                    </TouchableOpacity>


                                </View>

                            </View>
                            <View style={{
                                position: "absolute",
                                right: 10,
                                top: 10
                            }}>
                                <Image source={images.menu_icon}
                                    style={
                                        {
                                            resizeMode: "contain",
                                            height: 20,
                                            width: 20,
                                            tintColor: COLORS.graycolor.color

                                        }
                                    }
                                />
                            </View>

                        </TouchableOpacity>

                    ))
                }


            </ScrollView>
            <View
                style={{
                    position: 'absolute',
                    width: SIZES.width,
                    height: 80,
                    backgroundColor: 'white',
                    bottom: 0,
                    alignSelf: 'center',
                    justifyContent: 'center'
                }}
            >
                <TouchableOpacity
                    onPress={() => promoteEvent()}
                    style={{
                        width: "80%",
                        height: 40,
                        backgroundColor: COLORS.textgreen.color,
                        marginHorizontal: 20,
                        alignSelf: 'center',
                        borderRadius: 10,
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{
                        color: "white",
                        ...FONTS.boldsizeMedium,
                        alignSelf: 'center'
                    }}>
                        Publish
                    </Text>
                </TouchableOpacity>

            </View>

            {loader_visible ? <Loader /> : <View></View>}

        </View>
    );
};
export default PromoterScreen;

const styles = StyleSheet.create({
    space: {
        padding: 10,
        marginTop: 20
    },
    smalltitle: {
        color: "#000000", fontSize: 14, paddingTop: 15
    },
    changetxt: {
        color: "#293272", fontSize: 14
    },
    poptitle: {
        color: "#293272", fontSize: 18,
        marginBottom: 10
    },
    squrebox: {
        backgroundColor: "#56D6BE",
        width: 50,
        marginRight: 10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
    },
    topspace: {
        top: -20,
    },
    dotStyle: {
        width: "31%",
        height: 3,
        backgroundColor: '#C8C8C8',
        opacity: 0.8,
        borderRadius: 0,
    },
    activeDotStyle: {
        width: "31%",
        height: 3,
        backgroundColor: '#56D6BE',
        borderRadius: 0,
    },
    searchSection: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 50,
        flex: 1,
        position: "relative",
        marginHorizontal: 20,
        borderRadius: 8,
    },
    shadowbox: {
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        elevation: 4,
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        paddingTop: 10,
        paddingRight: 40,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    rowgroup: {
        flexDirection: "row",
    },
    textstyle: {
        color: "#fff",
        fontSize: 15,
        alignSelf: "center"
    },
    stretchitem: {
        alignItems: "stretch"
    },
    headerbg: {
        backgroundColor: "#293272",
        height: 100,
        paddingHorizontal: 40,
        paddingLeft: 20,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },

    HeadText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: "bold",
        marginLeft: 35
    },
    drop: {
        backgroundColor: '#fafafa',
        color: '#747474',

    },
    bottomspace: {
        marginBottom: 0
    },
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#293272',
        alignContent: 'center',
    },
    itemimg: {
        resizeMode: 'cover',
        height: 140,
        width: 140,
        marginRight: 10,
        alignSelf: "flex-start",
        justifyContent: "flex-start",
        borderRadius: 10
    },
    SectionStyle: {
        borderRadius: 8,
        flex: 1,
        backgroundColor: "white",
        marginBottom: 3,
        marginLeft: 10,
        marginRight: 0,
        height: 120
    },

    buttonStyle: {
        backgroundColor: '#293272',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#56D6BE',
        height: 33,
        alignItems: 'center',
        borderRadius: 8,
        marginRight: 35,
        marginTop: 10,
        marginBottom: 0,
        width: 100,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        fontWeight: "bold",
        fontSize: 14,
        paddingTop: 7,
        alignSelf: "center",
    },
    inputStyle: {
        color: '#747474',
        paddingLeft: 15,
        fontSize: 14,
        paddingRight: 15,
        borderRadius: 8,
        width: "100%",
        alignSelf: "flex-start",
        justifyContent: "flex-start"
    },
    forgot: {
        alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'flex-start',
        marginLeft: 35,
        padding: 10,
        paddingTop: 0
    },
    tabStyle: {
        height: 30,
        minHeight: 30,
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        fontFamily: "Oxygen-Bold",

    },
    tab: {
        backgroundColor: "#fff",
        width: "auto",
        fontFamily: "Oxygen-Bold",
        borderBottomWidth: 1,
        borderColor: "#C8C8C8"
    },
    indicator: {
        height: 4,
        backgroundColor: "#56D6BE",
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    inputIOS: {
        fontSize: 14,
        paddingVertical: 10,
        paddingHorizontal: 12,
        borderWidth: 1,
        borderColor: 'green',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 14,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: 'blue',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    item: {
        backgroundColor: '#fff',
        padding: 8,
        marginVertical: 8,
        marginHorizontal: 5,
        borderRadius: 8
    },
    title: {
        fontSize: 32,
    },
    searchIcon: {
        width: 18,
        height: 18,
        position: "absolute",
        right: 15,
    },

});
