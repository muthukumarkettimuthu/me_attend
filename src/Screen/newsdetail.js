// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { Component, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Picker,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import CheckBox from 'react-native-check-box'

import { images } from '../assets/styles/theme';

import font from './styles/font';


const newsdetail = ({ navigation }) => {
  const [checked, setChecked] = React.useState(false);
  const [selectedValue, setSelectedValue] = useState("City");
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <ScrollView>
        <View>
          <KeyboardAvoidingView enabled>
            <View style={styles.closeicon}>
              <Image source={images.close_icon} resizeMode="contain" style={{ position: "absolute", left: 15 }} />
              <Text style={[font.bold, styles.header]}>Advanced Filters</Text>
            </View>
            <View style={[styles.SectionStyle, styles.shadowbox]}>
              <Picker
                selectedValue={selectedValue}
                style={[styles.inputStyle, font.bold, styles.shadowbox]}
                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
              >
                <Picker.Item label="City" value="java" style={{ alignSelf: "center", fontSize: 10 }} />
                <Picker.Item label="JavaScript" value="js" />
              </Picker>
            </View>
            <View style={[styles.SectionStyle, styles.bottomspace, styles.shadowbox]}>
              <Picker
                selectedValue={selectedValue}
                style={[styles.inputStyle, font.bold, styles.shadowbox]}
                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
              >
                <Picker.Item label="United Kingdom" value="java" style={{ alignSelf: "center", fontSize: 10 }} />
                <Picker.Item label="JavaScript" value="js" />
              </Picker>
            </View>
            <View style={[styles.shadowbox, styles.SectionStyle]}>
              <DatePicker
                style={{ width: "100%", }}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2016-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{

                  dateIcon: {
                    position: 'absolute',
                    right: 10,
                    top: 15,
                    marginLeft: 0,
                    fontSize: 10,
                    width: 20,
                    height: 20
                  },
                  dateInput: {
                    marginLeft: 10, marginTop: 6, color: "#747474", opacity: 1,
                    borderColor: "#fff", borderWidth: 0, justifyContent: "center", alignItems: "flex-start"
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => { "7676" }}
              />
            </View>

            <View style={styles.checkboxContainer}>
              <CheckBox
                style={{ flex: 1, padding: 10 }}
                onClick={() => {
                  this.setState({
                    isChecked: !this.state.isChecked
                  })
                }}
                isChecked={this.state.isChecked}
                leftText={"CheckBox"}
              />
              <Text style={styles.label}>Music</Text>
            </View>

            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Sgn In</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5}>
              <Text
                style={[styles.registerTextStyle, styles.forgot, font.bold]}
                onPress={() => navigation.navigate('forgotpassword')}>
                Fogot Password
              </Text>
            </TouchableOpacity>
            <Text
              style={[font.bold, styles.registerTextStyle]}
              onPress={() => navigation.navigate('RegisterScreen')}>
              New to Me Attend? <Text style={{ color: "#56D6BE" }}>Sign Up</Text>
            </Text>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    </View>
  );

};

export default newsdetail

const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  closeicon: {
    flexDirection: "row", paddingHorizontal: 15, height: 60,
    alignItems: "center", backgroundColor: "#fff", marginHorizontal: 10, justifyContent: "center"
  },
  header: {
    color: "#293272", fontSize: 18,
    alignSelf: "center",
  },
  HeadText: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 35
  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#56D6BE',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 46,
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
});
