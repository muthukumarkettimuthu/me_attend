
import React, { Component } from 'react';

import font from './styles/font';
import moment from 'moment';
import { TabView, TabBar } from 'react-native-tab-view';

import Swipeable from 'react-native-swipeable';
import io from 'socket.io-client';
import { Root, Toast } from 'react-native-popup-confirm-toast'

import axios from 'react-native-axios'
import QRCode from 'react-native-qrcode-svg';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,

  FlatList,
  SafeAreaView,

  ActivityIndicator,
  Animated,
  Modal,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';


import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../services/APIConstants';
import { SIZES, FONTS, COLORS, images } from '../assets/styles/theme';
import AsyncStorage from '@react-native-community/async-storage';

const initialLayout = { width: Dimensions.get("window").width }
let chat_token = ""

const DATA1 = [{
  id: 1,
  reason: 'Spam',
  active: true
}, {
  id: 2,
  reason: 'Sexually inappropriate',
  active: false
},
{
  id: 3,
  reason: 'Child sexual exploitation or abuse',
  active: false
},
{
  id: 4,
  reason: 'Harassment or bullying',
  active: false
}

]








export default class notiify extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      search: '',
      chat_token: "",
      index: 0,
      routes: [
        { key: 'first', title: 'Notifications' },
        { key: 'second', title: 'Messages' },
      ],
      loader_visible: true,
      page: 1,
      last_page: 0,
      notification_list: [],
      messageList: [],
      block_dialog_visible: false,
      discount_dialog_visible: false,
      unblock_dialog_visible: false,
      report_dialog_visible: false,
      category_list: [],
      contact_list: [],
      block_list: [],
      search_list: [],
      request_list: [],
      active_user: {},
      friend_number: "",
      reason: "Spam",
      mobile_number: "",
      search_keyword: "",
      friend_list: [],
      new_user: [],
      discount: "no discount code",
    }
    this.socket = null
  }
  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };
  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  componentDidMount = () => {

    this.setState({ category_list: DATA1 })
    this.props.navigation.addListener('focus', e => {
      this.getNotificationList()
      this.startSocketConnection()
      this.getProfile()


    });

    this.props.navigation.addListener('blur', e => {

      this.setState({ notification_list: [] })



      this.setState({ page: 1 })

    });

    this.setState({
      center: {

        latitude: 51.519443,
        longitude: -0.126000,
      }
    });
  }


  getProfile = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {


            let mobile_number = responseJson.data.mobile_number;
            this.setState({ mobile_number: mobile_number })



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  rightButtons = (item) => [

    <TouchableOpacity
      onPress={() => {
        this.setState({ block_dialog_visible: true })
        this.setState({ friend_number: item.mobileNumber })

      }}
      style={{ backgroundColor: "#DB2B2B", height: '100%', justifyContent: 'center' }}>
      <Text style={{ color: "#fff", fontSize: 14, marginLeft: 20 }}>Block</Text>
    </TouchableOpacity>,
    <TouchableOpacity
      onPress={() => this.setState({ report_dialog_visible: true })}
      style={{ backgroundColor: "#C4C4C4", height: '100%', justifyContent: 'center' }}>
      <Text style={{ color: "#fff", fontSize: 14, marginLeft: 20 }}>Report</Text>
    </TouchableOpacity>



  ]

  block_listrightButtons = (item) => [

    <TouchableOpacity
      onPress={() => {
        this.setState({ unblock_dialog_visible: true })
        this.setState({ friend_number: item.mobileNumber })

      }}
      style={{ backgroundColor: "#DB2B2B", height: '100%', justifyContent: 'center' }}>
      <Text style={{ color: "#fff", fontSize: 14, marginLeft: 20 }}>Unblock</Text>
    </TouchableOpacity>,
    <TouchableOpacity
      onPress={() => this.setState({ report_dialog_visible: true })}
      style={{ backgroundColor: "#C4C4C4", height: '100%', justifyContent: 'center' }}>
      <Text style={{ color: "#fff", fontSize: 14, marginLeft: 20 }}>Report</Text>
    </TouchableOpacity>


  ]

  startSocketConnection = async () => {
    var token = await AsyncStorage.getItem("user_id")
    this.socket = io.connect('wss://chat.meattend.com/', {
      transports: ["websocket"],
    });

    this.socket.once("connect", () => {
      let accesstoken = {
        token: token,
        is_company: false,
      };


      console.log("rrrrr", accesstoken)
      this.socket.emit("login", accesstoken);
      this.socket.on("login_response", (token) => {
        console.log("gfg", token)
        chat_token = token.token
        this.setState({ chat_token: token.token })
        AsyncStorage.setItem("chat_token", token.token)

        let params = {
          token: chat_token
        }

        console.log("ggj", params)

        this.socket.on("error", (msg) => {
          console.log(msg); // ERROR MESSAGE
        })
        this.socket.emit("contactlist", params)
        this.socket.on("proccessing_error", (msg) => {

          console.log(msg); // ERROR MESSAGE
        })

        this.socket.on("contactlist_response", (usercontacts) => {



          let arr = usercontacts.data.map((item, index) => {
            item.contact = true
            item.request = false
            item.invite = false
            item.block = false
            return { ...item }
          })

          console.log("contactsss", arr)


          this.setState({ contact_list: arr })
        })
        this.socket.once("invitelist_response", (user) => {
          console.log("userinvite", user.data)
          let arr = user.data.map((item, index) => {
            item.contact = false
            item.request = false
            item.invite = true
            item.block = false
            return { ...item }
          })



          this.setState(state => ({ contact_list: [...arr, ...this.state.contact_list] }))

        })

        this.socket.once("blocklist_response", (user) => {
          console.log("block_user", user)
          this.setState({ block_list: user.data })
        })

        this.socket.once("requestlist_response", (user) => {
          console.log("userdd", user)

          var temp = []
          var with_order_time = []
          var with_out_order_time = []
          var merge_array = []

          let arr = user.data.map((item, index) => {
            item.contact = false
            item.request = true
            item.invite = false
            item.block = false
            return { ...item }
          })





          this.setState({ request_list: user.data })


          var jsonArray = [...arr, ...this.state.contact_list]



          jsonArray.forEach((list) => {

            if (list.hasOwnProperty("orderTime")) {
              with_order_time.push(list)
            } else {
              with_out_order_time.push(list)
            }





          })

          with_order_time.sort((a, b) => b.orderTime.localeCompare(a.orderTime));
          merge_array = [...with_order_time, ...with_out_order_time]



          this.setState({ contact_list: merge_array })
        })
      })

      this.socket.on('receive_message', msg => {

        var arr = []
        var with_order_time = []
        var with_out_order_time = []
        var merge_array = []
        console.log(msg.data)
        this.state.contact_list.forEach((list) => {

          console.log("rrrr", msg.data.senderId, list.mobileNumber);
          if (msg.data.senderId == list.mobileNumber) {
            list.count = list.count + 1
            list.orderTime = msg.data.created_at
            list.updatedAt = msg.data.updatedAt
            console.log(list.count)
          }





        })

        arr = this.state.contact_list

        arr.forEach((list) => {

          if (list.hasOwnProperty("orderTime")) {
            with_order_time.push(list)
          } else {
            with_out_order_time.push(list)
          }




        })

        with_order_time.sort((a, b) => b.orderTime.localeCompare(a.orderTime));



        merge_array = [...with_order_time, ...with_out_order_time]
        console.log("with_order_time", merge_array)

        this.setState({ contact_list: merge_array })
      })

      this.socket.on("online_user", (data) => {

        var jsonString = data.users
        this.setState({ active_user: data.users })


        let phone = "9080582585"



        this.state.contact_list.forEach((list) => {
          if (data.users[list.mobileNumber] != undefined) {

            if (list.mobileNumber == data.users[list.mobileNumber].user) {
              list.online = data.users[list.mobileNumber].online

            }
          }


        })

        this.setState({ contact_list: this.state.contact_list })


      })

      this.socket.once("accept_request_response", (msg) => {
        console.log("sasasasasas" + msg); // RESPONSE OF ACCEPT/DECLINE
        this.setState({ contact_list: [] })
        this.startSocketConnection()



      });

      this.socket.on("request_friend", (request) => {
        console.log("rqfrnd", request);
        var jsonArray = []
        jsonArray.push(request.friend)
        let arr = jsonArray.map((item, index) => {
          item.contact = false
          item.request = false
          item.invite = true
          item.block = false
          return { ...item }
        })
        console.log("userddfrrndreq", arr)


        this.setState(state => ({ contact_list: [...arr, ...this.state.contact_list] }))
      })

    })
  }






  getNotificationList = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {


        fetch(ApiConstants.url + "get-notification?page=" + this.state.page, {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
            // alert("sds")
            let count = responseJson.notification_count

            console.log("notification:", responseJson.notification)

            if (responseJson.notification.data.length == 0) {
              this.setState({ loader_visible: false })
            }
            console.debug(responseJson.notification.data[0])
            this.setState(state => ({ notification_list: [...this.state.notification_list, ...responseJson.notification.data] }))
            this.setState({ last_page: responseJson.notification.last_page })

            this.getCount(count);

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  getCount = (count) => {
    AsyncStorage.setItem("count", String(count))


  }

  acceptPromotion = async (notification_id, company_id) => {
    console.log(notification_id + "," + company_id)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {


        fetch(ApiConstants.url + "accept-promotion", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            notification_id: notification_id,
            company_id: company_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            // alert("sds")
            console.log(responseJson)
            this.setState({ loader_visible: false })
            this.setState({ notification_list: [] })
            this.setState({ page: 1 })
            this.getNotificationList()



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  declinePromotion = async (notification_id, company_id) => {
    console.log(notification_id + "," + company_id)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        // this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "decline-promotion", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            notification_id: notification_id,
            company_id: company_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            // alert("sds")
            console.log(responseJson)
            this.setState({ loader_visible: false })
            this.setState({ notification_list: [] })
            this.setState({ page: 1 })
            this.getNotificationList()



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  readNotification = async (notification_id) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        // this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "save-discount-count", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            notification_id: notification_id,

          })
        }).then(response => response.json())
          .then(responseJson => {
            // alert("sds")
            console.log(responseJson)
            this.setState({ loader_visible: false })
            this.setState({ notification_list: [] })
            this.setState({ page: 1 })
            this.getNotificationList()



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  renderItem = ({ item }) => {
    return (
      <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1, marginTop: 12 }}>
        <View>
          <TouchableOpacity
            onPress={() => {
              if (item.template_name == "NEAR_EVENT_NOTIFY") {
                this.props.navigation.navigate('SettingScreen', { event_id: item.event_id, notification_id: item.id })
              } else if (item.template_name == "DISCOUNT_NOTIFICATION") {
                this.setState({ discount: item.discount_code })
                this.setState({ discount_dialog_visible: true })
                this.readNotification(item.id)
              }


            }}
            style={[styles.rowgroup, styles.item, styles.noborder, styles.nospace]}>
            <View>
              <Image source={{ uri: ApiConstants.path + item.asset_url }}
                style={styles.itemimg}
              />
            </View>
            <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
              {item.read == 0 ?
                <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>{item.content_heading}</Text>
                : <Text style={{ color: "#a3a2a2", fontSize: 16, marginBottom: 15, ...FONTS.boldsizeMedium }}>{item.content_heading}</Text>}

              {item.template_name == "PROMOTER_REQUEST" && item.read == 1 ?

                <Text>sent you a<Text style={{ ...FONTS.regularsizeRegular, color: COLORS.textgreen.color }}> Promoter </Text>request</Text>
                : <Text style={{ color: "#747474", fontSize: 12, marginBottom: 0, fontSize: 14 }} numberOfLines={1}>{item.content_text}</Text>
              }
              <Text style={{ color: "#747474", fontSize: 14, lineHeight: 19, fontSize: 14, opacity: 0.6, position: 'absolute', right: -30, top: 0 }} numberOfLines={1}>{item.recived_time}</Text>
            </View>

          </TouchableOpacity>

        </View>
        {item.template_name == "PROMOTER_REQUEST" && item.read == 0 ? <View style={{
          flexDirection: "row", paddingHorizontal: 15, paddingBottom: 15, borderBottomWidth: 1,
          borderBottomColor: "#ccc"
        }}>
          <TouchableOpacity
            onPress={() => {
              this.acceptPromotion(item.id, item.company_id)
            }}
            style={[styles.widthspace, styles.buttonStyle, font.textWhiteColor]}
          >
            <Text style={[styles.buttonTextStyle, font.bold]}>Accept</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.declinePromotion(item.id, item.company_id)
            }}
            style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.borderbluecolor, styles.clrwhite]}
          >
            <Text style={[styles.buttonTextStyle, font.bold, styles.graytest]}>Decline</Text>
          </TouchableOpacity>
        </View> : <View>

        </View>}

      </View>
    )
  }



  getList = (item, index) => {

    this.setState({ reason: item.reason })

    const newArrData = this.state.category_list.map((e, index) => {

      if (item.id == e.id) {
        return {
          ...e,
          active: true
        }
      }
      return {
        ...e,
        active: false
      }
    })

    this.setState({ category_list: newArrData })
    console.log(this.state.category_list)

  }

  renderCategoryItem = ({ item, index }) => {
    return (
      <TouchableOpacity

        onPress={() => this.getList(item, index)}
        style={{ flexDirection: 'row', marginRight: 40, marginBottom: 10, marginTop: 10 }}>
        {item.active == false ? <Image style={{
          height: 20,
          width: 20,

        }} source={images.unselected_icon} />
          : <Image style={{
            height: 20,
            width: 20,

          }} source={images.selected_icon} />
        }
        <Text style={{
          alignSelf: 'center',
          marginLeft: 5,
          ...FONTS.regularsizeRegular,
          color: "#646464",
          fontWeight: '700'
        }}>{item.category}</Text>
      </TouchableOpacity>

    )

  }

  getTabWidthActive() {

    return {
      textAlign: "center",
      color: "#293272",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,

      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }
  getTabWidthInActive() {


    return {
      textAlign: "center",
      color: "#646464",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,

      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }

  renderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        {this.state.loader_visible ? <ActivityIndicator size="small" color="#aaa" /> : <View></View>}
      </View>
    )
  }

  getMessage = (item) => {
    this.props.navigation.navigate('Messages', {
      invite: "1",
      request: "0",
      online: item.online,
      profile: item.profile,
      id: item._id,
      chat_token: this.state.chat_token,
      mobile_number: item.mobileNumber,
      mymobile: this.state.mobile_number,
      name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

    })
  }

  blockContact = () => {
    let userdata = {
      token: this.state.chat_token,
      block_user: this.state.friend_number,
      blockStatus: true, // FOR BLOCK
      //  blockStatus: true, // FOR UNBLOCK
    }


    this.socket.emit("block_request", userdata);
    this.socket.once("block_response", (msg) => {
      this.setState({ block_dialog_visible: false })
      console.log(msg)
      Toast.show({
        title: '' + msg,
        text: "",
        color: '#000',
        timeColor: 'red',
        backgroundColor: COLORS.textblue.color,
        timing: 5000,
        position: 'bottom',
      })
      this.setState({ contact_list: [] })
      this.setState({ messageList: [] })
      this.setState({ request_list: [] })
      this.setState({ block_list: [] })

      this.startSocketConnection()
    });
  }

  unBlockContact = () => {

    let userdata = {
      token: this.state.chat_token,
      block_user: this.state.friend_number,
      blockStatus: false, // FOR BLOCK
      //  blockStatus: true, // FOR UNBLOCK
    }


    this.socket.emit("block_request", userdata);
    this.socket.once("block_response", (msg) => {
      this.setState({ unblock_dialog_visible: false })
      console.log(msg)
      Toast.show({
        title: '' + msg,
        text: "",
        color: '#000',
        timeColor: 'red',
        backgroundColor: COLORS.textblue.color,
        timing: 5000,
        position: 'bottom',
      })
      this.setState({ contact_list: [] })
      this.setState({ messageList: [] })
      this.setState({ request_list: [] })
      this.setState({ block_list: [] })
      this.startSocketConnection()
    });
  }

  reportContact = () => {
    let reportData = {
      token: this.state.chat_token,
      reporter: this.state.mobile_number,
      reported: this.state.friend_number,
      description: this.state.reason,
    };
    this.socket.emit("report_request", reportData);
    this.socket.on("report_response", (msg) => {
      this.setState({ report_dialog_visible: false })
      console.log(msg)
      Toast.show({
        title: '' + msg,
        text: "",
        color: '#000',
        timeColor: 'red',
        backgroundColor: COLORS.textblue.color,
        timing: 5000,
        position: 'bottom',
      })
      this.setState({ contact_list: [] })
      this.setState({ messageList: [] })
      this.startSocketConnection()
    })
  }

  loadMoreItem = () => {

    var current_page = this.state.page;
    var last_page = this.state.last_page

    if (current_page != last_page) {
      this.setState({ loader_visible: true })
      this.setState(state => ({ page: this.state.page + 1 }), () => this.getNotificationList())
    } else {
      this.setState({ loader_visible: false })
    }


  }

  searchPeople = () => {

    this.setState({ friend_list: [] })
    this.setState({ new_user: [] })
    if (this.state.search_keyword.length != 0) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {

          let config = {
            params: {
              key: this.state.search_keyword
            },
            auth: {
              username: "chat.aalavai.com",
              password: "c2db1fd389b27810d6f326c3e7acf790026f0621",
            },
          }
          axios.get("https://chat.meattend.com/api/search", config)
            .then((response) => {

              console.log(response.data)

              let list = response.data.data
              let temp = []
              var i;
              var s;
              for (i = 0; i < list.length; i++) {
                for (s = 0; s < this.state.contact_list.length; s++) {
                  if (list[i].mobileNumber == this.state.contact_list[s].mobileNumber) {
                    this.state.friend_list.push(list[i]);
                  }
                }
                this.state.contact_list.find((arr) => {
                  let value =
                    arr.mobileNumber == list[i].mobileNumber || list[i].mobileNumber == this.state.mobile_number;

                  this.state.new_user.push(value);
                })

                let check = this.state.new_user.every((arr) => arr == false);
                if (check == true) {

                  temp.push(list[i]);

                }
              }

              this.setState({ search_list: temp })

              console.log("friend", this.state.friend_list)
              console.log("new user", temp)

            })

        } else {


        }
      })
    }

  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return <View style={{ backgroundColor: '#fff' }}>
          <FlatList
            data={this.state.notification_list}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            ListFooterComponent={this.renderLoader}
            onEndReached={this.loadMoreItem}
            onEndReachedThreshold={0}
          />

        </View>
      case "second":
        return <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <View style={[styles.shadowbox, styles.searchSection]}>
            <TextInput numberOfLines={1}
              returnKeyType="search"
              style={[
                font.regular,
                font.bold,
                styles.inputStyle,
                styles.padding,

              ]}
              placeholder={
                "Search Users"
              }
              value={this.state.search_keyword}
              onSubmitEditing={() => {

                if (this.state.search_keyword.length != 0) {
                  this.searchPeople()
                }




              }}

              onChangeText={(searchString) => {
                this.setState({ search_keyword: searchString })


                if (searchString.length == 0) {
                  this.setState({ search_list: [] })
                  this.setState({ friend_list: [] })
                }
              }}
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              onPress={() => {
                if (this.state.search_keyword.length != 0) {
                  this.searchPeople()
                }
              }}
              style={{ width: 50, marginRight: 5, justifyContent: 'center', alignItems: 'center' }}>
              <Image

                style={{ width: 20, height: 20 }}
                source={images.search_icon}
              />
            </TouchableOpacity>

          </View>

          <ScrollView contentContainerStyle={{ paddingBottom: 60 }}>


            <View>
              {this.state.friend_list.length != 0 ? <Text style={{ padding: 15, ...FONTS.regularsizeLarge, color: COLORS.textblue.color }}>
                Friend List
              </Text> : <View></View>
              }
              {

                this.state.friend_list.map((item, index) => (

                  <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1 }}>
                    <TouchableOpacity
                      onPress={() => {

                        this.setState({ search_keyword: "" })
                        this.setState({ search_list: [] })
                        this.props.navigation.navigate('Messages', {
                          contact: 0,
                          blocked: 0,
                          wait: 0,
                          online: item.online,
                          profile: item.profile,
                          id: item._id,
                          mymobile: this.state.mobile_number,
                          mobile_number: item.mobileNumber,
                          name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                        })

                      }}
                      style={[styles.rowgroup, styles.item, styles.noborder]}>
                      <View>
                        <Image source={{ uri: item.profile }}
                          style={styles.itemimg}
                        />

                        <View style={{
                          resizeMode: "contain",
                          width: 12,
                          height: 12,
                          position: "absolute",
                          right: 5,
                          bottom: 0,
                          backgroundColor: item.online == true ? "#42f548" : "#ffd700",
                          borderRadius: 10
                        }}>

                        </View>


                      </View>
                      <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
                        {
                          item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                            {item.companyName}

                          </Text> :
                            <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                              {item.firstName} {item.lastName}

                            </Text>
                        }



                      </View>


                    </TouchableOpacity>
                  </View>


                ))
              }


              {this.state.search_list.length != 0 ? <Text style={{ padding: 15, ...FONTS.regularsizeLarge, color: COLORS.textblue.color }}>
                Meattend Directory
              </Text> : <View></View>
              }


              {

                this.state.search_list.map((item, index) => (

                  <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1 }}>
                    <TouchableOpacity
                      onPress={() => {

                        this.setState({ search_keyword: "" })
                        this.setState({ search_list: [] })
                        this.props.navigation.navigate('Messages', {
                          contact: 0,
                          blocked: 0,
                          wait: 0,
                          online: item.online,
                          profile: item.profile,
                          id: item._id,
                          mymobile: this.state.mobile_number,
                          mobile_number: item.mobileNumber,
                          name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                        })

                      }}
                      style={[styles.rowgroup, styles.item, styles.noborder]}>
                      <View>
                        <Image source={{ uri: item.profile }}
                          style={styles.itemimg}
                        />

                        <View style={{
                          resizeMode: "contain",
                          width: 12,
                          height: 12,
                          position: "absolute",
                          right: 5,
                          bottom: 0,
                          backgroundColor: item.online == true ? "#42f548" : "#ffd700",
                          borderRadius: 10
                        }}>

                        </View>


                      </View>
                      <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
                        {
                          item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                            {item.companyName}

                          </Text> :
                            <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                              {item.firstName} {item.lastName}

                            </Text>
                        }



                      </View>


                    </TouchableOpacity>
                  </View>


                ))
              }


              {this.state.search_keyword == 0 ? <View>
                {

                  this.state.contact_list.map((item, index) => (

                    <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1 }}>
                      {item.contact == true ? <Swipeable rightButtons={this.rightButtons(item)}>
                        <TouchableOpacity
                          onPress={() => {

                            this.props.navigation.navigate('Messages', {
                              contact: 1,
                              request: 0,
                              online: item.online,
                              blocked: 0,
                              invite: 0,
                              profile: item.profile,
                              id: item._id,
                              mymobile: this.state.mobile_number,
                              mobile_number: item.mobileNumber,
                              name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                            })

                          }}
                          style={[styles.rowgroup, styles.item, styles.noborder]}>
                          <View>
                            <Image source={{ uri: item.profile }}
                              style={styles.itemimg}
                            />

                            <View style={{
                              resizeMode: "contain",
                              width: 12,
                              height: 12,
                              position: "absolute",
                              right: 5,
                              bottom: 0,
                              backgroundColor: item.online == true ? "#42f548" : "#ffd700",
                              borderRadius: 10
                            }}>

                            </View>

                          </View>
                          <Text
                            style={{ color: "#747474", fontSize: 14, lineHeight: 19, fontSize: 14, opacity: 0.6, position: 'absolute', right: 22 }}
                          >{moment.utc(item.orderTime).local().startOf('seconds').fromNow()}</Text>

                          <View style={{ marginRight: 40, position: "relative", flex: 1, alignSelf: "center" }}>

                            {
                              item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                {item.companyName}

                              </Text> :
                                <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                  {item.firstName} {item.lastName}

                                </Text>
                            }
                            {item.count != 0 && item.count != null ? <Text style={{ color: "#56D5BD", fontSize: 14, marginTop: 10 }}>
                              {item.count} New Message

                            </Text> :
                              <View style={{ flexDirection: 'row' }}>

                                {item.lastMessage != null ? <View>
                                  {item.lastMessage.message != "" ?


                                    <Text
                                      numberOfLines={1}
                                      style={{ color: "#747474", fontSize: 14, marginTop: 10 }}>
                                      {item.lastMessage.message}

                                    </Text> : <View>
                                      {
                                        item.lastMessage.fileType.length != 0 ?
                                          <View>
                                            {
                                              item.lastMessage.fileType[item.lastMessage.fileType.length - 1].split('/')[0] == "image" ?
                                                <View style={{ flexDirection: 'row' }}><Image
                                                  source={images.image_attachment_icon}
                                                  style={{ width: 14, height: 14, marginLeft: 3, marginTop: 12, tintColor: COLORS.lightgray.color }} />
                                                  <Text style={{ color: "#747474", fontSize: 14, marginTop: 10, marginLeft: 3 }}>Photo</Text>
                                                </View>
                                                :
                                                <View>
                                                  {item.lastMessage.fileType[item.lastMessage.fileType.length - 1].split('/')[0] == "video" ?
                                                    <View style={{ flexDirection: 'row' }}>
                                                      <Image
                                                        resizeMode='contain'
                                                        source={images.video_attachment_icon}
                                                        style={{ width: 14, height: 14, marginLeft: 3, marginTop: 12, tintColor: COLORS.lightgray.color }} />
                                                      <Text style={{ color: "#747474", fontSize: 14, marginTop: 10, marginLeft: 3 }}>Video</Text>
                                                    </View> :
                                                    <View style={{ flexDirection: 'row' }}>
                                                      <Image
                                                        resizeMode='contain'
                                                        source={images.document_attachment_icon}
                                                        style={{ width: 14, height: 14, marginLeft: 3, marginTop: 12, tintColor: COLORS.lightgray.color }} />
                                                      <Text style={{ color: "#747474", fontSize: 14, marginTop: 10, marginLeft: 3 }}>file</Text>
                                                    </View>}
                                                </View>

                                            }
                                          </View> :
                                          <View></View>
                                      }
                                    </View>
                                  }
                                </View> : <View></View>}



                              </View>

                            }


                          </View>


                        </TouchableOpacity>
                      </Swipeable> :
                        <TouchableOpacity
                          onPress={() => {
                            if (item.request == true) {
                              this.props.navigation.navigate('Messages', {
                                invite: 0,
                                online: item.online,
                                contact: 1,
                                blocked: 0,
                                wait: 1,
                                profile: item.profile,
                                id: item._id,
                                mymobile: this.state.mobile_number,
                                mobile_number: item.mobileNumber,
                                name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                              })
                            }

                            if (item.invite == true) {
                              this.props.navigation.navigate('Messages', {
                                invite: "1",
                                request: "0",
                                online: item.online,
                                profile: item.profile,
                                id: item._id,
                                chat_token: this.state.chat_token,
                                mobile_number: item.mobileNumber,
                                mymobile: this.state.mobile_number,
                                name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                              })
                            }


                          }}
                          style={[styles.rowgroup, styles.item, styles.noborder]}>
                          <View>
                            <Image source={{ uri: item.profile }}
                              style={styles.itemimg}
                            />

                            <View style={{
                              resizeMode: "contain",
                              width: 12,
                              position: "absolute",
                              right: 5,
                              bottom: 0,
                              height: 12,
                              backgroundColor: item.online == true ? "#42f548" : "#ffd700",
                              borderRadius: 10
                            }}>

                            </View>

                          </View>
                          {item.count != 0 && item.count != null ? <Text
                            style={{ color: "#747474", fontSize: 14, lineHeight: 19, fontSize: 14, opacity: 0.6, position: 'absolute', right: 22 }}
                          >{moment.utc(item.updatedAt).local().startOf('seconds').fromNow()}</Text>
                            : <View></View>
                          }
                          <View style={{ marginRight: 40, position: "relative", flex: 1, alignSelf: "center" }}>

                            {
                              item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                {item.companyName}

                              </Text> :
                                <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                  {item.firstName} {item.lastName}

                                </Text>
                            }
                            {item.count != 0 && item.count != null ? <Text style={{ color: "#56D5BD", fontSize: 14, marginTop: 10 }}>
                              {item.count} New Message

                            </Text> : <View></View>}


                          </View>


                        </TouchableOpacity>
                      }
                    </View>


                  ))
                }
              </View> : <View></View>}




              {this.state.search_keyword == 0 ? <View>
                {

                  this.state.messageList.map((item, index) => (
                    <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1 }}>

                      <TouchableOpacity
                        onPress={() => {

                          this.getMessage(item)

                        }}
                        style={[styles.rowgroup, styles.item, styles.noborder]}>
                        <View>
                          <Image source={{ uri: item.profile }}
                            style={styles.itemimg}
                          />

                          <View style={{
                            resizeMode: "contain",
                            width: 12,
                            position: "absolute",
                            right: 5,

                            bottom: 0,
                            height: 12,
                            backgroundColor: item.online == true ? "#42f548" : "#ffd700",
                            borderRadius: 10
                          }}>

                          </View>

                        </View>
                        <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
                          {
                            item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                              {item.companyName}

                            </Text> :
                              <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                {item.firstName} {item.lastName}

                              </Text>
                          }


                        </View>


                      </TouchableOpacity>

                    </View>

                  ))
                }

              </View> : <View></View>}



              {this.state.search_keyword == 0 ? <View>
                {

                  this.state.block_list.map((item, index) => (
                    <View style={{ borderBottomColor: COLORS.lightgraycolor.color, borderBottomWidth: 1 }}>
                      <Swipeable rightButtons={this.block_listrightButtons(item)}>
                        <TouchableOpacity
                          onPress={() => {

                            this.props.navigation.navigate('Messages', {
                              contact: 1,
                              blocked: 1,

                              online: item.online,
                              profile: item.profile,
                              id: item._id,
                              mymobile: this.state.mobile_number,
                              mobile_number: item.mobileNumber,
                              name: item.companyUser == true ? item.companyName : item.firstName + " " + item.lastName

                            })

                          }}
                          style={[styles.rowgroup, styles.item, styles.noborder]}>
                          <View>
                            <Image source={{ uri: item.profile }}
                              style={styles.itemimg}
                            />


                          </View>
                          <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
                            {
                              item.companyUser == true ? <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                {item.companyName}

                              </Text> :
                                <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>
                                  {item.firstName} {item.lastName}

                                </Text>
                            }



                          </View>


                        </TouchableOpacity>
                      </Swipeable>
                    </View>

                  ))
                }
              </View> : <View></View>}

            </View>




          </ScrollView>
        </View>
    }
  }

  updateSearch = (search) => {
    this.setState({ search });
  };



  render() {
    const { index, routes } = this.state
    const { search } = this.state;



    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width, bottom: 40 }}>

        </Root>
        <View style={{ flex: 1, backgroundColor: "#fff", paddingTop: 30 }}>


          <TabView
            swipeEnabled={false}
            navigationState={{ index, routes }}
            renderScene={this.renderScene}
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ initialLayout }}
            indicatorStyle={{ backgroundColor: '#56D6BE' }}
            activeColor={{ color: "#ff" }}
            indicatorContainerStyle={{ backgroundColor: "#fff" }}
            style={[font.regular, font.sizeSmall]}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                renderLabel={({ route, focused, color }) => (
                  <Animated.Text
                    numberOfLines={1}
                    style={
                      focused
                        ? this.getTabWidthActive()
                        : this.getTabWidthInActive()
                    }
                  >
                    {route.title}
                  </Animated.Text>
                )}
                getLabelText={({ route: { title } }) => title}
                indicatorStyle={styles.indicator}
                tabStyle={[styles.tabStyle, font.semibold]}
                style={[styles.tab, font.semibold]}
              />
            )}

          />

        </View>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.block_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ block_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 150,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Block this contact?
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.blockContact()
                    }}
                    style={styles.attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Block
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.attend_desable_buttonStyle}
                    onPress={() => this.setState({ block_dialog_visible: false })}
                  >
                    <Text style={{

                      paddingRight: 10,
                      paddingLeft: 10,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Cancel
                    </Text>
                  </TouchableOpacity>


                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.unblock_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ unblock_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 200,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Warning
                </Text>
                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.regularsizeMedium }}>
                  Unblock this contact?
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={this.unBlockContact}
                    style={styles.attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Yes
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.attend_desable_buttonStyle}
                    onPress={() => this.setState({ unblock_dialog_visible: false })}
                  >
                    <Text style={{

                      paddingRight: 10,
                      paddingLeft: 10,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      No
                    </Text>
                  </TouchableOpacity>


                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.report_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ report_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 300,
                maxHeight: 400,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{

                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Report this contact?
                </Text>
                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Select a reason
                </Text>

                {this.state.category_list.map((item, index) => (
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.getList(item, index)}
                    style={{ flexDirection: 'row', marginRight: 10, marginLeft: 10, marginBottom: 10, marginTop: 10 }}>
                    {!item.active ? <Image style={{
                      height: 20,
                      width: 20,

                    }} source={images.unselected_icon} />
                      : <Image style={{
                        height: 20,
                        width: 20,

                      }} source={images.selected_icon} />
                    }
                    <Text style={{
                      alignSelf: 'center',
                      marginLeft: 5,
                      marginRight: 25,
                      ...FONTS.regularsizeRegular,
                      color: "#646464",
                      fontWeight: '700'
                    }}>{item.reason}</Text>
                  </TouchableOpacity>

                ))}
                <View
                  style={{

                    position: 'absolute',
                    bottom: 10,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    width: "100%"
                  }}
                >
                  <TouchableOpacity
                    onPress={this.reportContact}
                    style={styles.attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Report
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.attend_desable_buttonStyle}
                    onPress={() => this.setState({ report_dialog_visible: false })}
                  >
                    <Text style={{

                      paddingRight: 10,
                      paddingLeft: 10,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Cancel
                    </Text>
                  </TouchableOpacity>


                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.discount_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ discount_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 300,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Event QRCode
                </Text>
                <View style={{ height: 150, alignItems: "center", justifyContent: "center" }}>
                  <QRCode
                    value={String(this.state.discount)}

                    logoBackgroundColor='transparent'
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ discount_dialog_visible: false })}
                    style={styles.dialog_attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Close
                    </Text>
                  </TouchableOpacity>



                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>

      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  Attendwidthspace: {

  },

  dialog_attend_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,


  },

  attend_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,


  },
  attend_desable_buttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: 'center',
    marginBottom: 60
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  nospace: {
    marginBottom: 0
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  graytest: {
    color: "#293272",

  },
  searchSection: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#fff',
    height: 50,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
    flexDirection: 'row'
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  searchIcon: {
    padding: 10,
  },
  clrwhite: {
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#293272",
    marginLeft: 16
  },
  widthspace: {
    flex: 1
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "cover",
    height: 50,
    width: 50,
    marginRight: 10,
    borderRadius: 12,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,

    flex: .5,
    marginTop: 10,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 5,
    alignSelf: "center",
  },
  padding: {
    flex: 1,
    padding: 0
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 15,
    paddingTop: 0,
    marginVertical: 8,
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    position: "relative"
  },
  noborder: {
    borderBottomWidth: 0,
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
    top: 15
  },
  boxsearchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
    top: 15
  },

});
