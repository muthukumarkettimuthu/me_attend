// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import { ActivityIndicator } from 'react-native-paper';
import React, { Component, useState } from 'react';
import Icon from "react-native-vector-icons/FontAwesome";


import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import GridImageView from 'react-native-grid-image-viewer';
import ReadMore from 'react-native-read-more-text';
import RBSheet from "react-native-raw-bottom-sheet";

import SwiperFlatList from 'react-native-swiper-flatlist';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  useWindowDimensions,
  FlatList,
  SafeAreaView,
  KeyboardAvoidingView,
  Modal,
  Animated,
  Dimensions,
  TouchableOpacity,
  Button,
} from 'react-native';
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-player';
import { ImageBackground } from 'react-native';
import ImageView from "react-native-image-viewing";
import CameraRollGallery from "react-native-camera-roll-gallery";
import Gallerys from './gallery';
import NetInfo from "@react-native-community/netinfo";
import Loader from './Components/Loader';
import font from './styles/font';
import { images, COLORS, SIZES, FONTS } from '../assets/styles/theme';
import ApiConstants from '../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import { height } from 'dom-helpers';
import { Root, Toast } from 'react-native-popup-confirm-toast'
var pos = 0

const imagelist = [
  {
    uri: "https://images.unsplash.com/photo-1571501679680-de32f1e7aad4"
  },
  {
    uri: "https://images.unsplash.com/photo-1573273787173-0eb81a833b34"
  },
  {
    uri: "https://images.unsplash.com/photo-1569569970363-df7b6160d111"
  }
];

// image and video




const initialLayout = { width: Dimensions.get("window").width }
const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    eventtitle: 'Purple Friday',
    eventdate: '10pm-3am | 11/01/2021',
    city: 'District Sky Lounge',
    address: '4th Floor,dsjbfjdsb fdvfd dfvfdv cdcd ho...'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    eventtitle: 'Purple Friday',
    eventdate: '10pm-3am | 11/01/2021',
    city: 'District Sky Lounge',
    address: '4th Floor,dsjbfjdsb fdvfd dfvfdv cdcd ho...'
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    eventtitle: 'Purple Friday',
    eventdate: '10pm-3am | 11/01/2021',
    city: 'District Sky Lounge',
    address: '4th Floor,dsjbfjdsb fdvfd dfvfdv cdcd ho...'
  },
];






export default class PeopleScreen extends Component {

  constructor(props) {

    const containerStyle = { backgroundColor: 'white', padding: 20 };
    super(props);
    this.state = {
      user_id: props.route.params.user_id,
      modalVisible: false,
      search: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Photos' },
        { key: 'second', title: 'Attend' },
      ],
      dialog_visible: false,
      dialog_message: "",
      loader_visible: false,
      attend_list: [],
      image_list: [],
      grid_list: [],
      video_list: [],
      age: "",
      city: "",
      country: "",
      email_id: "",
      name: "",
      followers: "",
      following: "",
      gender: "",
      id: "",
      mobile_number: "",
      profile_image: "",
      event_loader_visible: true,
      page: 1,
      event_last_page: 0,
      show_preview: false,
      age: "",
      position: 0,
      play_button_visible: true,
      follow_label: "Follow",
      size_flag: 0


    }
    this.flatListRef = null;

  }

  componentDidMount = () => {
    this.getProfile()

  }





  getProfile = async () => {
    this.setState({ grid_list: [] })
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "view-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            user_id: this.state.user_id
          })
        }).then(response => response.json())
          .then(responseJson => {


            let age = responseJson.data.age;
            let gender = responseJson.data.gender;
            let email_id = responseJson.data.email_id;
            let mobile_number = responseJson.data.mobile_number;
            const namevalu = responseJson.data.first_name + " " + responseJson.data.last_name
            let followers = responseJson.data.followers;
            let following = responseJson.data.following;
            let profile_image = responseJson.data.profile_image;
            let is_following = responseJson.data.is_following.length
            if (is_following == 0) {
              this.setState({ follow_label: "Follow" })
            } else {
              this.setState({ follow_label: "Unfollow" })
            }
            let list = []
            list = responseJson.data.gallery
            this.storeToken(email_id, mobile_number)
            this.setState({ profile_image: profile_image })
            console.debug("pp", responseJson.data)
            this.setState({ name: namevalu })
            this.setState({ age: "" + age })
            this.setState({ gender: gender })
            this.setState({ followers: followers })
            this.setState({ following: following })
            this.setState({ loader_visible: false })
            this.setState({ image_list: responseJson.data.gallery })

            let image_list = responseJson.data.gallery;
            console.log()
            if (image_list.length == 0) {
              this.setState({ flag: 0 })
            } else {
              this.setState({ flag: 1 })
            }

            var i = 0


            for (i = 0; i < list.length; i++) {
              if (list[i].type == 0) {
                this.state.grid_list.push(ApiConstants.path + "uploads/users-gallery/" + list[i].attachement_link)

              } else {

              }

              // console.debug("prof", this.state.grid_list)
            }
          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  storeToken = (email_id, mobile_number) => {
    AsyncStorage.setItem("email_id", email_id)
    AsyncStorage.setItem("mobile_number", mobile_number)
    console.log(email_id + ", " + mobile_number)
  }

  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };
  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={[font.sizeVeryRegular, font.textblue, font.bold]} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={[font.sizeVeryRegular, font.textblue, font.bold]} onPress={handlePress}>
        Show less
      </Text>
    );
  }

  _handleTextReady = () => {
    // ...
  }
  getTabWidthActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);
    return {
      textAlign: "center",
      color: "#293272",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "regular",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }
  getTabWidthInActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);

    return {
      textAlign: "center",
      color: "#646464",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "bold",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }

  renderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        {this.state.event_loader_visible ? <ActivityIndicator size="small" color="#aaa" /> : <View></View>}
      </View>
    )
  }

  loadMoreItem = () => {

    var current_page = this.state.page;
    var last_page = this.state.event_last_page
    // alert(current_page + "," + last_page)
    if (current_page != last_page) {
      this.setState({ event_loader_visible: true })
      this.setState(state => ({ page: this.state.page + 1 }), () => this.getAttentEventList())
    } else {
      this.setState({ event_loader_visible: false })
    }


  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return <View style={{
          marginTop: 10, backgroundColor: '#fff',
          justifyContent: 'center',
          alignItems: 'center',
          width: SIZES.width,
          marginBottom: 50
        }}>



          {/* <GridImageView data={this.state.grid_list} /> */}
        </View>
      case "second":
        return <View style={{ flex: 1, backgroundColor: '#fff', }}>
          <FlatList

            data={this.state.attend_list}
            renderItem={this.renderItem2}
            keyExtractor={item => item.id}
            ListFooterComponent={this.renderLoader}
            onEndReached={this.loadMoreItem}
          />
        </View>
    }
  }

  updateSearch = (search) => {
    this.setState({ search });
  };

  followCompany = async () => {
    var token = await AsyncStorage.getItem("user_id")
    console.log(token)
    if (this.state.follow_label == "Follow") {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })

          fetch(ApiConstants.url + "user-follow", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              follower_id: this.state.user_id
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.log(responseJson)
              const status = responseJson.status
              this.getProfile()

              // alert(JSON.stringify(responseJson))
              this.setState({ loader_visible: false })


            }).catch(error => {
              alert("ree" + responseJson)
              this.setState({ loader_visible: false })


            })
        } else {


        }
      })
    } else {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })

          fetch(ApiConstants.url + "user-unfollow", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              follower_id: this.state.user_id
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.log(responseJson)
              const status = responseJson.status
              this.getProfile()

              // alert(JSON.stringify(responseJson))
              this.setState({ loader_visible: false })


            }).catch(error => {
              alert("ree" + responseJson)
              this.setState({ loader_visible: false })


            })
        } else {


        }
      })
    }


  }


  chooseImages = () => {
    ImagePicker.openPicker({
      mediaType: "photo",
      width: 300,
      height: 300,
    }).then(images => {
      console.log(images);
      ImgToBase64.getBase64String(images.path)
        .then(base64String => {
          // console.log(base64String);
          this.uploadImage(base64String)
        })
        .catch(err => {
          console.log(err);
        });
    }).catch(error => {
      console.log(error);
    });
  }

  uploadImage = async (base64String) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "profile-image-upload", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            profile_image: "data:image/png;base64," + base64String
          })
        }).then(response => response.json())
          .then(responseJson => {


            // console.debug("up", responseJson)
            this.getProfile()
            this.setState({ loader_visible: false })

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }


  renderItem = ({ item, index }) => {
    return (
      <View style={{ marginHorizontal: 2, marginTop: 5 }}>

        {item.type == 0 ?
          <TouchableOpacity onPress={() => {
            this.setState({ show_preview: true })

            try {
              setTimeout(() => this.flatListRef.scrollToIndex({ index: index }), 1000);
              pos = index
              this.setState({ position: index })
            } catch (error) {
              console.warn(error);
            }
          }}>
            <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
              style={styles.galleryitemimg}
            />
          </TouchableOpacity>

          :

          <TouchableOpacity

            onPress={() => {
              this.setState({ show_preview: true })

              try {
                setTimeout(() => this.flatListRef.scrollToIndex({ index: index, animated: true }), 1000);
                this.setState({ position: index })
                pos = index
              } catch (error) {
                console.warn(err);
              }
            }}
          >
            <VideoPlayer

              video={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
              autoplay={false}
              defaultMuted={true}
              customStyles={{ playButton: { height: 0, width: 0 } }}
              style={styles.galleryitemvideo}
            />
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={images.default_play_button_icon}
                style={{
                  height: 17,
                  width: 17,
                  tintColor: 'white'
                }} />
            </View>


          </TouchableOpacity>


        }




      </View>)
  }
  renderItem2 = ({ item }) => {
    return (


      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('SettingScreen', { event_id: item.id })}
        style={[styles.rowgroup, styles.item, styles.shadowbox]}>
        <View>
          <Image source={{ uri: ApiConstants.path + "images/events/" + item.image }}
            style={styles.itemimg}
          />
        </View>
        <View style={{ marginRight: 20, position: "relative", flex: 1 }}>
          <Text style={{ color: "#333333", fontSize: 16, ...FONTS.regularsizeMedium, marginBottom: 4, fontWeight: 'bold' }}>
            {item.event_name}
          </Text>
          <Text style={{ color: "#747474", fontSize: 11, marginBottom: 8 }} numberOfLines={1}>
            {item.from_time}-{item.to_time} | {item.date_time.split("-").reverse().join("/")}
          </Text>
          <Text style={{ color: "#747474", fontSize: 14, fontWeight: 'bold', ...FONTS.regularsizeRegular }} numberOfLines={1}> {item.company_name}</Text>

          <View style={{ paddingTop: 8, flexDirection: "row", paddingRight: 30 }} >
            <Image
              source={images.location_icon}
              resizeMode="stretch"
              style={{
                height: 13,
                width: 13,
                marginRight: 2,
                alignSelf: "center",
                tintColor: COLORS.textgreen.color
              }}
            />

            <Text style={{ color: "#747474", fontSize: 12, }, font.regular} numberOfLines={1}> {item.event_address}</Text>

          </View>
          <View>

            {item.attend_status == "attending" ? <View style={{ flexDirection: "row", borderWidth: 1.5, borderColor: "#293272", borderRadius: 8, width: 100, paddingVertical: 6, justifyContent: "center", marginTop: 10 }}>
              <Image
                source={images.tick_icon} resizeMode="contain"
                style={{
                  marginRight: 5,
                  alignSelf: "center",
                  height: 15,
                  width: 15
                }} />
              <Text style={[font.textblue, font.bold]}>Attending</Text>
            </View> :
              <TouchableOpacity
                onPress={() => this.attendEvent(item.id)}
                style={styles.attendedbuttonStyle}
              >
                <Text style={[styles.buttonTextStyle, font.bold]}>Attended</Text>
              </TouchableOpacity>}
          </View>

        </View>
        <View style={{
          position: "absolute",
          right: 10,
          top: 10
        }}>
          <Image source={images.menu_icon}
            style={
              {
                resizeMode: "contain",
                height: 20,
                width: 20,
                tintColor: COLORS.graycolor.color

              }
            }
          />
        </View>

      </TouchableOpacity>

    )
  }
  scrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.flatListRef.scrollToOffset({ offset });
    setTimeout(() => this.flatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;


    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

        </Root>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>
          <View style={[styles.rowgroup, styles.header, styles.centercontent]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{ position: "absolute", left: 5 }}>
              <Image source={images.prevarrow_icon}
                style={{ resizeMode: "contain", height: 15, width: 9 }}
              />

            </TouchableOpacity>
            <View style={{ position: "relative", justifyContent: "center" }}>
              <Text style={[font.bold, font.textblue, font.sizeLarge]}>View Profile</Text>
            </View>



          </View>
          <View style={[styles.rowgroup, styles.item,]}>
            <View>

              {this.state.profile_image != null ?
                <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + this.state.profile_image }}
                  style={styles.itemimg}
                /> :
                <Image source={{ uri: ApiConstants.path + "images/users/profile_image/profile.png" }}
                  style={styles.itemimg}
                />
              }



            </View>
            <View style={{ position: "relative", flex: 1 }}>
              <View
                style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <View
                  style={{ flexDirection: "column", width: "60%" }}
                >
                  <Text style={[font.sizeMedium, font.blackcolor, font.bold, styles.textStyle]}>
                    {this.state.name}

                  </Text>
                  <Text style={{ ...FONTS.semiboldsizeRegular }}>
                    {this.state.age},{this.state.gender}

                  </Text>
                </View>



              </View>
              <ReadMore
                numberOfLines={3}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
                onReady={this._handleTextReady}>
                {/* <Text style={[font.sizeVeryRegular, font.graycolor, font.regular, styles.rowgroup, font.linevertical]}>
                  An Artist, Loves Sushi, Up for thrilling adventures and unforgettable times. Love to party 24x7
                </Text> */}
              </ReadMore>
            </View>

          </View>
          <View style={{ flexDirection: "row", paddingHorizontal: 20, paddingBottom: 15 }}>
            <TouchableOpacity
              onPress={this.followCompany}
              style={[styles.widthspace, this.state.follow_label == "Follow" ? styles.buttonStyle : styles.unFollowButtonStyle]}
            >
              <Text style={[this.state.follow_label == "Follow" ? styles.buttonTextStyle : styles.unFollowbuttonTextStyle, font.bold, font.sizeRegular]}>{this.state.follow_label}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('people_follow', { user_id: this.state.user_id })}
              style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.lightbg, font.borderradiustyle]}
            >
              <Text style={[styles.buttonTextStyle, font.regular, font.graycolor, font.sizeVeryRegular]}>{this.state.followers} Followers</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('people_following', { user_id: this.state.user_id })}
              style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.lightbg, font.borderradiustyle]}
            >
              <Text style={[styles.buttonTextStyle, font.regular, font.graycolor, font.sizeVeryRegular]}>{this.state.following} Following</Text>
            </TouchableOpacity>
          </View>
          {this.state.flag == 0 ? <View style={{
            height: 50,
            marginLeft: 20,
            marginTop: 20,

            marginRight: 20,
            backgroundColor: "#FFF6C6",
            borderRadius: 5,
            borderColor: "#FEF1AF",

            justifyContent: 'center',
            paddingLeft: 8
          }}>
            <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No data found</Text>
          </View> :
            <FlatList
              numColumns={3}
              contentContainerStyle={{ paddingBottom: 50, paddingLeft: 10, paddingRight: 10 }}
              data={this.state.image_list}
              renderItem={this.renderItem}
              keyExtractor={item => item.gallery_id}
            />}



        </View>
        <Modal
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.onCloseModel();
          }}

        >
          <View style={{ backgroundColor: "#fff", minHeight: 300, margin: 15, borderRadius: 10, padding: 25 }}>
            <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
            <Text style={[styles.smalltitle, font.bold]}>Location</Text>
            <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ flex: 1, paddingRight: 20 }}>
                <TextInput
                  style={[font.regular, styles.inputStyle, styles.shadowbox]}
                  underlineColorAndroid="#f000"
                  placeholder="London, UK"
                  placeholderTextColor="#747474"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  blurOnSubmit={false}
                />
              </View>
              <View>
                <Text style={[font.bold, styles.changetxt]}>Change</Text>
              </View>
            </View>
            <Text style={[styles.smalltitle, font.bold]}>Time</Text>
            <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ flex: 1, paddingRight: 20 }}>
                <TextInput
                  style={[font.regular, styles.inputStyle, styles.shadowbox]}
                  underlineColorAndroid="#f000"
                  placeholder="Today,Noon"
                  placeholderTextColor="#747474"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  blurOnSubmit={false}
                />
              </View>
              <View>
                <Text style={[font.bold, styles.changetxt]}>Change</Text>
              </View>
            </View>
          </View>
        </Modal>
        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }

        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.show_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.flatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.scrollToIndexFailed.bind(this)}
              data={this.state.image_list}
              renderItem={({ item }) => (
                <View>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {item.type == 0 ?
                      <ImageBackground source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }} resizeMode="contain"
                        style={{
                          height: SIZES.height / 1.1,
                          marginVertical: 20,
                          width: SIZES.width,
                          borderRadius: 8
                        }}
                      /> :
                      <View>
                        <VideoPlayer
                          ref={r => this.player = r}
                          video={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
                          autoplay={false}
                          defaultMuted={true}
                          disableControlsAutoHide
                          videoHeight={300}

                          style={{

                            backgroundColor: 'black',
                            alignSelf: 'center',
                            height: 300,
                            width: SIZES.width,

                          }}
                        />


                      </View>

                    }

                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ show_preview: false })
              }}
              style={{

                top: 30,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.image_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.image_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.image_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>

      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  header: {

    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    marginBottom: 10,

  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  attendedbuttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,
    width: 100,

  },
  topspace: {
    top: -20,
  },
  bottommargin: {
    marginBottom: 20
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  textStyle: {
    width: "100%"
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  searchIcon: {
    padding: 10,
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: 'center',
    marginBottom: 60
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "cover",
    height: 140,
    width: 140,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start",
    borderRadius: 10,
    borderWidth: 1
  },
  galleryitemimg: {
    resizeMode: "cover",
    height: 120,
    width: 120,

    alignSelf: "flex-start",
    justifyContent: "flex-start",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  galleryitemvideo: {

    height: 120,
    width: 120,

    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  unFollowButtonStyle: {
    height: 33,
    borderWidth: 1.5,
    borderColor: "#293272",
    borderRadius: 8,

    marginTop: -7,
    justifyContent: "center",
    width: 100,
    marginBottom: 0,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: -7,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  unFollowbuttonTextStyle: {
    color: '#293272',
    fontWeight: "bold",
    fontSize: 14,

    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 6,
    marginVertical: 8,
    marginHorizontal: 14,
    marginBottom: 20,
    borderRadius: 8
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
