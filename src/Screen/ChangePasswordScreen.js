
import React, { useState} from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    Modal,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';



import Loader from './Components/Loader';
import font from './styles/font';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';
import ApiConstants from '../services/APIConstants';


const ChangePasswordScreen = ({ navigation, route }) => {

    const [new_password, setNewPassword] = useState("");
    const [confirm_password, setConfirmPassword] = useState("");
    var password_reg = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    const [dialog_visible, setDialogVisible] = useState(false);
    const [dialog_message, setDialogMessage] = useState("");
    const [loader_visible, setLoaderVisibility] = useState(false);
    const [token, setToken] = useState(route.params.token)
    const [password_validation_msg, setPasswordValidationVisible] = useState(false)
    const [confirm_password_validation_msg, setConfirmPasswordValidationVisible] = useState(false)
    console.log(token)
    const changePassword = () => {

        if (password_reg.test(new_password) == false) {
            setPasswordValidationVisible(true)

        } else if (confirm_password != new_password) {
            setConfirmPasswordValidationVisible(true)
        } else {
            setLoaderVisibility(true)
            fetch(ApiConstants.url + "change-password", {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer' + token
                },
                body: JSON.stringify({
                    password: new_password,
                    password_confirmation: confirm_password
                })
            }).then(response => response.json())
                .then(responseJson => {
                    console.debug("sendEmal", responseJson)
                    const status = responseJson.status
                    if (status == "success") {
                        setDialogVisible(true);
                        setDialogMessage(responseJson.message)
                        navigation.navigate('LoginScreen')
                        setLoaderVisibility(false)
                    } else {
                        setLoaderVisibility(false)
                        setDialogVisible(true);
                        setDialogMessage(responseJson.value)
                    }
                })
        }






    }

    return (
        <View style={styles.mainBody}>
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}><View style={{ alignItems: 'center', position: 'absolute', top: 0, flex: 1, left: 0, right: 0 }}>
                    <Image
                        source={images.large_logo_icon}
                        style={{
                            resizeMode: "contain",
                            margin: 30,
                            height: 30
                        }}
                    />
                </View>

                <View>
                    <KeyboardAvoidingView enabled>
                        <Text
                            style={[
                                font.bold,
                                styles.HeadText
                            ]}
                        >
                            Change Password
                        </Text>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={[styles.inputStyle, font.regular]}
                                onChangeText={(text) => {
                                    setNewPassword(text)
                                    setPasswordValidationVisible(false)
                                }}
                                placeholder="Enter New Password" //dummy@abc.com
                                placeholderTextColor="#747474"
                                autoCapitalize="none"
    
                                returnKeyType="next"
                                underlineColorAndroid="#f000"
                                blurOnSubmit={false}
                                secureTextEntry={true}
                            />
                        </View>
                        {
                            password_validation_msg ?
                                <View style={styles.validationText}>
                                    <Text style={{ color: 'red', ...FONTS.regularfont11 }}>
                                        Password must has at least 8 characters that
                                        include atleast 1 lowercase character, 1 uppercase
                                        character,1 number and 1 special character
                                    </Text>
                                </View> :
                                <View></View>
                        }
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={[styles.inputStyle, font.regular]}
                                onChangeText={(text) => {
                                    setConfirmPassword(text)
                                    setConfirmPasswordValidationVisible(false)
                                }}
                                placeholder="Confirm Password" //dummy@abc.com
                                placeholderTextColor="#747474"
                                autoCapitalize="none"
                                
                                returnKeyType="next"
                                underlineColorAndroid="#f000"
                                secureTextEntry={true}
                                blurOnSubmit={false}
                            />
                        </View>
                        {
                            confirm_password_validation_msg ?
                                <View style={styles.validationText}>
                                    <Text style={{ color: 'red', ...FONTS.regularfont11 }}>
                                        Password Mismatch
                                    </Text>
                                </View> :
                                <View></View>
                        }
                        <TouchableOpacity
                            onPress={() => changePassword()}
                            style={styles.buttonStyle}
                            activeOpacity={0.5}
                        >
                            <Text style={[styles.buttonTextStyle, font.bold]}>Change Password</Text>
                        </TouchableOpacity>


                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
            {
                loader_visible == true ? <Loader /> : <View></View>
            }
            <Modal
                activeOpacity={1}
                transparent={true}
                visible={dialog_visible}
            >
                <TouchableOpacity
                    onPress={() => setDialogVisible(false)}
                    activeOpacity={1}
                    style={{
                        backgroundColor: "#000000aa",
                        flexDirection: 'column',
                        height: SIZES.height,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            width: SIZES.width / 1.5,
                            backgroundColor: 'white',
                            borderWidth: 1,
                            borderColor: COLORS.textblue.color,
                            minHeight: 150,
                            maxHeight: 150,
                            borderRadius: 10,
                            shadowColor: '#4e4f72',
                            shadowOpacity: 0.2,
                            shadowRadius: 30,
                            shadowOffset: {
                                height: 0,
                                width: 0,
                            },
                            elevation: 30,
                        }}>

                        <ScrollView
                            contentContainerStyle={{
                                alignSelf: 'center',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                height: "100%"

                            }}
                        >

                            <Text style={{ marginTop: 10, padding: 10, textAlign: 'justify', ...FONTS.regularsizeRegular }}>
                                {dialog_message}
                            </Text>



                        </ScrollView>
                        <TouchableOpacity
                            style={{
                                position: 'absolute',
                                top: 0,
                                right: 0
                            }}
                            onPress={() => setDialogVisible(false)}
                        >
                            <Image source={images.cancel_icon} style={{
                                width: 20,
                                height: 20,
                                tintColor: COLORS.textblue,
                                alignSelf: 'flex-end',
                                marginTop: 10,
                                marginRight: 10
                            }} />
                        </TouchableOpacity>
                    </TouchableOpacity>

                </TouchableOpacity>
            </Modal>
        </View>
    );
};
export default ChangePasswordScreen;

const styles = StyleSheet.create({
    HeadText: {
        color: '#fff',
        fontSize: 18,
        marginLeft: 35
    },
    bottomspace: {
        marginBottom: 0
    },
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#293272',
        alignContent: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 8,
        backgroundColor: "white",
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },
    validationText: {
        marginLeft: 35,
        marginRight: 35,
        flexDirection: 'column',

    },
    buttonStyle: {
        backgroundColor: '#56D6BE',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#56D6BE',
        height: 46,
        alignItems: 'center',
        borderRadius: 8,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 10,
        marginBottom: 15,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        fontWeight: "bold",
        paddingVertical: 13,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: '#747474',
        paddingLeft: 15,
        fontSize: 14,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: 'white',
    },
    forgot: {
        alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'flex-start',
        marginLeft: 35,
        padding: 10,
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
});
