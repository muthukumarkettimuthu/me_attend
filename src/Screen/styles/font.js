import { StyleSheet } from 'react-native'
export default StyleSheet.create({
    regular: {
        fontFamily: "Oxygen-Regular"
    },
    semibold: {
        fontFamily: "Oxygen-Light"
    },
    bold: {
        fontFamily: "Oxygen-Bold"
    },
    sizeLittlesmall:{
        fontSize:6
    },
    sizeVerySmall: {
        fontSize: 8
    },
    sizeSmall: {
        fontSize: 10,
    },
    font11: {
        fontSize: 11
    },
    sizeVeryRegular: {
        fontSize: 12
    },
    sizeLittleRegular:{
        fontSize: 13
    },
    sizeRegular: {
        fontSize: 14
    },
    sizeLittleLarger:{
        fontSize: 17
    },
    sizeLarge: {
        fontSize: 18
    },
    sizeExtraLarge: {
        fontSize: 20
    },
    sizeDoubleLarge: {
        fontSize: 25
    },
    opacityrange:{
        opacity:0.7

    },
    lightgray:{
        color:"#646464"
    },
    
    topspaceing:{
        marginTop:5
    },
    sizeMedium: {
        fontSize: 16
    },
    bgcolorbtn:{
        backgroundColor:"#56D6BE"
    },
    borderbluecolor:{
        borderColor:'#56D6BE'
    },
    textgreen:{
        color:"#56D6BE"
    },
    
    textWhiteColor: {
        color: "#fff",
        marginRight: 16
    },
    textSpacing: {
        letterSpacing: 1,
    },
    textblue:{
        color:"#293272"
    },textblue_disable:{
        color:"#abb6ff"
    },
    textwhite:{
        color:"#fff"
    },
    blackcolor:{
        color:"#333333",
    },
    graycolor:{
        color:"#747474"
    },
    backbg:{
        backgroundColor:"#EEFBF9"
    },
    borderradiustyle:{
        borderRadius:13.5,
        opacity:0.8
    },
    lightbg:{
        backgroundColor:"#EEFBF8"
    },
    linevertical:{
        lineHeight:19
    }
});