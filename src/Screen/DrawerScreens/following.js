// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import React, { Component, useState } from 'react';
import font from '../styles/font';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import { images } from '../../assets/styles/theme';
import ApiConstants from '../../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import NetInfo from "@react-native-community/netinfo";
import Loader from '../Components/Loader';






export default class following extends Component {

  constructor(props) {

    const containerStyle = { backgroundColor: 'white', padding: 20 };
    super(props);
    this.state = {
      modalVisible: false,
      search: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Photos' },
        { key: 'second', title: 'Attend' },

      ],
      loader_visible: false,
      follower_list: [],
      is_follow: false,
      id:props.route.params.user_id
    }
  }
  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };

  componentDidMount = () => {
    this.getFollowingList()
  }

  userFollow = async (follower_id) => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
 

        fetch(ApiConstants.url + "user-follow", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            follower_id: follower_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)

            this.setState({ loader_visible: false })
            let status = responseJson.status
            if (status == "success") {
              this.getFollowingList()

            } else {

            }

          }).catch(error => {
            alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  userUnFollow = async (follower_id) => {
    console.log(follower_id)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-unfollow", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            follower_id: follower_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)

            this.setState({ loader_visible: false })
            let status = responseJson.status
            if (status == "success") {
              this.getFollowingList()

            } else {

            }

          }).catch(error => {
            alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  userEventUnFollow = async (follower_id) => {
    console.log(follower_id)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "unfollow-event", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            event_id: follower_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            this.getFollowingList()
            this.setState({ loader_visible: false })
            let status = responseJson.status
            if (status == "Success") {
              

            } else {

            }

          }).catch(error => {
            alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  getFollowingList = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "following-list", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            user_id:this.state.id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson.following.data)
            this.setState({ follower_list: responseJson.following.data })
            this.setState({ loader_visible: false })


          }).catch(error => {
            alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity 
      onPress={() => {
       
        if(item.is_event==0){
          this.props.navigation.navigate('PeopleScreen', { user_id: item.id })
      }else{
          this.props.navigation.navigate('SettingScreen', { event_id: item.id })
      }
    }}
      style={[styles.rowgroup, styles.item,]}>
        <View>
        {item.is_event==0?<Image source={{ uri: ApiConstants.path+'uploads/users-gallery/' + item.profile_image }}
            style={styles.itemimg}
          />:
          <Image source={{ uri: ApiConstants.path+'/images/events/' + item.event_first_image.image }}
            style={styles.itemimg}
          />
          }

        </View>
        <View style={{ position: "relative", flex: 1 }}>
        <View
            style={{ flexDirection: "row", justifyContent: "space-between", marginTop: -7 }}>
            {item.is_event==0?
              <Text style={[font.sizeMedium, font.blackcolor, font.bold]}>{item.first_name + " " + item.last_name}</Text>
              :
              <Text style={[font.sizeMedium, font.blackcolor, font.bold]}>{item.event_name}</Text>
            }
          </View>
          <View
            style={{   marginBottom: 4 }}>
          {item.is_event==0?<Text style={[font.sizeRegular, font.graycolor, font.regular]} numberOfLines={1}>
            {item.age}, {item.gender}
          </Text>:
          <Text style={[font.sizeRegular, font.graycolor, font.regular]} numberOfLines={1}>
          {item.event_address}

        </Text>
          }
          </View>
         
            <TouchableOpacity
            onPress={()=>{
              if(item.is_event==0){
                this.userUnFollow(item.id)
              }else{
                this.userEventUnFollow(item.id)
              }
              
            }
          }
              style={{
                width: 110,
                height: 30,
                flexDirection: "row",
                borderWidth: 1,
                borderColor: "#293272",
                borderRadius: 5,
                justifyContent: "center",
                marginTop: 5, //5
                alignItems: "center"
              }}>
              <Image
                source={images.tick_icon} resizeMode="contain"
                style={{
                  height: 15,
                  width: 15,
                  marginRight: 5,
                  alignSelf: "center"
                }} />
              <Text style={[font.textblue, font.bold, font.sizeRegular]}>Following</Text>
            </TouchableOpacity> 
            
        </View>

      </TouchableOpacity>

    )
  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;


    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1}}>

          <View style={[styles.rowgroup, styles.header, styles.centercontent]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{ position: "absolute", left: 5 }}>
              <Image source={images.prevarrow_icon}
                style={{ resizeMode: "contain",height:15,width:9 }}
              />

            </TouchableOpacity>
            <View style={{ position: "relative", justifyContent: "center" }}>
              <Text style={[font.bold, font.textblue, font.sizeLarge]}>Following</Text>
            </View>



          </View>
          <View style={{ marginBottom: 70 }}>
            <FlatList
              data={this.state.follower_list}
              renderItem={this.renderItem}
              keyExtractor={item => item.id} />
          </View>

        </View>
        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }
      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  header: {
    
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    marginBottom:10,
   
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  followButtonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,
    width: 100,
    flex: .4
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    borderRadius: 10,
    height: 85,
    width: 85,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: -7,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    
    flex:1,
    marginVertical: 8,
    marginHorizontal: 20,
    borderRadius: 8,
    
    marginTop: 0,
    marginBottom:12,
   
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
