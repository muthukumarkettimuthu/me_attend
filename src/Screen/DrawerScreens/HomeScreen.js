
import React, { Component, useState } from 'react';
import font from '../styles/font';
import { ActivityIndicator } from 'react-native-paper';
import { TabView, TabBar } from 'react-native-tab-view';
import Swiper from 'react-native-swiper'
import NetInfo from "@react-native-community/netinfo";
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import DateTimePicker from '@react-native-community/datetimepicker';


import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  Modal,
  FlatList,
  SafeAreaView,
  PermissionsAndroid,
  StatusBar,
  Animated,
  Dimensions,
  TouchableOpacity,
  RefreshControl,

} from 'react-native';
import { ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';
import ApiConstants from '../../services/APIConstants';
import { Dropdown } from 'react-native-element-dropdown';
const initialLayout = { width: Dimensions.get("window").width }
import Loader from '../Components/Loader';
import Geolocation from 'react-native-geolocation-service';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';

const data = [
  { label: 'Today AM', value: 'AM' },
  { label: 'Today PM', value: 'PM' },

];






export default class HomeScreen extends Component {

  constructor(props) {


    super(props);
    this.state = {
      modalVisible: false,
      loader_visible: false,
      event_list: [],
      search: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Events' },
        { key: 'second', title: 'People' },
      ],
      people_list: [],
      people_list_page: 1,
      page: 1,
      event_last_page: 0,
      people_last_page: 0,
      people_loader_visible: true,
      event_loader_visible: true,
      search_keyword: "",
      search_flag: false,
      tab_index: "",
      advance_filter_visible: false,
      city_name: "",
      country_list: [],
      category_list: [],
      city_list: [],
      time_value: "AM",
      city_value: "",
      country_value: "",
      date: new Date(),
      current_date: "",
      mode: 'date',
      dateshow: false,
      date_value: "",
      advanced_country_id: "",
      advanced_city_id: "",
      advance_search: false,
      country_name: "",
      city_name: "",
      category_id_list: [],
      advance_search_flag_loader: false,
      current_time: "",
      refresh: false,
      people_city_id: "",
      people_country_id: ""
    }


  }



  componentDidMount = () => {

    AsyncStorage.setItem("time_value", this.state.time_value)

    this.props.navigation.addListener('focus', () => {
      this.setState({ index: 0 })
      var tdate = "" + this.state.date.toISOString().split('T')[0];
      var newdate = tdate.split("-").join("-");
      this.setState({ date_value: "" + newdate })

      let current_date = tdate.split("-").reverse().join("/");
      this.setState({ current_date: current_date })


      let current_time = this.state.date.getHours()

      if (current_time >= 12) {

        this.setState({ time_value: "PM" })
        AsyncStorage.setItem("time_value", this.state.time_value)
      } else {

        this.setState({ time_value: "AM" })
        AsyncStorage.setItem("time_value", this.state.time_value)
      }

      this.getCurrentCuntry()
      this.getCategory()
      this.getPeopleList()
    });
    this.props.navigation.addListener('blur', () => {
      this.setState({ event_list: [] })
      this.setState({ people_list: [] })
      this.setState({ city_list: [] })
      this.setState({ country_list: [] })
      this.setState({ category_list: [] })
      this.setState({ category_id_list: [] })
      this.setState({ page: 1 })
      this.setState({ people_list_page: 1 })
      this.setState({ index: 0 })
      this.setState({ advance_search: false })

      AsyncStorage.setItem("filter_country", "");
      AsyncStorage.setItem("filter_city", "")
      AsyncStorage.setItem("filter_date", "")
      this.initBackgroundFetch();
    });








  }

  initBackgroundFetch = async () => {
    if (ReactNativeForegroundService.is_task_running('taskid')) return;
    // Creating a task.
    ReactNativeForegroundService.add_task(
      () => {

        Geolocation.getCurrentPosition(
          (position) => {

            // console.log(position)
            let latitude = position.coords.latitude;
            let longitude = position.coords.longitude
            this.updateLiveLocation(latitude, longitude)
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      },
      {
        delay: 50000,
        onLoop: true,
        taskId: 'taskid',
        onError: (e) => console.log(`Error logging:`, e),
      },
    );

    return ReactNativeForegroundService.start({
      id: 144,
      title: 'Foreground Service',
      message: 'you are online!',
    });
  }

  updateLiveLocation = async (lat, lng) => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {

        fetch(ApiConstants.url + "update-live-location", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            lat: lat,
            lon: lng
          })
        }).then(response => {
          response.json()


        })
          .then(responseJson => {


          }).catch(error => {




          })
      }
    })
  }

  getLocation = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Permission",
          message:
            "App needs access to your location ",

          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("granted");

        Geolocation.getCurrentPosition(
          (position) => {

            this.setState({ latitude: position.coords.latitude })
            this.setState({ longitude: position.coords.longitude })
            this.getEventList()
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );


      } else {
        console.log("denied" + granted);
      }
    } catch (err) {
      console.warn(err);
    }
  }



  getKeyWord = async () => {

    const keyword = await AsyncStorage.getItem("keyword");
    const time = await AsyncStorage.getItem("time_value");
    const city_value = await AsyncStorage.getItem("city_value");

    const filter_country = await AsyncStorage.getItem("filter_country");
    const filter_city = await AsyncStorage.getItem("filter_city");
    const filter_date = await AsyncStorage.getItem("filter_date");





    if (keyword != null && keyword.length != 0) {
      this.setState({ search_keyword: keyword })

      this.getSearchResult()
    } else if (filter_country != null && filter_country.length != 0) {
      if (filter_city != null && filter_city.length != 0) {
        if (filter_date != null && filter_date.length != 0) {

          this.setState({ country_name: filter_country })
          this.setState({ city_name: filter_city })
          this.setState({ date_value: filter_date })
          this.getAdvancedSearchResult()


        }
      }
    }

    else {
      this.getEventList()

    }


  }

  setDate = (event, date) => {
    console.log(event)

    if (event.type == "set") {
      var tdate = "" + date.toISOString().split('T')[0];
      var newdate = tdate.split("-").reverse().join("/");
      this.setState({ date_value: tdate })
      this.setState({ advance_search: true })


      this.setState({ dateshow: false })
    } else if (event.type == "dismissed") {
      this.setState({ dateshow: false })

    }

  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('SettingScreen', { event_id: item.id })
          this.setState({ search_keyword: "" })
        }}
        style={[styles.item, styles.listshadowbox]}>

        {item.description != null ?



          <View style={{ flexDirection: 'row', width: "100%", paddingRight: 5, paddingTop: 5, paddingBottom: 5, marginBottom: 20 }}>
            <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.profile_image }} style={{ height: 60, width: 60, borderRadius: 10 }} />
            <View style={{ flex: 1, marginLeft: 10 }}>
              {item.is_promote == 1 ? <Text style={{ ...FONTS.regularsizeMedium }}><Text style={{ fontWeight: '700' }}>{item.first_name} {item.last_name}</Text> is Promoting this event</Text> : null}
              
              <Text style={{ ...FONTS.regularsizeRegular }}>{item.description}
              </Text>
            </View>

          </View>
          :
          <View></View>}

        <View style={styles.rowgroup}>
          {item.event_first_image != null ? <Image source={{ uri: ApiConstants.path + "images/events/" + item.event_first_image.image }}
            style={styles.itemimg}
          /> : <Image source={{ uri: ApiConstants.path + "images/events/" }}
            style={styles.itemimg}
          />}

          <View style={{ marginRight: 20, position: "relative", flex: 1 }}>
            <Text numberOfLines={1} style={{ color: "#333333", marginBottom: 4, ...FONTS.regularsizeMedium, fontWeight: '700' }}>
              {item.event_name}
            </Text>
            <Text style={{ color: "#747474", fontSize: 11, marginBottom: 8, fontFamily: "Oxygen-Regular" }} numberOfLines={1}>
              {item.from_time}-{item.to_time} | {item.date_time.split("-").reverse().join("/")}
            </Text>
            <Text numberOfLines={1} style={{ color: "#747474", fontWeight: '700', ...FONTS.regularsizeRegular }} > {item.company_name}</Text>

            <View style={{ paddingTop: 9, flexDirection: "row", paddingRight: 30 }} >
              <Image
                source={images.location_icon}
                resizeMethod="scale"
                resizeMode="stretch"
                style={{
                  height: 13,
                  width: 13,
                  marginRight: 2,
                  alignSelf: "center",
                  tintColor: COLORS.textgreen.color
                }}
              />

              <Text style={{ color: "#747474", fontSize: 12, }, font.regular} numberOfLines={1}> {item.event_address}</Text>

            </View>
            {item.event_expire != "inactive" ?
              <View>
                {item.attend_event == null ?
                  <TouchableOpacity
                    onPress={() => this.attendEvent(item.id)}
                    style={styles.buttonStyle}
                  >
                    <Text style={[styles.buttonTextStyle, font.bold]}>Attend</Text>
                  </TouchableOpacity> :
                  <View>
                    {
                      item.attend_status == "attending" ?
                        <TouchableOpacity
                          onPress={() => this.attendEvent(item.id)}
                          style={{
                            flexDirection: "row",
                            borderWidth: 1.5,
                            borderColor: "#293272",
                            borderRadius: 8,
                            padding: 6,
                            justifyContent: "center",
                            marginTop: 10,
                            width: 100
                          }}>
                          <Image
                            source={images.tick_icon} resizeMode="contain"
                            style={{
                              marginRight: 5,
                              alignSelf: "center",
                              height: 15,
                              width: 15
                            }} />
                          <Text style={[font.textblue, font.bold]}>Attending</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity
                          onPress={() => this.attendEvent(item.id)}
                          style={styles.attendedbuttonStyle}
                        >
                          <Text style={[styles.buttonTextStyle, font.bold]}>Attended</Text>
                        </TouchableOpacity>

                    }

                  </View>
                }
              </View> : <View>



                {item.attend_event == null ?
                  <TouchableOpacity

                    style={styles.disableButtonStyle}
                  >
                    <Text style={[styles.buttonTextStyle, font.bold]}>Attend</Text>
                  </TouchableOpacity> :
                  <View>
                    {
                      item.attend_status == "attending" ?
                        <TouchableOpacity
                          activeOpacity={1}
                          style={{
                            flexDirection: "row",
                            borderWidth: 1.5,
                            borderColor: "#abb6ff",
                            borderRadius: 8,
                            padding: 6,
                            justifyContent: "center",
                            marginTop: 10,
                            width: 100
                          }}>
                          <Image
                            source={images.tick_icon} resizeMode="contain"
                            style={{
                              marginRight: 5,
                              alignSelf: "center",
                              height: 15,
                              width: 15
                            }} />
                          <Text style={[font.textblue_disable, font.bold]}>Attending</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity
                          activeOpacity={1}
                          style={styles.attendedbuttonStyle}
                        >
                          <Text style={[styles.buttonTextStyle, font.bold]}>Attended</Text>
                        </TouchableOpacity>

                    }

                  </View>
                }




              </View>



            }



          </View>
          <View style={{
            position: "absolute",
            right: 0,
            top: 10
          }}>
            <Image source={images.menu_icon}
              style={
                {
                  resizeMode: "contain",
                  height: 20,
                  width: 20,
                  tintColor: COLORS.graycolor.color

                }
              }
            />
          </View>
        </View>


      </TouchableOpacity>
    )
  }

  renderItem2 = ({ item }) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('PeopleScreen', { user_id: item.id })
            this.setState({ search_keyword: "" })
          }
          }
          style={{ alignSelf: 'center', width: SIZES.width / 1.1, marginBottom: 10, marginTop: 10, borderRadius: 10 }}>

          {item.user_galleries.length != 0 ? <SwiperFlatList
            autoplay
            autoplayDelay={2}
            autoplayLoop
            showPagination
            paginationActiveColor={COLORS.textgreen.color}
            paginationStyle={{ position: 'relative', bottom: 60, height: 10, width: 10 }}
            paginationStyleItem={{ height: 10, width: 10 }}
            data={item.user_galleries}
            renderItem={({ item }) => (

              <View style={{ borderRadius: 10, alignSelf: 'center', flex: 1, justifyContent: 'center' }}>
                <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }} resizeMode="contain"
                  resizeMode="cover"
                  style={{
                    height: 353,
                    width: SIZES.width / 1.1,
                    borderRadius: 10,
                    alignSelf: 'center'

                  }}
                />

              </View>


            )}
            keyExtractor={(item2, index) => index}
          /> :
            <View style={{

              alignSelf: 'center',
              borderRadius: 10,


            }}>
              <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.profile_image }} resizeMode="contain"
                resizeMode="cover"
                style={{
                  height: 353,
                  width: SIZES.width / 1.1,
                  borderRadius: 20,
                  alignSelf: 'center',

                }}
              />
            </View>}


          <View style={{ height: 40, width: SIZES.width, flexDirection: 'row', flex: 1, marginTop: item.user_galleries.length != 0 ? -30 : 0 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('PeopleScreen', { user_id: item.id })}
              style={{ marginTop: 10 }}>
              <Text style={[font.bold]}>{item.first_name} {item.last_name} <Image style={{ height: 15, width: 15, tintColor: '#747474',marginTop: -3 }} source={images.information_icon} />
              </Text>
              <Text style={{ ...FONTS.semiboldsizeRegular }}>{item.age}, {item.gender}</Text>
            </TouchableOpacity>
            {item.current_user_followers.length == 0 ?
              <TouchableOpacity
                onPress={() => this.userFollow(item.id)}
                style={styles.followButtonStyle}
              >
                <Text style={[styles.buttonTextStyle, font.bold]}>Follow</Text>
              </TouchableOpacity> :

              <View style={{ marginRight: 35, flexDirection: "row", justifyContent: "space-between", flex: 1, justifyContent: 'flex-end' }}>

                {
                  item.current_user_followers.length != 0 ?
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                      <Image source={images.message_icon} resizeMode="contain"
                        style={{
                          height: 22,
                          width: 22,
                          marginRight: 15,
                          tintColor: COLORS.textblue.color
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => this.likePost(item.id)}
                      >
                        {item.liked_post.length == 0 ? <Image source={images.Path4_icon} resizeMode="contain"
                          style={{
                            height: 20,
                            width: 20,
                            marginRight: 15,
                            tintColor: COLORS.textblue.color
                          }}
                        /> :
                          <Image source={images.like_active_icon} resizeMode="contain"
                            style={{
                              height: 20,
                              width: 20,
                              marginRight: 15,

                            }}
                          />
                        }
                      </TouchableOpacity>

                      <Image source={images.Path55870_icon} resizeMode="contain"
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: COLORS.textblue.color
                        }}
                      />
                    </View> : <View></View>}
              </View>

            }

          </View>


          {item.current_user_followers.length != 0 ?
            <View>
              <Text style={[font.regular], { color: "#747474", fontSize: 12 }}>


              </Text>
              <Text style={[font.regular], { color: "#747474", fontSize: 10, marginTop: 5 }}>

              </Text>
            </View> : <View></View>}

        </TouchableOpacity>
        {item.event_attend_list.map((item, index) => (
          <View style={styles.bgdiv}>
            <Text><Text style={{
              ...FONTS.regularsizeMedium,
              fontWeight: "700",
              color: COLORS.textgreen.color
            }}>
              Attend </Text> <Text style={{ ...FONTS.regularsizeMedium }}>an event at </Text>
              <Text style={{
                ...FONTS.regularsizeMedium,
                fontWeight: "700",
                color: "#646464"
              }}>{item.event_name} </Text>
              <Text style={{ ...FONTS.regularsizeMedium }}>with her</Text>
            </Text>
          </View>
        ))

        }
      </View>

    )
  }

  likePost = async (id) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })
        fetch(ApiConstants.url + "like-post", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            user_id: id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            this.setState({ people_list_page: 1 })
            this.setState({ people_list: [] })
            this.getPeopleList()
          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      }
    })
  }

  userFollow = async (id) => {
    console.log(id)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-follow", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            follower_id: id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            const status = responseJson.status
            if (status == "success") {

              this.setState({ people_list_page: 1 })
              this.setState({ people_list: [] })
              this.getPeopleList()
              Toast.show({
                title: 'Success',
                text: responseJson,

                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            } else {
              Toast.show({
                title: 'Error',
                text: responseJson,

                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            }

            this.setState({ loader_visible: false })


          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }
  attendEvent = async (id) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "attend-event", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            event_id: id
          })
        }).then(response => response.json())
          .then(responseJson => {



            if (this.state.advance_search) {
              this.setState({ loader_visible: false })
              this.setState({ event_list: [] })
              this.setState({ page: 1 })
              this.getAdvancedSearchResult()
            } else {
              this.setState({ loader_visible: false })
              this.setState({ event_list: [] })
              this.setState({ page: 1 })
              this.getEventList()
            }


          }).catch(error => {
            // alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }



  getPeopleList = async () => {
    // alert(this.state.people_list_page)

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {


        fetch(ApiConstants.url + "people-list?page=" + this.state.people_list_page, {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {

            console.debug("people", responseJson.peoples.data[1])
            if (responseJson.peoples.data.length == 0) {
              this.setState({ people_loader_visible: false })
              Toast.show({
                title: 'No data!',
                text: "",
                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            }

            this.setState(state => ({ people_list: [...this.state.people_list, ...responseJson.peoples.data] }))
            this.setState({ loader_visible: false })
            this.setState({ people_last_page: responseJson.peoples.last_page })


          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }

  getEventList = async () => {
    // alert(this.state.city_value+","+this.state.time_value)

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {

        fetch(ApiConstants.url + "event-list?page=" + this.state.page, {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            city: this.state.city_value,
            time: this.state.time_value,
          })
        }).then(response => response.json())
          .then(responseJson => {

            this.setState({ advance_search_flag_loader: false })
            this.setState({ event_last_page: responseJson.event_list.last_page })
            console.debug("evt", responseJson.event_list.data)
            this.setState({ loader_visible: false })
            this.setState(state => ({ event_list: [...this.state.event_list, ...responseJson.event_list.data] }))

            if (responseJson.event_list.data.length == 0) {
              this.setState({ event_loader_visible: false })
              Toast.show({
                title: 'No data!',
                text: "",
                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            } else {
              this.setState({ event_loader_visible: true })
            }
            // this.setState({city_value:city_id})

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }

  getSearchResult = async () => {
    console.log("work")
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        console.log("workin")
        this.setState({ event_list: [] })
        this.setState({ page: 1 })
        fetch(ApiConstants.url + "event-list", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            search: this.state.search_keyword
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            this.setState({ search_flag: true })
            this.setState({ loader_visible: false })
            this.setState({ event_list: responseJson.event_list.data })
            if (responseJson.event_list.data.length == 0) {
              this.setState({ event_loader_visible: false })
              Toast.show({
                title: 'No data!',
                text: "",
                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            }

          }).catch(error => {
            console(error)
            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }

  getAdvancedSearchResult = async () => {
    this.setState({ advance_filter_visible: false })
    this.setState({ advance_search: true })
    let country_name = this.state.country_name;
    let city_name = this.state.city_name;
    let date = this.state.date_value
    console.log(country_name + ", " + city_name + ", " + date)

    var token = await AsyncStorage.getItem("user_id")
    var i = 0
    for (i = 0; i < this.state.category_list.length; i++) {
      if (this.state.category_list[i].isSelected == true) {
        this.state.category_id_list.push(this.state.category_list[i].id)
      }

    }
    console.log(this.state.category_id_list)
    if (date.length != 0) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          if (!this.state.advance_search_flag_loader) {
            this.setState({ event_list: [] })
            this.setState({ page: 1 })
          }


          fetch(ApiConstants.url + "event-list?page=" + this.state.page, {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              country: this.state.country_name,
              city: this.state.city_name,
              date: this.state.date_value,
              category_id: this.state.category_id_list,
              advance_search: this.state.advance_search,
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.log(responseJson.event_list.data)

              this.setState({ search_flag: false })
              this.setState({ loader_visible: false })

              this.setState({ event_last_page: responseJson.event_list.last_page })
              this.setState(state => ({ event_list: [...this.state.event_list, ...responseJson.event_list.data] }))
              if (responseJson.event_list.data.length == 0) {
                this.setState({ event_loader_visible: false })
                Toast.show({
                  title: 'No data!',
                  text: "",
                  color: '#000',
                  timeColor: 'red',
                  backgroundColor: COLORS.textblue.color,
                  timing: 5000,
                  position: 'bottom',
                })
              }
            }).catch(error => {

              this.setState({ loader_visible: false })


            })
        } else {

          Toast.show({
            title: 'Network Error!',
            text: "",

            color: '#000',
            timeColor: 'red',
            backgroundColor: COLORS.textblue.color,
            timing: 5000,
            position: 'bottom',
          })
        }
      })
    } else {
      Toast.show({
        title: 'Date is missing !',
        text: "",

        color: '#000',
        timeColor: 'red',
        backgroundColor: COLORS.textblue.color,
        timing: 5000,
        position: 'bottom',
      })
    }

  }
  getAdvancedPeopleSearchResult = async () => {
    this.setState({ advance_filter_visible: false })
    this.setState({ advance_search: true })
    let country_name = this.state.advanced_country_id;
    let city_name = this.state.advanced_city_id;
    var token = await AsyncStorage.getItem("user_id")
    console.log(country_name + ", " + city_name + ", ")




    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        if (!this.state.advance_search_flag_loader) {
          this.setState({ people_list: [] })
          this.setState({ people_list_page: 1 })
        }


        fetch(ApiConstants.url + "people-list?page=" + this.state.people_list_page, {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            country: country_name,
            city: city_name,
            advance_search: this.state.advance_search,
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)

            this.setState({ search_flag: false })
            this.setState({ loader_visible: false })


            this.setState(state => ({ people_list: [...this.state.people_list, ...responseJson.peoples.data] }))

            this.setState({ people_last_page: responseJson.peoples.last_page })



            if (responseJson.event_list.data.length == 0) {
              this.setState({ event_loader_visible: false })
              Toast.show({
                title: 'No data!',
                text: "",
                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            }
          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })


  }

  getSearchPeopleResult = async () => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ people_list: [] })
        this.setState({ people_list_page: 1 })
        fetch(ApiConstants.url + "people-list", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            search: this.state.search_keyword
          })
        }).then(response => response.json())
          .then(responseJson => {

            this.setState({ search_flag: true })
            this.setState({ loader_visible: false })
            this.setState({ people_list: responseJson.peoples.data })
            if (responseJson.peoples.data.length == 0) {
              this.setState({ people_loader_visible: false })
              Toast.show({
                title: 'No data!',
                text: "",
                color: '#000',
                timeColor: 'red',
                backgroundColor: COLORS.textblue.color,
                timing: 5000,
                position: 'bottom',
              })
            }

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }

  getCountry = () => {
    // alert("work")
    fetch(ApiConstants.url + "country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {

          this.setState({ country_list: responseJson.value })

        }
      })


  }



  getCategory = async () => {
    // alert("work")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "category-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {
          let list = responseJson.categories

          this.setState({ category_list: responseJson.categories })
          let arr = this.state.category_list.map((item, index) => {
            item.isSelected = false
            return { ...item }
          })
          console.log(arr)
          this.setState({ category_list: arr })
        }
      })


  }

  getCity = (country_id) => {

    fetch(ApiConstants.url + "city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        country_id: country_id

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("city", responseJson.value[0].city_id)
        const status = responseJson.status
        if (status == "success") {



          this.setState({ city_list: responseJson.value })

        } else {

        }
      })

  }

  getCurrentCuntry = async () => {
    // alert("work"+"city")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "user-country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {
          console.log(responseJson)
          const country_id = responseJson.value[0].id
          const country_name = responseJson.value[0].country
          if (country_id.length != 0) {
            this.getCurrentCity(country_id)
            this.setState({ advanced_country_id: country_id })
            this.setState({ country_name: country_name })
            this.getCountry()
          }



        } else {

        }
      })

  }

  getCurrentCity = async (country_id) => {
    // alert("work"+"city")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "user-city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {
        console.log("currentcity", responseJson)
        const status = responseJson.status
        if (status == "success") {

          const city_id = responseJson.current_city[0].city_id
          const city_name = responseJson.current_city[0].city
          this.setState({ city_value: city_id })
          this.setState({ city_name: city_name })
          AsyncStorage.setItem("city_name", city_name)
          this.setState({ advanced_city_id: city_id })
          if (city_id.length != 0) {
            this.getCity(country_id)
            this.getKeyWord();

          }

        } else {

        }
      })

  }

  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };
  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };
  getTabWidthActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);
    return {
      textAlign: "center",
      color: "#293272",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "regular",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }
  getTabWidthInActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);

    return {
      textAlign: "center",
      color: "#646464",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "bold",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }

  renderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        {this.state.event_loader_visible ? <ActivityIndicator size="small" color="#aaa" /> : <View></View>}
      </View>
    )
  }

  peopleRenderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        {this.state.people_loader_visible ? <ActivityIndicator size="small" color="#aaa" /> : <View></View>}
      </View>
    )
  }

  loadMoreItem = () => {
    // alert(this.state.event_last_page)
    var current_page = this.state.page;
    var last_page = this.state.event_last_page
    var advance_search_flag = this.state.advance_search
    if (current_page != last_page && !this.state.search_flag) {
      this.setState({ event_loader_visible: true })
      if (!advance_search_flag) {
        this.setState(state => ({ page: this.state.page + 1 }), () => this.getEventList())
      } else {
        this.setState({ advance_search_flag_loader: true })
        this.setState(state => ({ page: this.state.page + 1 }), () => this.getAdvancedSearchResult())
      }

    } else {
      this.setState({ event_loader_visible: false })
    }


  }

  peopleLoadMoreItem = () => {

    var current_people_page = this.state.people_list_page;
    var people_last_page = this.state.people_last_page
    var advance_search_flag = this.state.advance_search
    if (current_people_page != people_last_page && !this.state.search_flag) {
      this.setState({ people_loader_visible: true })
      if (!advance_search_flag) {
        this.setState(state => ({ people_list_page: this.state.people_list_page + 1 }), () => this.getPeopleList())
      } else {
        this.setState({ advance_search_flag_loader: true })
        this.setState(state => ({ page: this.state.page + 1 }), () => this.getAdvancedPeopleSearchResult())
      }


    }
    else {
      this.setState({ people_loader_visible: false })
    }
  }

  refreshEvent = () => {

    if (this.state.advance_search) {
      this.setState({ loader_visible: false })
      this.setState({ event_list: [] })
      this.setState({ page: 1 })
      this.getAdvancedSearchResult()
    } else {
      this.setState({ loader_visible: false })
      this.setState({ event_list: [] })
      this.setState({ page: 1 })
      this.getEventList()
    }

    // this.setState({ event_list: [] })
    // this.setState({ page: 1 })
    // this.getEventList()
    // this.setState({ advance_search: false })
    AsyncStorage.setItem("keyword", "")
    this.setState({ search_keyword: "" })

    AsyncStorage.setItem("filter_country", "");
    AsyncStorage.setItem("filter_city", "")
    AsyncStorage.setItem("filter_date", "")
  }

  refreshPeople = () => {
    this.setState({ people_list: [] })
    this.setState({ people_list_page: 1 })
    this.getPeopleList()
  }



  refreshEventPage = () => {
    this.setState(
      {
        refresh: true,
      },
      () => this.refreshEvent()

    );
    this.setState({
      refresh: false,
    });
  };

  refreshPeoplePage = () => {
    this.setState(
      {
        refresh: true,
      },
      () => this.refreshPeople()

    );
    this.setState({
      refresh: false,
    });
  };


  renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <FlatList
            data={this.state.event_list}
            renderItem={this.renderItem}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refresh}
                onRefresh={this.refreshEventPage}
              />
            }
            keyExtractor={item => item.reference_key}
            ListFooterComponent={this.renderLoader}
            onEndReached={this.loadMoreItem}
            onEndReachedThreshold={0}
          />
        </View>
      case "second":
        return <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <FlatList
            data={this.state.people_list}
            renderItem={this.renderItem2}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refresh}
                onRefresh={this.refreshPeoplePage}
              />
            }
            ListFooterComponent={this.peopleRenderLoader}
            onEndReached={this.peopleLoadMoreItem}
            keyExtractor={item => item.id}
            onEndReachedThreshold={0}
          />
        </View>
    }
  }

  updateSearch = (search) => {
    this.setState({ search });
  };
  _renderTimeItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>

      </View>
    );
  };

  getList = (item, index) => {


    const newArrData = this.state.category_list.map((e, index) => {

      if (item.id == e.id) {
        item.isSelected = !item.isSelected

      }
      return {
        ...e

      }
    })

    this.setState({ category_list: newArrData })
    console.log(this.state.category_list)
    // this.setState({ item_id: item.id })
  }

  renderCategoryItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.getList(item, index)}
        style={{ flexDirection: 'row', marginRight: 40, marginBottom: 10, marginTop: 10 }}>
        {!item.isSelected ? <Image style={{
          height: 20,
          width: 20,

        }} source={images.unselected_icon} />
          : <Image style={{
            height: 20,
            width: 20,

          }} source={images.selected_icon} />
        }
        <Text style={{
          alignSelf: 'center',
          marginLeft: 5,
          ...FONTS.regularsizeRegular,
          color: "#646464",
          fontWeight: '700'
        }}>{item.category}</Text>
      </TouchableOpacity>

    )

  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;



    return (
      <View style={{ flex: 1 }}>

        <StatusBar
          animated={true}
          backgroundColor={"#293272"}
        />

        <View style={{ flex: 1, backgroundColor: "#fff" }}>
          <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width, bottom: 40 }}>

          </Root>
          <View style={styles.headerbg}>
            <View style={styles.selfalign}>
              <Image
                source={images.large_logo_icon}
                style={{
                  resizeMode: "contain",
                  height: 40,
                  width: 70,
                  bottom: 5
                }}
              />
            </View>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                this.showModal();
              }}
              style={[styles.locationrowgroup, styles.stretchitem]}>
              <Image
                source={images.location_icon}
                style={{
                  height: 18,
                  width: 18,
                  marginRight: 5,
                  tintColor: COLORS.textgreen.color,
                  bottom: 5
                }}
              />
              <Text style={[font.bold, styles.textstyle]}>

                {this.state.city_name}<Text style={[font.bold, styles.textstyle], { color: "#56D6BE", }}> . </Text>
              </Text><Text style={[font.bold, styles.textstyle]}>Today - {this.state.time_value}</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.input_field, styles.topspace]}>
            <View style={[styles.shadowbox, styles.searchSection]}>
              <TextInput
                returnKeyType="search"
                numberOfLines={1}
                style={[
                  font.regular,
                  font.bold,
                  styles.inputStyle,

                ]}
                placeholder={
                  "Search For Events and People..."
                }
                onSubmitEditing={() => {
                  if (this.state.index == 0) {
                    if (this.state.search_keyword.length != 0) {
                      this.getSearchResult()
                    }

                  }
                  else {
                    if (this.state.search_keyword.length != 0) {
                      this.getSearchPeopleResult()
                    }
                  }

                  // alert("dsd"+this.state.index)
                }}
                value={this.state.search_keyword}

                onChangeText={(searchString) => {
                  this.setState({ search_keyword: searchString })
                  if (searchString.length == 0) {
                    this.setState({ search_flag: false })

                    if (this.state.advance_search) {
                      this.setState({ loader_visible: false })
                      this.setState({ event_list: [] })
                      this.setState({ page: 1 })
                      this.getAdvancedSearchResult()
                    } else {
                      this.setState({ loader_visible: false })
                      this.setState({ event_list: [] })
                      this.setState({ page: 1 })
                      this.getEventList()
                    }

                    this.setState({ people_list: [] })
                    this.getPeopleList()
                    AsyncStorage.setItem("keyword", "")
                  }
                }}
                underlineColorAndroid="transparent"
              />
              <TouchableOpacity
                style={styles.searchIcon}
                onPress={() => {
                  if (this.state.index == 0) {
                    if (this.state.search_keyword.length != 0) {
                      this.getSearchResult()
                    }
                  }
                  else {
                    if (this.state.search_keyword.length != 0) {
                      this.getSearchPeopleResult()
                    }
                  }
                }}
              >
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    position: "absolute",
                    alignSelf: 'center',
                    tintColor: 'black'
                  }}
                  source={images.search_icon}
                />
              </TouchableOpacity>

            </View>
            <View style={[styles.shadowbox, styles.squrebox]}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ advance_filter_visible: true })
                }}
              >
                <Image
                  source={images.filter_icon}
                  style={{
                    height: 25,
                    width: 25,
                    tintColor: COLORS.white.color
                  }}
                />
              </TouchableOpacity>

            </View>
          </View>
          <TabView
            navigationState={{ index, routes }}
            renderScene={this.renderScene}
            onIndexChange={index => {
              this.setState({ index })

            }}

            initialLayout={{ initialLayout }}
            indicatorStyle={{ backgroundColor: '#56D6BE' }}
            activeColor={{ color: "#ff" }}
            indicatorContainerStyle={{ backgroundColor: "#fff" }}
            style={[font.regular, font.sizeSmall]}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                renderLabel={({ route, focused, color }) => (
                  <Animated.Text
                    numberOfLines={1}
                    style={
                      focused
                        ? this.getTabWidthActive()
                        : this.getTabWidthInActive()
                    }
                  >
                    {route.title}
                  </Animated.Text>
                )}
                getLabelText={({ route: { title } }) => title}
                indicatorStyle={styles.indicator}
                tabStyle={[styles.tabStyle, font.semibold]}
                style={[styles.tab, font.semibold]}
              />
            )}

          />

        </View>
        <Modal
          transparent={true}
          visible={this.state.modalVisible}

        // onDismiss={() => this.gotoLogin()}
        >
          <TouchableOpacity

            onPress={() => this.setState({ modalVisible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',

            }}

          >

            <View style={{ backgroundColor: "#fff", minHeight: 300, maxHeight: 350, margin: 15, borderRadius: 10, padding: 25 }}>

              <View style={{ flexDirection: 'row', flex: 1 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false })
                  }}
                  style={{ marginTop: 4, flex: .2, justifyContent: 'center' }}
                >

                  <Image
                    source={images.cancel_icon}
                    style={{
                      tintColor: COLORS.textblue.color,
                      height: 15,
                      width: 15
                    }}
                  />
                </TouchableOpacity>

                <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
                <View style={{ flex: .2 }}></View>
              </View>

              <Text style={[styles.smalltitle, font.bold]}>Location</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1 }}>
                  <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                    <Dropdown
                      style={{ width: "90%", marginLeft: 10 }}

                      data={this.state.city_list}
                      searchPlaceholder="Search"
                      labelField="city"
                      value={Number(this.state.city_value)}
                      valueField="city_id"
                      placeholder="Select item"
                      onChange={item => {
                        this.setState({ city_name: item.city })
                        this.setState({ city_value: item.city_id })
                        console.log('selected', item);
                        const city = item.city_id;
                        this.setState({ event_list: [] })
                        if (city.length != 0) {
                          this.getEventList()
                        }
                      }}


                    />
                  </View>
                </View>

              </View>
              <Text style={[styles.smalltitle, font.bold]}>Time</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1 }}>
                  <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                    <Dropdown
                      style={{ width: "90%", marginLeft: 10 }}
                      value={this.state.time_value}
                      data={data}
                      maxHeight={100}
                      searchPlaceholder="Search"
                      labelField="label"
                      valueField="value"
                      placeholder="Select item"
                      onChange={item => {
                        AsyncStorage.setItem("time_value", item.value)
                        this.setState({ time_value: item.value })
                        this.setState({ event_list: [] })
                        const time = item.value
                        if (time.length != 0) {
                          this.getEventList()
                        }

                        console.log('selected', item.value);
                      }}
                    // renderItem={item => this._renderTimeItem(item)}

                    />
                  </View>
                </View>

              </View>
            </View>
          </TouchableOpacity>

        </Modal>

        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }

        <Modal
          visible={this.state.advance_filter_visible}
        >
          <TouchableOpacity
            activeOpacity={1}
            style={{
              height: SIZES.height,
            }}>
            <View
              style={{
                marginHorizontal: 35,
                height: 50,

              }}
            >
              <View style={{
                flexDirection: 'row',
                flex: 1,
                height: 10,


              }}>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    marginTop: 30,



                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ advance_filter_visible: false })
                    }}
                    style={{
                      justifyContent: 'center',


                      height: 30,
                      width: 60,

                    }}
                  >
                    <Image
                      source={images.cancel_icon}
                      style={{
                        height: 15,
                        width: 15,
                        tintColor: COLORS.textblue.color,
                        marginTop:10

                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: .6,
                    justifyContent: 'center',
                    height: 30,
                    alignItems: 'center',
                    marginTop: 30
                  }}
                >
                  <Text
                    style={{
                      ...FONTS.boldsizeLarge,
                      color: COLORS.textblue.color
                    }}
                  >Advanced Filters</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    marginTop: 30,
                    alignSelf: 'center',
                    justifyContent: 'flex-end',

                  }}
                >

                </View>
              </View>
            </View>


            <View style=
              {{
                marginTop: 30,
                flex: 1,

              }}
            >
              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeRegular,
                  color: COLORS.textblue.color
                }}
              >
                City
              </Text>
              <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                <Dropdown
                  style={{ width: "90%", marginLeft: 10 }}
                  data={this.state.city_list}
                  value={Number(this.state.advanced_city_id)}
                  searchPlaceholder="Search"
                  labelField="city"
                  valueField="city_id"
                  placeholder="Select item"

                  onChange={item => {
                    this.setState({ advanced_city_id: item.city_id })
                    this.setState({ people_city_id: item.city_id })
                    this.setState({ city_name: item.city })
                    console.log('selected', this.state.city_name);
                  }}


                />
              </View>
              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeRegular,
                  color: COLORS.textblue.color
                }}
              >
                Country
              </Text>
              <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                <Dropdown

                  style={{ width: "90%", marginLeft: 10 }}
                  value={Number(this.state.advanced_country_id)}
                  data={this.state.country_list}
                  searchPlaceholder="Search"
                  labelField="country"
                  valueField="country_id"
                  placeholder="Select item"

                  onChange={item => {
                    this.setState({ advanced_country_id: item.country_id })
                    this.setState({ people_country_id: item.country_id })
                    this.setState({ country_name: item.country })
                    this.getCity(item.country_id)
                    console.log('selected', item);
                  }}


                />

              </View>
              {this.state.index == 0 ? <View>
                <Text
                  style={{
                    marginLeft: 35,
                    ...FONTS.regularsizeRegular,
                    color: COLORS.textblue.color
                  }}
                >
                  Date
                </Text>
                <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                  <TextInput
                    style={[styles.inputStyle, font.regular]}
                    value={this.state.date_value.split("-").reverse().join("/")}
                    placeholder="Select Date"
                    placeholderTextColor="#747474"
                    keyboardType="name-phone-pad"
                    blurOnSubmit={false}
                    underlineColorAndroid="#f000"

                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ dateshow: true })
                      // alert(this.state.dateshow)
                    }}
                    style={{
                      position: 'absolute',
                      right: 0,
                      width: 40,
                      height: "100%",
                      alignSelf: 'flex-end',
                      justifyContent: 'center'
                    }}
                  >
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        alignSelf: 'center',
                        tintColor: COLORS.textblue.color
                      }}

                      source={images.date_pcker_icon}
                    />
                  </TouchableOpacity>

                </View>
              </View> : <View></View>}






              {this.state.dateshow == true ?
                (<DateTimePicker value={this.state.date}
                  mode={this.state.mode}
                  is24Hour={true}
                  onTouchCancel={(value) => { alert(value) }}
                  display='default'
                  onChange={this.setDate}
                ></DateTimePicker>) : (<View></View>)
              }


              {this.state.index == 0 ? <View>
                <Text
                  style={{
                    marginLeft: 35,
                    ...FONTS.regularsizeMedium,
                    color: "#333333",
                    fontWeight: '700',
                    marginBottom: 30,
                    marginTop: 30
                  }}
                >
                  Categories
                </Text>

                <View style={{ width: SIZES.width, marginLeft: 35, }}>


                  <FlatList

                    numColumns={3}
                    data={this.state.category_list}
                    renderItem={this.renderCategoryItem}
                    keyExtractor={item => item.id}
                  />
                </View>
              </View> : <View></View>}





              <TouchableOpacity
                onPress={() => {

                  if (this.state.index == 0) {
                    this.getAdvancedSearchResult()
                  } else {
                    this.getAdvancedPeopleSearchResult()
                  }




                }}
                style={{
                  width: "85%",
                  height: 40,

                  backgroundColor: COLORS.textgreen.color,
                  marginHorizontal: 20,
                  alignSelf: 'center',
                  borderRadius: 10,
                  justifyContent: 'center',
                  bottom: 35,
                  position: 'absolute'
                }}
              >
                <Text style={{
                  color: "white",
                  ...FONTS.boldsizeMedium,
                  alignSelf: 'center'
                }}>
                  Search
                </Text>
              </TouchableOpacity>
            </View>



          </TouchableOpacity>

        </Modal>
      </View >
    );
  }
};


const styles = StyleSheet.create({
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272",
    fontSize: 18,
    flex: .6,
    alignSelf: 'center',
    textAlign: 'center'
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginLeft: 12,
    marginRight: 3,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -25,
  },
  bgdiv: {
    backgroundColor: "#EEFBF9",

    marginTop: 6,
    marginHorizontal: 50,
    borderRadius: 8,
    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 4,
    marginBottom: 20,
    flexDirection: 'row',
    paddingLeft: 15
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: 'center',
    marginBottom: 60
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',

    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginLeft: 14,

    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: .1,
    shadowRadius: 8,
    elevation: 24,
    shadowOpacity: .08,
    shadowColor: "#444444"
  },
  listshadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: .1,
    shadowRadius: 8,
    elevation: 24,
    shadowRadius: 5,
    shadowOpacity: .09,
    shadowColor: "#444444"
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",


  },
  locationrowgroup: {
    flexDirection: "row",
    marginLeft: 21

  },
  input_field: {
    flexDirection: "row",
    marginRight: 10

  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center",
    bottom: 5
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 135,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",

  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: 'cover',
    height: 140,
    width: 140,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start",
    borderRadius: 10,

  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 8,
  },
  AdvanceFilterSectionStyle: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 8,
  },
  CitySectionStyle: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,


  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,   //10
    marginBottom: 0, //0
    width: 100,

  },
  disableButtonStyle: {
    backgroundColor: '#abb6ff',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,   //10
    marginBottom: 0, //0
    width: 100,

  },
  attendedbuttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,
    width: 100,

  },
  followButtonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 0,
    width: 100,
    flex: .4,
    position: 'absolute',
    right: 10
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 4, //7
    alignSelf: "center",
  },
  desableButtonTextStyle: {
    color: '#b5b5b5',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',

    fontSize: 14,
    paddingLeft: 14,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,

    flex: 1
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 40,
    height: "100%",
    position: "absolute",
    right: 4,
    justifyContent: 'center',
    alignItems: 'center',

  },

});
