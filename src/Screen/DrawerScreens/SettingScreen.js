// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import React, { Component, useState } from 'react';
import Icon from "react-native-vector-icons/FontAwesome";
import font from '../styles/font';
import { Dropdown } from 'react-native-element-dropdown';
import QRCode from 'react-native-qrcode-svg';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import HorizontalSlider from 'react-horizontal-slider'
import Swiper from 'react-native-swiper'
import Carousel from 'react-native-snap-carousel';
import VideoPlayer from 'react-native-video-player';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import RazorpayCheckout from 'react-native-razorpay'
import PayPal from 'react-native-paypal-wrapper'
import { Root, Toast } from 'react-native-popup-confirm-toast'
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  useWindowDimensions,
  FlatList,
  SafeAreaView,
  KeyboardAvoidingView,
  Keyboard,
  Animated,
  Dimensions,
  Modal,
  Linking,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { ImageBackground } from 'react-native';
import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';
import ApiConstants from '../../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import Loader from '../Components/Loader';
import DateTimePicker from '@react-native-community/datetimepicker';



var pos = 0


const horizontalMargin = 20;
const slideWidth = 280;



const data = [
  { label: 'Today AM', value: 'AM' },
  { label: 'Today PM', value: 'PM' },

];

export default class SettingScreen extends Component {

  constructor(props) {

    const containerStyle = { backgroundColor: 'white', padding: 20 };
    super(props);
    this.state = {
      modalVisible: false,
      event_id: props.route.params.event_id,
      notification_id: props.route.params.notification_id,
      city_name: "",
      time_value: "",
      search: '',
      time: "",
      date: new Date(),
      current_date: "",
      event_date: "",
      date_value: "",
      event_name: "",
      event_address: "",
      company_name: "",
      event_image_list: [],
      event_menu_image_list: [],
      event_category: [],
      event_video_list: [],
      category_list: [],
      promote_button_visible: false,
      loader_visible: false,
      index: 0,
      mode: 'date',
      follow_label: "",
      attending: false,
      attend_button_label: "",
      latitude: 0.0,
      longitude: 0.0,
      evenImage: "",
      discount: "no discount code",
      discount_name: "",
      image_list_preview: false,
      menu_list_preview: false,
      video_list_preview: false,
      dateshow: false,
      modal_active: false,
      live_stream_amount: "",
      expire_status: "",
      position: 0,
      qr_code_visible: false,
      name: "",
      useEmail: "",
      user_token: "",
      search_keyword: "",
      time_city_visible: false,
      advanced_country_id: "",
      country_list: [],
      city_list: [],
      live_stream_option: "",
      country_name: "",
      advance_filter_visible: false,
      routes: [
        { key: 'first', title: 'Events' },
        { key: 'second', title: 'People' },
      ],
      activeIndex: 0,
      is_joined_live_stream: "",
      is_live_stream_started: ""
    }



    this.flatListRef = null;
    this.menuFlatListRef = null;
    this.videoFlatListRef = null;
  }

  componentDidMount = async () => {

    this.props.navigation.addListener('focus', e => {
      this.getCurrentCuntry()
      this.getCategory()
      this.getProfile()
      this.getEventDetails()
    })

    this.props.navigation.addListener('blur', e => {

    })
    // console.log(socket)
    // alert(socket)
    var date = new Date()
    var tdate = "" + date.toISOString().split('T')[0];
    var newdate = tdate.split("-").join("-");
    this.setState({ date_value: "" + newdate })

    let current_date = tdate.split("-").reverse().join("/");
    this.setState({ current_date: current_date })

    let current_time = this.state.date.getHours()

    // if (current_time > 12) {

    //   this.setState({ time_value: "PM" })
    //   AsyncStorage.setItem("time_value", this.state.time_value)
    // } else {

    //   this.setState({ time_value: "AM" })
    //   AsyncStorage.setItem("time_value", this.state.time_value)
    // }

    var city_name = await AsyncStorage.getItem("city_name")
    var time_value = await AsyncStorage.getItem("time_value")
    this.setState({ city_name: city_name })
    this.setState({ time_value: time_value })
    console.log(this.state.event_id)
    // if (promoter_label != null) {
    //   if (promoter_label == "Be Promote") {
    //     this.setState({ promote_button_visible: false })
    //   } else {
    //     this.setState({ promote_button_visible: true })
    //   }

    //   console.log(promoter_label)
    // } else {
    //   this.setState({ promote_button_visible: false })

    //   console.log(promoter_label)
    // }



  }

  getCurrentCuntry = async () => {
    // alert("work"+"city")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "user-country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {
          console.log(responseJson)
          const country_id = responseJson.value[0].id
          const country_name = responseJson.value[0].country
          if (country_id.length != 0) {
            this.getCurrentCity(country_id)
            this.setState({ advanced_country_id: country_id })
            this.setState({ country_name: country_name })
            this.getCountry()
          }



        } else {

        }
      })

  }


  getCurrentCity = async (country_id) => {
    // alert("work"+"city")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "user-city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {
        console.log("currentcity", responseJson)
        const status = responseJson.status
        if (status == "success") {

          const city_id = responseJson.current_city[0].city_id
          const city_name = responseJson.current_city[0].city
          this.setState({ city_value: city_id })
          this.setState({ city_name: city_name })
          AsyncStorage.setItem("city_name", city_name)
          this.setState({ advanced_city_id: city_id })
          if (city_id.length != 0) {
            this.getCity(country_id)

          }

        } else {

        }
      })

  }

  getCity = (country_id) => {

    fetch(ApiConstants.url + "city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        country_id: country_id

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("city", responseJson.value[0].city_id)
        const status = responseJson.status
        if (status == "success") {



          this.setState({ city_list: responseJson.value })

        } else {

        }
      })

  }

  getCountry = () => {
    // alert("work")
    fetch(ApiConstants.url + "country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {

        const status = responseJson.status
        if (status == "success") {

          this.setState({ country_list: responseJson.value })

        }
      })


  }


  getCategory = async () => {
    
    // alert("work")
    var token = await AsyncStorage.getItem("user_id")
    fetch(ApiConstants.url + "category-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer' + token
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {
        // alert(responseJson)
        // console.log(responseJson)
        const status = responseJson.status
        if (status == "success") {
          let list = responseJson.categories

          this.setState({ category_list: responseJson.categories })
          let arr = this.state.category_list.map((item, index) => {
            item.isSelected = false
            return { ...item }
          })
          console.log(arr)
          this.setState({ category_list: arr })
        }
      })


  }

  attendEvent = async () => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "attend-event", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            event_id: this.state.event_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            // alert(JSON.stringify(responseJson))
            this.setState({ loader_visible: false })
            this.getEventDetails()

          }).catch(error => {
            // alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }



  getProfile = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {

            let id = responseJson.data.id
            let age = responseJson.data.age;
            let gender = responseJson.data.gender;
            let email_id = responseJson.data.email_id;
            let mobile_number = responseJson.data.mobile_number;
            const name = responseJson.data.first_name

            this.setState({ useEmail: email_id });
            this.setState({ name: name })

            console.debug("pp", responseJson)
            let is_promoter = responseJson.data.is_promoter
            if (is_promoter == 1) {
              this.setState({ promote_button_visible: true })
            } else {
              this.setState({ promote_button_visible: false })
            }

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  followCompany = async () => {
    var token = await AsyncStorage.getItem("user_id")
    console.log(token)
    if (this.state.follow_label == "Follow") {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })

          fetch(ApiConstants.url + "follow-event", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              event_id: this.state.event_id
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.log(responseJson)
              const status = responseJson.status
              if (status == "Success") {
                this.getEventDetails()
              } else {

              }
              // alert(JSON.stringify(responseJson))
              this.setState({ loader_visible: false })


            }).catch(error => {
              alert("ree" + responseJson)
              this.setState({ loader_visible: false })


            })
        } else {


        }
      })
    } else {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })

          fetch(ApiConstants.url + "unfollow-event", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              event_id: this.state.event_id
            })
          }).then(response => response.json())
            .then(responseJson => {
              console.log(responseJson)
              const status = responseJson.status
              if (status == "Success") {
                this.getEventDetails()
              } else {

              }
              // alert(JSON.stringify(responseJson))
              this.setState({ loader_visible: false })


            }).catch(error => {
              alert("ree" + responseJson)
              this.setState({ loader_visible: false })


            })
        } else {


        }
      })
    }


  }

  getEventDetails = async () => {

    var token = await AsyncStorage.getItem("user_id")
    this.setState({ user_token: token })
    console.log(token)
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "view-event", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            id: this.state.event_id,
            notification_id: this.state.notification_id
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log("detail", responseJson)

            const status = responseJson.status
            if (status == "success") {
              let time = responseJson.value.from_time + "-" + responseJson.value.to_time
              let event_name = responseJson.value.event_name;
              let company_name = responseJson.value.company_name;
              let event_address = responseJson.value.event_address;
              let discount = responseJson.value.discount_code
              let discount_name = responseJson.value.discount_name
              let event_follower = responseJson.value.event_follower.length
              let date = responseJson.value.date_time
              let event_menu_image_list = responseJson.value.event_menu_image
              let attend_event = responseJson.value.attend_event
              let attend_status = responseJson.value.attend_status;
              let event_first_image = responseJson.value.event_first_image.image;
              let latitude = responseJson.value.lat
              let longitude = responseJson.value.lon
              let live_stream_option = responseJson.value.live_stream_option;
              let live_stream_amount = responseJson.value.live_stream_amount.value
              let event_expire = responseJson.value.event_expire
              let is_live_stream_started = responseJson.value.is_live_stream_started
              let is_joined_live_stream = responseJson.value.is_joined_live_stream
              this.setState({ is_joined_live_stream: is_joined_live_stream })
              this.setState({ live_stream_amount: live_stream_amount })
              this.setState({ live_stream_option: live_stream_option })
              this.setState({ discount_name: discount_name })
              this.setState({ expire_status: event_expire })
              this.setState({ is_live_stream_started: is_live_stream_started })
              if (attend_status == "attending") {
                this.setState({ attend_button_label: "Attending" })
              } else {
                this.setState({ attend_button_label: "Attended" })
              }





              if (latitude != null && latitude.length != 0) {
                this.setState({ latitude: Number(latitude) })
              }
              if (longitude != null && longitude.length != 0) {
                this.setState({ longitude: Number(longitude) })
              }
              this.setState({ evenImage: event_first_image })
              if (attend_event != null) {
                this.setState({ attending: true })
              } else {
                this.setState({ attending: false })
              }
              if (discount != null) {
                this.setState({ discount: discount })
                this.setState({ qr_code_visible: true })
              } else {
                this.setState({ qr_code_visible: false })

              }
              this.setState({ event_date: date.split("-").reverse().join("/") })
              console.log(event_follower)
              if (event_follower == 0) {
                this.setState({ follow_label: "Follow" })
              } else {
                this.setState({ follow_label: "Unfollow" })
              }
              this.setState({ event_name: event_name })
              this.setState({ time: time })
              this.setState({ company_name: company_name })
              this.setState({ event_address: event_address })
              // this.setState({ discount: discount })
              this.setState({ event_image_list: responseJson.value.event_images })
              this.setState({ event_menu_image_list: event_menu_image_list })
              this.setState({ event_category: responseJson.value.event_category })
              this.setState({ event_video_list: responseJson.value.event_videos })
            } else {

            }
            // alert(JSON.stringify(responseJson))
            this.setState({ loader_visible: false })


          }).catch(error => {
            alert(error)
            this.setState({ loader_visible: false })


          })
      } else {


      }
    })
  }





  updateSearch = (search) => {
    this.setState({ search });
  };

  getDirection = () => {
    Linking.openURL('http://maps.google.com/maps?daddr=' + this.state.latitude + ',' + this.state.longitude).catch(err => console.error('An error occurred', err))
  }

  setDate = (event, date) => {
    console.log(event)

    if (event.type == "set") {
      var tdate = "" + date.toISOString().split('T')[0];
      var newdate = tdate.split("-").reverse().join("/");
      this.setState({ date_value: tdate })


      this.setState({ dateshow: false })
    } else {
      this.setState({ dateshow: false })
    }
  }

  _renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ image_list_preview: true })
          this.setState({ modal_active: true })
          try {
            setTimeout(() => this.flatListRef.scrollToIndex({ index: index }), 1000);
            pos = index
            this.setState({ position: index })
          } catch (error) {
            console.warn(error);
          }
        }
        }
        style={{
          borderRadius: 5,
          height: 150,
          padding: 0,

          marginRight: 20,

        }}>
        <Image
          source={{ uri: ApiConstants.path + "images/events/" + item.image }}

          style={{
            borderRadius: 10,
            width: "100%",
            height: "100%"
          }}
        />

      </TouchableOpacity>

    )
  }
  _renderItem1 = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ menu_list_preview: true })
          this.setState({ modal_active: true })
          try {
            setTimeout(() => this.menuFlatListRef.scrollToIndex({ index: index }), 1000);
            pos = index
            this.setState({ position: index })
          } catch (error) {
            console.warn(error);
          }
        }
        }
        style={{
          borderRadius: 5,
          height: 150,
          padding: 0,

          marginRight: 20,
        }}>
        <Image
          source={
            { uri: ApiConstants.path + "images/events/" + item.menu_image }
          }
          style={{
            borderRadius: 10,
            height: "100%",
            width: "100%"
          }}
        />
        <Text>{item.menu_image}</Text>
      </TouchableOpacity>

    )
  }

  _renderItem2 = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={
          () => {
            this.setState({ video_list_preview: true })
            this.setState({ modal_active: true })
            try {
              setTimeout(() => this.videoFlatListRef.scrollToIndex({ index: index }), 1000);
              pos = index
              this.setState({ position: index })
            } catch (error) {
              console.warn(error);
            }
          }
        }
        style={{
          borderRadius: 5,
          height: 150,
          padding: 0,

          marginRight: 20,
        }}>
        {/* <Image
          source={
            { uri: "https://app.meattend.com/images/events/" + item.menu_image }
          }
          style={{
            borderRadius: 10,
            height: "100%",
            width: "100%"
          }}
        /> */}
        <VideoPlayer
          customStyles={{ playButton: { height: 0, width: 0 } }}
          video={{ uri: ApiConstants.path + "videos/events/" + item.video }}
          autoplay={false}
          defaultMuted={true}

          style={{
            borderRadius: 10,
            height: "100%",
            width: "100%",
            borderRadius: 10,
            borderColor: 'black',
            borderWidth: 1
          }}
        />
        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={images.default_play_button_icon}
            style={{
              height: 17,
              width: 17,
              tintColor: 'white'
            }} />
        </View>
      </TouchableOpacity>

    )
  }
  renderCategoryItem = (item) => {
    return (
      <Text numberOfLines={1} style={[font.regular, font.graycolor]}>{item.categoery}</Text>

    )

  }



  emitServer = async () => {
    console.log("working click")
    if (this.state.is_joined_live_stream == 1) {
      this.props.navigation.navigate("LiveStreamScreen", {
        event_id: this.state.event_id,
        name: this.state.name,
        token: this.state.user_token,
        email: this.state.useEmail
      });
    } else {
      PayPal.initialize(PayPal.NO_NETWORK, "AV8PeuErLWzzUNJgQ7TunB2bXFDtU0uynblUryuwA_hxOiisxZbJ231xdqDYoelYjObb-0lO7acSJluA");
      PayPal.pay({
        price: this.state.live_stream_amount,
        currency: 'USD',
        description: this.state.event_name + ' Live Stream',
      }).then(confirm => {
        console.log(confirm)
        this.save_user(confirm.response.id)

      })
        .catch(error => {
          Toast.show({
            title: 'Payment Failed',
            text: "",
            bottom: 100,
            color: '#000',
            timeColor: 'red',
            backgroundColor: COLORS.textblue.color,
            timing: 5000,
            position: 'bottom',
          })
        }

        );
    }



  }
  save_user = async (confirm) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "save-live-stream-users", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            event_id: this.state.event_id,
            charge_id: confirm,
            paid_status: "succeeded"
          })
        }).then(response => response.json())
          .then(responseJson => {
            console.log(responseJson)
            // alert(JSON.stringify(responseJson))
            this.setState({ loader_visible: false })

            this.props.navigation.navigate("LiveStreamScreen", {
              event_id: this.state.event_id,
              name: this.state.name,
              token: this.state.user_token,
              email: this.state.useEmail
            });

          }).catch(error => {
            // alert("ree" + responseJson)
            this.setState({ loader_visible: false })


          })
      } else {

        Toast.show({
          title: 'Network Error!',
          text: "",

          color: '#000',
          timeColor: 'red',
          backgroundColor: COLORS.textblue.color,
          timing: 5000,
          position: 'bottom',
        })
      }
    })
  }
  scrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.flatListRef.scrollToOffset({ offset });
    setTimeout(() => this.flatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }
  menuScrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.menuFlatListRef.scrollToOffset({ offset });
    setTimeout(() => this.menuFlatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }
  videoScrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.videoFlatListRef.scrollToOffset({ offset });
    setTimeout(() => this.videoFlatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }

  getList = (item, index) => {


    const newArrData = this.state.category_list.map((e, index) => {

      if (item.id == e.id) {
        item.isSelected = !item.isSelected

      }
      return {
        ...e

      }
    })

    this.setState({ category_list: newArrData })
    console.log(this.state.category_list)
    // this.setState({ item_id: item.id })
  }

  renderFilterCategoryItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.getList(item, index)}
        style={{ flexDirection: 'row', marginRight: 40, marginBottom: 10, marginTop: 10 }}>
        {!item.isSelected ? <Image style={{
          height: 20,
          width: 20,

        }} source={images.unselected_icon} />
          : <Image style={{
            height: 20,
            width: 20,

          }} source={images.selected_icon} />
        }
        <Text style={{
          alignSelf: 'center',
          marginLeft: 5,
          ...FONTS.regularsizeRegular,
          color: "#646464",
          fontWeight: '700'
        }}>{item.category}</Text>
      </TouchableOpacity>

    )

  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

        </Root>
        <View style={styles.headerbg}>
          <View style={styles.selfalign}>
            <Image
              source={images.large_logo_icon}
              style={{
                resizeMode: "contain",
                height: 40,
                width: 70
              }}
            />
          </View>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              this.setState({ time_city_visible: true })
            }}
            style={[styles.rowgroup, styles.stretchitem]}>
            <Image
              source={images.location_icon}
              style={{
                height: 18,
                width: 18,
                marginRight: 5,
                tintColor: COLORS.textgreen.color
              }}
            />
            <Text style={[font.bold, styles.textstyle]}>{this.state.city_name}<Text style={[font.bold, styles.textstyle], { color: "#56D6BE", }}> . </Text></Text><Text style={[font.bold, styles.textstyle]}>Today - {this.state.time_value}</Text>
          </TouchableOpacity>
        </View>
       {/* <View style={[styles.input_field, styles.topspace]}>
          <View style={[!this.state.modal_active ? styles.shadowbox : styles.modalshadowbox, styles.searchSection]}>
            <TextInput
              returnKeyType="search"
              numberOfLines={1}
              style={[
                font.regular,
                font.bold,
                styles.inputStyle,

              ]}
              placeholder={
                "Search for Events and Peoples.."
              }
              onSubmitEditing={() => {

                AsyncStorage.setItem("keyword", this.state.search_keyword)
                this.props.navigation.goBack()
                // alert("dsd"+this.state.index)
              }}
              value={this.state.search_keyword}
              // placeholderTextColor={[font.semibold,color.greyText,]}
              onChangeText={(searchString) => {
                this.setState({ search_keyword: searchString })

              }}
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              style={styles.searchIcon}
              onPress={() => {

                AsyncStorage.setItem("keyword", this.state.search_keyword)
                this.props.navigation.goBack()


              }}
            >
              <Image
                style={{
                  width: 18,
                  height: 18,
                  position: "absolute",
                  alignSelf: 'center',
                  tintColor: 'black'
                }}
                source={images.search_icon}
              />
            </TouchableOpacity>

          </View>
          <View style={[!this.state.modal_active ? styles.shadowbox : styles.modalshadowbox, styles.squrebox]}>
            <TouchableOpacity
              onPress={() => {
                this.setState({ advance_filter_visible: true })
              }}
            >
              <Image
                source={images.filter_icon}
                style={{
                  height: 25,
                  width: 25,
                  tintColor: COLORS.white.color
                }}
              />
            </TouchableOpacity>

          </View>
        </View> */}
        <ScrollView>


          <View
            style={{
              backgroundColor: '#fff',


            }}
          >
            <View
              style={{
                backgroundColor: '#fff',
                height: 50,

              }}
            >
              {/* <View style={{
                flexDirection: 'row',
                flex: 1,
                height: 10,
                marginHorizontal: 20,

              }}> */}
              {/* <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack()
                  }}
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    marginTop: 10
                  }}
                >
                  <Image
                    source={images.prevarrow_icon}
                    style={{
                      alignSelf: 'center',
                      tintColor: COLORS.textblue.color,
                      height: 15,
                      width: 9
                    }}
                  />
                </TouchableOpacity> */}
              {/* <View
                  style={{
                    flexDirection: 'row',
                    flex: .6,
                    height: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10
                  }}
                >
                  <Text
                    style={{
                      ...FONTS.boldsizeLarge,
                      color: COLORS.textblue.color
                    }}
                  >Event Details</Text>
                </View> */}



              {/* <View
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10
                  }}
                >

                </View> */}
              {/* </View> */}
            </View>
            <View style={{ flex: 1, backgroundColor: "#fff", marginTop: 150 }}>

              <View style={{ flexDirection: "row", flex: 1, paddingHorizontal: 20, paddingBottom: 18, paddingTop: 10 }}>


                {this.state.expire_status != "inactive" ?
                  <View style={{ flex: .5, flexDirection: 'row', marginRight: 16 }}>
                    {!this.state.attending ?
                      <TouchableOpacity
                        onPress={this.attendEvent}
                        style={[styles.Attendwidthspace, styles.attend_buttonStyle, font.textWhiteColor]}
                      >
                        <Text style={[styles.buttonTextStyle, font.bold]}>Attend</Text>
                      </TouchableOpacity> :
                      <TouchableOpacity
                        activeOpacity={this.state.attend_button_label == "Attending" ? 0 : 1}
                        onPress={() => {
                          if (this.state.attend_button_label == "Attending") {
                            this.attendEvent()
                          }


                        }}
                        style={[styles.Attendwidthspace, this.state.attend_button_label == "Attending" ? styles.attending_buttonStyle : styles.attendedbuttonStyle]}>
                        {this.state.attend_button_label == "Attending" ? <Image
                          source={images.tick_icon} resizeMode="contain"
                          style={{
                            marginRight: 5,
                            alignSelf: "center",
                            height: 15,
                            width: 15
                          }} /> : <View></View>}
                        <Text style={[this.state.attend_button_label == "Attending" ? font.textblue : font.textwhite, font.bold]}>{this.state.attend_button_label}</Text>
                      </TouchableOpacity>}
                  </View> :
                  <View style={{ flex: .5, flexDirection: 'row', marginRight: 16 }}>


                    {!this.state.attending ?
                      <TouchableOpacity

                        style={[styles.Attendwidthspace, styles.attend_desable_buttonStyle, font.textWhiteColor]}
                      >
                        <Text style={[styles.buttonTextStyle, font.bold]}>Attend</Text>
                      </TouchableOpacity> :
                      <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                          if (this.state.attend_button_label == "Attending") {
                            // this.attendEvent()
                          }


                        }}
                        style={[styles.Attendwidthspace, this.state.attend_button_label == "Attending" ? styles.desable_attending_buttonStyle : styles.attendedbuttonStyle]}>
                        {this.state.attend_button_label == "Attending" ? <Image
                          source={images.tick_icon} resizeMode="contain"
                          style={{
                            marginRight: 5,
                            alignSelf: "center",
                            height: 15,
                            width: 15
                          }} /> : <View></View>}
                        <Text style={[this.state.attend_button_label == "Attending" ? font.textblue_disable : font.textwhite, font.bold]}>{this.state.attend_button_label}</Text>
                      </TouchableOpacity>}


                  </View>}

                {this.state.expire_status != "inactive" && this.state.is_live_stream_started == "1" ? <TouchableOpacity
                  onPress={() => this.emitServer()}
                  style={[styles.widthspace, styles.liveStream, styles.colornone,]}
                >
                  <Text style={[styles.buttonTextStyle, font.bold, font.textblue]}>Live Stream for ${this.state.live_stream_amount}</Text>
                </TouchableOpacity> :

                  <TouchableOpacity
                    activeOpacity={1}
                    style={[styles.widthspace, styles.disableLiveStream, styles.disableColornone,]}
                  >
                    <Text style={[styles.buttonTextStyle, font.bold, font.textblue_disable]}>Live Stream for ${this.state.live_stream_amount}</Text>
                  </TouchableOpacity>
                }
              </View>

              <View>

                <Text style={[font.blackcolor, font.sizeMedium, font.bold, styles.labelleftspace]}>{this.state.event_name}</Text>
                <View style={{ marginBottom: 4 }}></View>
                <Text style={[font.graycolor, font.sizeVeryRegular, font.regular, styles.labelleftspace, font.opacityrange]}>{this.state.time} | {this.state.event_date}  </Text>
                <Text style={[font.lightgray, font.sizeRegular, font.bold, styles.labelleftspace, styles.smallTopSpace, font.opacityrange]}>{this.state.company_name}</Text>
                <View style={{ marginBottom: 4 }}></View>
                <View style={[styles.rowgroup, styles.stretchitem, styles.gpsleftspace, font.topspaceing]}>
                  <Image
                    source={images.location_icon}

                    resizeMode="stretch"
                    style={{
                      height: 13,
                      width: 13,
                      marginRight: 2,
                      alignSelf: "center",
                      tintColor: COLORS.textgreen.color
                    }}
                  />
                  <Text numberOfLines={1} style={[font.graycolor, font.sizeVeryRegular, font.regular, font.opacityrange]}>
                    {this.state.event_address}
                  </Text>
                </View>
                {this.state.expire_status != "inactive" ?
                  <TouchableOpacity
                    onPress={() => {
                      this.followCompany()
                    }}
                    style={[styles.widthspace, this.state.follow_label == "Follow" ? styles.buttonStyle : styles.unfollow_buttonStyle, styles.leftspace, styles.smallwidth]}
                  >
                    <Text style={[this.state.follow_label == "Follow" ? styles.buttonTextStyle : styles.bluebuttonTextStyle, font.bold]}>{this.state.follow_label}</Text>
                  </TouchableOpacity> :
                  <View

                    style={[styles.widthspace, this.state.follow_label == "Follow" ? styles.desablebuttonStyle : styles.desable_unfollow_buttonStyle, styles.leftspace, styles.smallwidth]}
                  >
                    <Text style={[this.state.follow_label == "Follow" ? styles.buttonTextStyle : styles.bluebuttondesableTextStyle, font.bold]}>{this.state.follow_label}</Text>
                  </View>
                }
                <View style={styles.bgdiv}>

                  {this.state.event_category.map((item, key) => {

                    return (
                      <View>
                        {
                          key != this.state.event_category.length - 1 ? <Text numberOfLines={1} style={[font.regular, font.graycolor]}>{item.category},</Text>
                            : <Text numberOfLines={1} style={[font.regular, font.graycolor]}>{item.category}</Text>
                        }

                      </View>

                    )

                  }
                  )
                  }

                </View>
                <View style={{ flexDirection: "row", flex: 1, paddingHorizontal: 20, marginBottom: 18, paddingTop: 10 }}>
                  {this.state.promote_button_visible && this.state.expire_status != "inactive" ?
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("PromoterScreen", { event_id: this.state.event_id, event_image: this.state.evenImage })}
                      style={[styles.widthspace, styles.buttonStyle, font.bgcolorbtn, font.textWhiteColor]}
                    >
                      <Text style={[styles.buttonTextStyle, font.bold]}>Promote</Text>
                    </TouchableOpacity>
                    : <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.widthspace, styles.desablePromotebuttonStyle, font.textWhiteColor]}
                    >
                      <Text style={[styles.buttonTextStyle, font.bold]}>Promote</Text>
                    </TouchableOpacity>
                  }
                  <TouchableOpacity
                    onPress={this.getDirection}
                    style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.borderbluecolor]}
                  >
                    <Text style={[styles.buttonTextStyle, font.bold, font.textgreen]}>Directions</Text>
                  </TouchableOpacity>
                </View>
                <Text style={[font.lightgray, font.sizeRegular, font.bold, styles.leftspace]}>Photos</Text>
                <View style={{ marginLeft: 20, flex: 1, marginBottom: 18, flexDirection: 'row', justifyContent: 'center', }}>
                  {this.state.event_image_list.length != 0 ? <Carousel
                    layout={"default"}
                    ref={ref => this.carousel = ref}
                    data={this.state.event_image_list}
                    sliderWidth={150}
                    itemWidth={150}
                    inactiveSlideOpacity={1}
                    inactiveSlideScale={0.99}
                    renderItem={this._renderItem}
                    onSnapToItem={index => this.setState({ activeIndex: index })} />
                    :
                    <View style={{
                      height: 50,

                      marginBottom: 10,
                      width: "94%",
                      marginRight: 20,
                      backgroundColor: "#FFF6C6",
                      borderRadius: 5,
                      borderColor: "#FEF1AF",

                      justifyContent: 'center',
                      paddingLeft: 8
                    }}>
                      <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No data found</Text>
                    </View>
                  }
                </View>
                <Text style={[font.lightgray, font.sizeRegular, font.bold, styles.leftspace]}>Menus</Text>
                <View style={{ marginLeft: 20, flex: 1, flexDirection: 'row', marginBottom: 18 }}>
                  {
                    this.state.event_menu_image_list.length != 0 ?
                      <Carousel
                        layout={"default"}
                        ref={ref => this.carousel = ref}
                        data={this.state.event_menu_image_list}
                        sliderWidth={150}
                        itemWidth={150}

                        inactiveSlideOpacity={1}
                        inactiveSlideScale={0.99}
                        renderItem={this._renderItem1}
                        onSnapToItem={index => this.setState({ activeIndex: index })} />
                      :
                      <View style={{
                        height: 50,

                        marginBottom: 10,
                        width: "94%",
                        marginRight: 20,
                        backgroundColor: "#FFF6C6",
                        borderRadius: 5,
                        borderColor: "#FEF1AF",

                        justifyContent: 'center',
                        paddingLeft: 8
                      }}>
                        <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No data found</Text>
                      </View>
                  }
                </View>

                <Text style={[font.lightgray, font.sizeRegular, font.bold, styles.leftspace, styles.topspace]}>Videos</Text>
                <View style={{ marginLeft: 20, flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                  {this.state.event_video_list.length != 0 ? <Carousel
                    layout={"default"}
                    ref={ref => this.carousel = ref}
                    data={this.state.event_video_list}
                    sliderWidth={150}
                    itemWidth={150}

                    inactiveSlideOpacity={1}
                    inactiveSlideScale={0.99}
                    renderItem={this._renderItem2}
                    onSnapToItem={index => this.setState({ activeIndex: index })} />
                    :
                    <View style={{
                      height: 50,

                      marginBottom: 10,
                      width: "94%",

                      backgroundColor: "#FFF6C6",
                      borderRadius: 5,
                      borderColor: "#FEF1AF",

                      justifyContent: 'center',
                      paddingLeft: 8
                    }}>
                      <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No data found</Text>
                    </View>
                  }
                </View>
                {this.state.attending && this.state.qr_code_visible ? <View>
                  <Text style={[font.lightgray, font.sizeRegular, font.bold, styles.leftspace]}>Your Entry Token</Text>
                  <Text style={[font.lightgray, font.sizeVeryRegular, font.bold, styles.leftspace, font.opacityrange]}>{this.state.discount_name} by scanning this QR upon entry</Text>
                  <View style={{ height: 150, alignItems: "center", justifyContent: "center" }}>
                    <QRCode
                      value={String(this.state.discount)}
                      logoBackgroundColor='transparent'
                    />
                  </View>
                </View> : <View></View>}

              </View>
            </View>
          </View>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            zoomEnabled={true}
            minZoomLevel={16}
            region={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,

            }}
          >
            <Marker
              coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}>
              <Image source={images.location_map_pointer_icon} style={{ height: 35, width: 35 }} />
              <Callout

                style={{ backgroundColor: "white", borderRadius: 10, height: 50, alignItems: 'center' }}>
                <Text style={{ ...FONTS.boldsizeMedium }}>{this.state.company_name}</Text>
                <Text
                  style={{
                    width: 140,
                    ...FONTS.regularsizeVeryRegular,
                    textAlign: 'center'
                  }}
                >{this.state.event_address}</Text>
              </Callout>
            </Marker>
          </MapView>
          <Modal
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.onCloseModel();
            }}
          // onDismiss={() => this.gotoLogin()}
          >
            <View style={{ backgroundColor: "#fff", minHeight: 300, margin: 15, borderRadius: 10, padding: 25 }}>
              <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
              <Text style={[styles.smalltitle, font.bold]}>Location</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1, paddingRight: 20 }}>
                  <TextInput
                    style={[font.regular, styles.inputStyle, styles.shadowbox]}
                    underlineColorAndroid="#f000"
                    placeholder="London, UK"
                    placeholderTextColor="#747474"
                    autoCapitalize="sentences"
                    returnKeyType="next"
                    blurOnSubmit={false}
                  />
                </View>
                <View>
                  <Text style={[font.bold, styles.changetxt]}>Change</Text>
                </View>
              </View>
              <Text style={[styles.smalltitle, font.bold]}>Time</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1, paddingRight: 20 }}>
                  <TextInput
                    style={[font.regular, styles.inputStyle, styles.shadowbox]}
                    underlineColorAndroid="#f000"
                    placeholder="Today,Noon"
                    placeholderTextColor="#747474"
                    autoCapitalize="sentences"
                    returnKeyType="next"
                    blurOnSubmit={false}
                  />
                </View>
                <View>
                  <Text style={[font.bold, styles.changetxt]}>Change</Text>
                </View>
              </View>
            </View>
          </Modal>
        </ScrollView>
        <View style={[styles.input_field, styles.topspaces]}>
          <View style={[!this.state.modal_active ? styles.shadowbox : styles.modalshadowbox, styles.searchSection]}>
            <TextInput
              returnKeyType="search"
              numberOfLines={1}
              style={[
                font.regular,
                font.bold,
                styles.inputStyle,

              ]}
              placeholder={
                "Search for Events and Peoples.."
              }
              onSubmitEditing={() => {

                AsyncStorage.setItem("keyword", this.state.search_keyword)
                this.props.navigation.goBack()
                // alert("dsd"+this.state.index)
              }}
              value={this.state.search_keyword}
              // placeholderTextColor={[font.semibold,color.greyText,]}
              onChangeText={(searchString) => {
                this.setState({ search_keyword: searchString })

              }}
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              style={styles.searchIcon}
              onPress={() => {

                AsyncStorage.setItem("keyword", this.state.search_keyword)
                this.props.navigation.goBack()


              }}
            >
              <Image
                style={{
                  width: 18,
                  height: 18,
                  position: "absolute",
                  alignSelf: 'center',
                  tintColor: 'black'
                }}
                source={images.search_icon}
              />
            </TouchableOpacity>

          </View>
          <View style={[!this.state.modal_active ? styles.shadowbox : styles.modalshadowbox, styles.squrebox]}>
            <TouchableOpacity
              onPress={() => {
                this.setState({ advance_filter_visible: true })
              }}
            >
              <Image
                source={images.filter_icon}
                style={{
                  height: 25,
                  width: 25,
                  tintColor: COLORS.white.color
                }}
              />
            </TouchableOpacity>

          </View>
        </View>

        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.image_list_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "black",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.flatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.scrollToIndexFailed.bind(this)}
              data={this.state.event_image_list}
              renderItem={({ item }) => (
                <View>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <ImageBackground source={{ uri: ApiConstants.path + "images/events/" + item.image }}
                      resizeMode="contain"
                      style={{
                        height: SIZES.height / 1.1,
                        marginVertical: 20,
                        width: SIZES.width,
                        borderRadius: 8
                      }}
                    />




                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ image_list_preview: false })
                this.setState({ modal_active: false })
              }}
              style={{

                top: 50,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.event_image_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.event_image_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.event_image_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.menu_list_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "black",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.menuFlatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.menuScrollToIndexFailed.bind(this)}
              data={this.state.event_menu_image_list}
              renderItem={({ item }) => (
                <View>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <ImageBackground source={{ uri: ApiConstants.path + "images/events/" + item.menu_image }}
                      resizeMode="contain"
                      style={{
                        height: SIZES.height / 1.1,
                        marginVertical: 20,
                        width: SIZES.width,
                        borderRadius: 8
                      }}
                    />




                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ menu_list_preview: false })
                this.setState({ modal_active: false })
              }}
              style={{

                top: 50,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.menuFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.event_menu_image_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.event_menu_image_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.menuFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.event_menu_image_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>
        <Modal

          activeOpacity={1}
          transparent={true}
          visible={this.state.video_list_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "black",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.videoFlatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.videoScrollToIndexFailed.bind(this)}
              data={this.state.event_video_list}
              renderItem={({ item }) => (
                <View>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <VideoPlayer
                      autoplay={this.state.video_list_preview}
                      video={{ uri: ApiConstants.path + "videos/events/" + item.video }}
                      autoplay={false}
                      defaultMuted={true}
                      disableControlsAutoHide
                      videoHeight={300}

                      style={{

                        backgroundColor: 'black',
                        alignSelf: 'center',
                        height: 300,
                        width: SIZES.width,

                      }}
                    />




                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ video_list_preview: false })
                this.setState({ modal_active: false })
              }}
              style={{

                top: 30,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.videoFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.event_video_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.event_video_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.videoFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.event_video_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>


        <Modal
          transparent={true}
          visible={this.state.time_city_visible}

        // onDismiss={() => this.gotoLogin()}
        >
          <TouchableOpacity

            onPress={() => this.setState({ time_city_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',

            }}

          >

            <View style={{ backgroundColor: "#fff", minHeight: 300, maxHeight: 350, margin: 15, borderRadius: 10, padding: 25 }}>

              <View style={{ flexDirection: 'row', flex: 1 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ time_city_visible: false })
                  }}
                  style={{ marginTop: 4, flex: .2, justifyContent: 'center' }}
                >

                  <Image
                    source={images.cancel_icon}
                    style={{
                      tintColor: COLORS.textblue.color,
                      height: 15,
                      width: 15,
                      
                    }}
                  />
                </TouchableOpacity>

                <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
                <View style={{ flex: .2 }}></View>
              </View>

              <Text style={[styles.smalltitle, font.bold]}>Location</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1, paddingRight: 20 }}>
                  <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                    <Dropdown
                      style={{ width: "90%", marginLeft: 10 }}

                      data={this.state.city_list}
                      searchPlaceholder="Search"
                      labelField="city"
                      value={Number(this.state.city_value)}
                      valueField="city_id"
                      placeholder="Select item"
                      onChange={item => {
                        this.setState({ city_name: item.city })
                        this.setState({ city_value: item.city_id })
                        console.log('selected', item);
                        const city = item.city_id;
                        AsyncStorage.setItem("city_value", String(item.city_id))

                      }}


                    />
                  </View>
                </View>

              </View>
              <Text style={[styles.smalltitle, font.bold]}>Time</Text>
              <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <View style={{ flex: 1, paddingRight: 20 }}>
                  <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                    <Dropdown
                      style={{ width: "90%", marginLeft: 10 }}
                      value={this.state.time_value}
                      data={data}
                      maxHeight={100}
                      searchPlaceholder="Search"
                      labelField="label"
                      valueField="value"
                      placeholder="Select item"
                      onChange={item => {
                        AsyncStorage.setItem("time_value", item.value)

                        this.setState({ time_value: item.value })
                        console.log('selected', item.value);
                      }}


                    />
                  </View>
                </View>

              </View>
            </View>
          </TouchableOpacity>

        </Modal>

        <Modal
          visible={this.state.advance_filter_visible}
        >
          <TouchableOpacity
            activeOpacity={1}
            style={{
              height: SIZES.height,
            }}>
            <View
              style={{
                marginHorizontal: 35,
                height: 50,

              }}
            >
              <View style={{
                flexDirection: 'row',
                flex: 1,
                height: 10,


              }}>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    marginTop: 30,



                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ advance_filter_visible: false })
                    }}
                    style={{
                      justifyContent: 'center',


                      height: 30,
                      width: 60,

                    }}
                  >
                    <Image
                      source={images.cancel_icon}
                      style={{
                        height: 15,
                        width: 15,
                        tintColor: COLORS.textblue.color,
                        marginTop:10

                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: .6,
                    justifyContent: 'center',
                    height: 30,
                    alignItems: 'center',
                    marginTop: 30
                  }}
                >
                  <Text
                    style={{
                      ...FONTS.boldsizeLarge,
                      color: COLORS.textblue.color
                    }}
                  >Advanced Filters</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: .2,
                    height: 30,
                    marginTop: 30,
                    alignSelf: 'center',
                    justifyContent: 'flex-end',

                  }}
                >

                </View>
              </View>
            </View>


            <View style=
              {{
                marginTop: 30,
                flex: 1,

              }}
            >
              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeRegular,
                  color: COLORS.textblue.color
                }}
              >
                City
              </Text>
              <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                <Dropdown
                  style={{ width: "90%", marginLeft: 10 }}
                  data={this.state.city_list}
                  value={Number(this.state.advanced_city_id)}
                  searchPlaceholder="Search"
                  labelField="city"
                  valueField="city_id"
                  placeholder="Select item"

                  onChange={item => {
                    this.setState({ advanced_city_id: item.city_id })
                    this.setState({ city_name: item.city })
                    console.log('selected', this.state.city_name);
                  }}


                />
              </View>
              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeRegular,
                  color: COLORS.textblue.color
                }}
              >
                Country
              </Text>
              <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                <Dropdown

                  style={{ width: "90%", marginLeft: 10 }}
                  value={Number(this.state.advanced_country_id)}
                  data={this.state.country_list}
                  searchPlaceholder="Search"
                  labelField="country"
                  valueField="country_id"
                  placeholder="Select item"

                  onChange={item => {
                    this.setState({ advanced_country_id: item.country_id })
                    this.setState({ country_name: item.country })
                    this.getCity(item.country_id)
                    console.log('selected', item);
                  }}


                />

              </View>
              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeRegular,
                  color: COLORS.textblue.color
                }}
              >
                Date
              </Text>
              <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                <TextInput
                  style={[styles.inputStyle, font.regular]}
                  value={this.state.date_value.split("-").reverse().join("/")}
                  placeholder="Select Date"
                  placeholderTextColor="#747474"
                  keyboardType="name-phone-pad"
                  blurOnSubmit={false}
                  underlineColorAndroid="#f000"

                />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ dateshow: true })

                  }}
                  style={{
                    position: 'absolute',
                    right: 0,
                    width: 40,
                    height: "100%",
                    alignSelf: 'flex-end',
                    justifyContent: 'center'
                  }}
                >
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      alignSelf: 'center',
                      tintColor: COLORS.textblue.color
                    }}

                    source={images.date_pcker_icon}
                  />
                </TouchableOpacity>

              </View>




              {this.state.dateshow == true ?
                (<DateTimePicker
                  value={this.state.date}
                  mode={this.state.mode}
                  is24Hour={true}
                  onTouchCancel={(value) => { alert(value) }}
                  display='default'
                  onChange={this.setDate}
                ></DateTimePicker>) : (<View></View>)
              }

              <Text
                style={{
                  marginLeft: 35,
                  ...FONTS.regularsizeMedium,
                  color: "#333333",
                  fontWeight: '700',
                  marginBottom: 30,
                  marginTop: 30
                }}
              >
                Categories
              </Text>

              <View style={{ width: SIZES.width, marginLeft: 35, }}>


                <FlatList

                  numColumns={3}
                  data={this.state.category_list}
                  renderItem={this.renderFilterCategoryItem}
                  keyExtractor={item => item.id}
                />
              </View>



              <TouchableOpacity
                onPress={() => {
                  let country_name = this.state.country_name;
                  let city_name = this.state.city_name;
                  let date = this.state.date_value

                  AsyncStorage.setItem("filter_country", country_name);
                  AsyncStorage.setItem("filter_city", city_name)
                  AsyncStorage.setItem("filter_date", date)

                  this.props.navigation.goBack()
                }}
                style={{
                  width: "85%",
                  height: 40,

                  backgroundColor: COLORS.textgreen.color,
                  marginHorizontal: 20,
                  alignSelf: 'center',
                  borderRadius: 10,
                  justifyContent: 'center',
                  bottom: 35,
                  position: 'absolute'
                }}
              >
                <Text style={{
                  color: "white",
                  ...FONTS.boldsizeMedium,
                  alignSelf: 'center'
                }}>
                  Search
                </Text>
              </TouchableOpacity>
            </View>



          </TouchableOpacity>

        </Modal>

      </View>
    );
  }
};


const styles = StyleSheet.create({
  padding: {
    paddingLeft: 0,
  },
  slide: {
    flex: 1

  },
  slideInnerContainer: {
    width: slideWidth,
    flex: 1

  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: 200,
    position: 'absolute',

    width: SIZES.width
  },
  bgdiv: {
    backgroundColor: "#EEFBF9",
    marginLeft: 20,
    marginTop: 10,
    marginHorizontal: 20,
    borderRadius: 8,
    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 4,
    marginTop: 20,
    flexDirection: 'row',
    paddingLeft: 15
  },
  smallwidth: {
    width: 100,
  },
  leftspace: {

    marginHorizontal: 20,
    letterSpacing: 0.5,
    marginBottom: 10,

  },
  topspace: {

    marginTop: 10

  },

  labelleftspace: {

    marginHorizontal: 20,
    letterSpacing: 0.5,


  },
  gpsleftspace: {

    marginHorizontal: 18,
    letterSpacing: 0.5,

  },
  smallTopSpace: {
    marginTop: 10,
  },
  topSpace: {
    marginTop: 20,
  },
  colornone: {
    backgroundColor: "#fff",
    borderColor: COLORS.textblue.color,
    borderWidth: 1,

  },
  disableColornone: {
    backgroundColor: "#fff",
    borderColor: '#abb6ff',
    borderWidth: 1,

  },
  widthspace: {
    width: "45%"
  },
  Attendwidthspace: {
    width: "100%"
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  poptitle: {
    color: "#293272",
    fontSize: 18,
    flex: .6,
    alignSelf: 'center',
    textAlign: 'center'
  },
  CitySectionStyle: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },

  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginLeft: 12,
    marginRight: 3,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspaces: {
    marginTop: 20,
  },
  input_field: {
    flexDirection: "row",
    marginRight: 10,
    position: 'absolute',
    top: 75
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',

    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginLeft: 14,

    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 24,
  },
  AdvanceFilterSectionStyle: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 8,
  },
  modalshadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.1,
    shadowRadius: 8,

  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
    marginLeft: 21
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 135,
    paddingHorizontal: 40,
    paddingBottom: 10,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 150,
    width: 150,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,
    flex: .5,
    marginTop: 10,
    marginBottom: 0,

  },
  desablebuttonStyle: {
    backgroundColor: '#abb6ff',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,
    flex: .5,
    marginTop: 10,
    marginBottom: 0,

  },
  desablePromotebuttonStyle: {
    backgroundColor: '#bffff3',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,
    flex: .5,
    marginTop: 10,
    marginBottom: 0,

  },
  liveStream: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: COLORS.textblue.color,
    height: 35,
    flex: .5,
    alignItems: 'center',
    borderRadius: 8,

    marginTop: 10,
    marginBottom: 0,
  },
  disableLiveStream: {
    backgroundColor: '#abb6ff',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: COLORS.textblue.color,
    height: 35,
    flex: .5,
    alignItems: 'center',
    borderRadius: 8,

    marginTop: 10,
    marginBottom: 0,
  },
  attending_buttonStyle: {
    flexDirection: "row",
    borderWidth: 1.5,
    borderColor: "#293272",
    borderRadius: 8,
    paddingVertical: 6,
    justifyContent: "center",
    marginTop: 10,



  },
  desable_attending_buttonStyle: {
    flexDirection: "row",
    borderWidth: 1.5,
    borderColor: "#abb6ff",
    borderRadius: 8,
    paddingVertical: 6,
    justifyContent: "center",
    marginTop: 10,



  },

  unfollow_buttonStyle: {


    borderWidth: 1.5,
    justifyContent: 'center',
    borderColor: '#293272',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,
    flex: .5,
    marginTop: 10,
    marginBottom: 0,

  },

  desable_unfollow_buttonStyle: {


    borderWidth: 1.5,
    justifyContent: 'center',
    borderColor: '#abb6ff',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,
    flex: .5,
    marginTop: 10,
    marginBottom: 0,

  },

  attendedbuttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    justifyContent: "center",
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,


  },
  attend_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,

    marginTop: 10,
    marginBottom: 0,
  },
  attend_desable_buttonStyle: {
    backgroundColor: "#abb6ff",
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    alignItems: 'center',
    borderRadius: 8,

    marginTop: 10,
    marginBottom: 0,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 4, //
    alignSelf: "center",
  },
  desableButtonTextStyle: {
    color: '#b5b5b5',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  bluebuttonTextStyle: {
    color: '#293272',
    fontWeight: "bold",
    fontSize: 14,

    alignSelf: "center",
  },
  bluebuttondesableTextStyle: {
    color: '#abb6ff',
    fontWeight: "bold",
    fontSize: 14,

    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
