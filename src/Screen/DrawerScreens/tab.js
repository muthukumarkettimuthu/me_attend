import React, { useState, createRef, useEffect, useRef } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
  Keyboard,
  TouchableOpacity,
  ScrollView,
} from 'react-native';import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../DrawerScreens/HomeScreen';
import SettingScreen from '../DrawerScreens/SettingScreen';
import MapScreen from './MapScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import profile from './profile';
import Cameras from '../Cameras'
import notiify from '../notify';
import { COLORS, FONTS, images } from '../../assets/styles/theme';
import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../../services/APIConstants'
import AsyncStorage from '@react-native-community/async-storage';
const Tab = createBottomTabNavigator();
let number=1;
const tab = () => {

  let [count, setCount] = useState(0);
  const MINUTE_MS = 1000;
  useEffect(() => {
    getNotificationList();
    const interval = setInterval(async() => {
      var temp = await AsyncStorage.getItem("count")
      if(temp!=null&&temp!=count){
        setCount(temp)
      }

      if(temp==0){
        setCount(temp)
      }
      
    }, MINUTE_MS);
  
    return () => clearInterval(interval);
      
      
  },[])
 
  const getNotificationList = async () => {
    var saved_count = await AsyncStorage.getItem("count")
    if(saved_count!=null){
      setCount(saved_count)
    }
  
    
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
       

        fetch(ApiConstants.url + "get-notification" , {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
            
            let count = responseJson.notification_count
            
            
           getCount(count);
           

          }).catch(error => {
        
            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  const getCount = async(count) =>{
    AsyncStorage.setItem("count",String(count))
    var saved_count = await AsyncStorage.getItem("count")
    setCount(count)
  }


  return (
    <Tab.Navigator
    tabBarOptions={{
      showLabel:false,
      style:{
        position:"absolute",
        bottom:0,
        left:0,
        right:0,
        backgroundColor:"#fff"
      }
    }}
    >
      <Tab.Screen name="Home" component={HomeScreen} options={{
          
          tabBarIcon: ({ focused }) => (
            <View>
            <Image
            source={images.home_icon} resizeMode="contain"

            style={{
              height:20,
              width:20,
              tintColor: focused ? "#293272" : "#C8C8C8"
            }}
            />
            </View>
          )
      }}
        
        />

      <Tab.Screen name="add"   component={MapScreen}  options={{
          
          tabBarIcon: ({ focused }) => (
            <View>
            <Image
            source={images.location_icon} resizeMode="contain"

            style={{
              height:20,
              width:20,
              tintColor: focused ? "#293272" : "#C8C8C8"
            }}
            />
            </View>
          )
      }}/>

      <Tab.Screen name={"notiify"} component={notiify}  options={{
         
          tabBarIcon: ({focused}) => (
            <View>
              
            <Image
            source={images.bell_icon} resizeMode="contain"

            style={{
              height:20,
              width:20,
              tintColor: focused ? "#293272" : "#C8C8C8"
            }}
            
            />
            {count!=0?<View style={{width:15,
                height:15,right:3,bottom:10,
                borderRadius:7.5,
                justifyContent:'center',
                alignItems:'center',
                position:'absolute',
                backgroundColor:'red'}}>
                  <Text style={{fontSize:8,...FONTS.regularsizeVerySmall,color:COLORS.white.color}}>{count}</Text>
                </View>:<View></View>}
            </View>
          )
      }}/>

      <Tab.Screen name="mix" component={Cameras}  options={{
          
          tabBarIcon: ({ focused }) => (
            <View>
            <Image
            source={images.camera_icon} resizeMode="contain"

            style={{
              height:20,
              width:20,
              tintColor: focused ? "#293272" : "#C8C8C8"
            }}
            />
            </View>
          )
      }}/>


      <Tab.Screen name="profile" component={profile}  options={{
          
          tabBarIcon: ({ focused }) => (
            <View>
            <Image
            source={images.user_icon} resizeMode="contain"

            style={{
              height:20,
              width:20,
              tintColor: focused ? "#293272" : "#C8C8C8"
            }}
            />
            </View>
          )
      }}/>
    </Tab.Navigator>
  );
}

export default tab;


