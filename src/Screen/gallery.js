// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component

import React, { useState, useEffect } from 'react';
import {
  ActivityIndicator, View, StyleSheet, Image, TouchableOpacity, Text, Platform,
  PermissionsAndroid,
} from 'react-native';
import {
  launchCamera,
  launchImageLibrary
} from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import font from './styles/font';
import { SIZES } from '../assets/styles/theme';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../services/APIConstants';
import profile from './DrawerScreens/profile';
import { file } from '@babel/types';
import Loader from './Components/Loader';
var once = false;
var video_once = false;
const Gallerys = ( props ) => {

  const [filePath, setFilePath] = useState({});
  const [base64_list,setBase64List] = useState([])
  const [file_uri,setFileUri] = useState("")
  const [videos,setVideoList] = useState([])
  const [loader_visible,setLoderVisible] = useState(false)
  // var [once,setOnce] = useState(false)
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const PendingView = () => (
    <View
      style={{
        flex: 1,
        backgroundColor: 'lightgreen',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text>Waiting</Text>
    </View>
  );


  const takePicture = async function (camera) {
    const options = { quality: 0.5, base64: true };
    const data = await camera.takePictureAsync(options);
    //  eslint-disable-next-line
    console.log(data.uri);
  };
  

  const chooseImages = () => {
    setBase64List([])
    ImagePicker.openPicker({
      mediaType: "photo",
      multiple: true
    }).then(images => {
      console.log("sas",images.length-1);
      var i =0;
      for(i=0;i<images.length;i++){
        console.log("i="+i+","+images.length);
        ImgToBase64.getBase64String(images[i].path)
        .then(base64String => {
          // console.log(base64String);
          // uploadImage(base64String)
          console.log("in="+i+","+images.length);
          base64_list.push("data:image/png;base64,"+base64String)
          
          if(i==images.length){
            console.log(base64_list.length+","+i+","+images.length);
            if(base64_list.length!=0){
              if(!once){
                // setOnce(true)
                // uploadImage()
                once = true;
                // alert(once)
                uploadImage()
              }
              
              return
            }
           
          }
        })
        .catch(err => {
          console.log(err);
        });
        
      }
      // if(base64_list.length!=0){
        
      // }else{
      //   console.log("null"+base64_list.length);
      // }
      
    }).catch(error=>{
      console.log(error);
    });
  }

  // getProfile()

  const uploadImage = async () =>{
    
    
    
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        
        setLoderVisible(true)
        fetch(ApiConstants.url + "user-upload", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            attachment: base64_list,
            type:"0"
          })
        }).then(response => response.json())
          .then(responseJson => {

            setLoderVisible(false)
            console.debug("up", responseJson)
            props.onChange(true)
            once=false
            return
          }).catch(error => {

            alert(error)
            setLoderVisible(false)
          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  const uploadVideos = async() =>{
    
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        setLoderVisible(true)
        let data = new FormData()

        videos.forEach((element, i) => {
          const newFile = {
            name: 'image',
            uri: element, 
            type: 'video/mp4'
          }
          data.append('attachment[]', newFile)
        });

        data.append('type','1');

        console.log(data.keys)

        fetch(ApiConstants.url + "user-upload", {
          method: "POST",
          headers: {
            
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + token
          },
          body:data
        }).then(response => {response.json()
          console.log(response)
        }
          
        )
          .then(responseJson => {

            setLoderVisible(false)
            console.debug("video", responseJson)
            props.onChange(true)
            video_once=false
            return
          }).catch(error => {

            alert(error)

          })
      } else {
        setLoderVisible(false)
        setDialogMessage("No Internet")
      }
    })
  }

  const chooseVideos = () => {
    setVideoList([])
    ImagePicker.openPicker({
      mediaType: "video",
      multiple:true,
    }).then(video => {
      console.log(video);
      
      
      var i =0;
      for(i=0;i<video.length;i++){
        console.log(videos.length+","+i+","+video.length);
        videos.push(video[i].path)
        if(i==video.length-1){
          console.log(videos.length+","+i+","+video.length);
          if(videos.length!=0){
            if(!video_once){
              video_once = true;
              uploadVideos()
            }
            
            return
          }
         
        }
      }
     
    }).catch(error=>{
      console.log(error);
    });
  }

  const takePhotos = () =>{
    setBase64List([])
    ImagePicker.openCamera({
      width: 400,
      height: 400,
     
    }).then(image => {
      console.log(image);
      ImgToBase64.getBase64String(image.path)
        .then(base64String => {
          
         
          base64_list.push("data:image/png;base64,"+base64String)
          
         
                
                uploadImage()
              
             
            
           
         
        })
        .catch(err => {
          console.log(err);
        });
    });
  }

  const takeVideos = () =>{
    ImagePicker.openCamera({
      mediaType: 'video',
    }).then(image => {
      console.log(image);
    });
  }

  return (
    <View style={{ flex: 1.5 }}>


      <TouchableOpacity
        activeOpacity={0.5}
        style={{
          flex: .35,
          marginTop: 10,
          width: SIZES.width,
          alignItems: 'center',
          justifyContent: 'center',
          borderBottomWidth: 1,
          borderBottomColor: "#d9d9d9"
        }}
        onPress={() => chooseImages()}>
        <Text style={[font.sizeRegular, font.blackcolor, font.bold]}>Choose Photo</Text>
      </TouchableOpacity>


      <TouchableOpacity
        activeOpacity={0.5}
        style={{
          flex: .35,
          width: SIZES.width,
          alignItems: 'center',
          justifyContent: 'center',
          borderBottomWidth: 1,
          borderBottomColor: "#d9d9d9"
        }}
        onPress={() => chooseVideos()}>
        <Text style={[font.sizeRegular, font.blackcolor, font.bold]}>Choose  video</Text>
      </TouchableOpacity>



      <TouchableOpacity
        activeOpacity={0.5}
        style={{
          flex: .35,
          marginTop: 2,
          width: SIZES.width,
          alignItems: 'center',
          justifyContent: 'center',
          borderBottomWidth: 1,
          borderBottomColor: "#d9d9d9"
        }}
        onPress={() => takePhotos()}
      >
        <Text style={[font.sizeRegular, font.blackcolor, font.bold]}>Take Photo</Text>
      </TouchableOpacity>



      <TouchableOpacity
        activeOpacity={0.5}
        style={{
          flex: .35,

          marginTop: 2,
          width: SIZES.width,
          alignItems: 'center',
          justifyContent: 'center',
          borderBottomWidth: 1
        }}
        onPress={() => takeVideos()}
      >
        <Text style={[font.sizeRegular, font.blackcolor, font.bold]}>Take Video</Text>
      </TouchableOpacity>

      {
          loader_visible == true ? <Loader /> : <View></View>
        }

    </View>
  );
}

export default Gallerys

