// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { Component, createRef, useState } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Modal,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import CountDown from 'react-native-countdown-component';
import Loader from './Components/Loader';
import font from './styles/font';
import { images, COLORS, SIZES, FONTS } from '../assets/styles/theme';
import ApiConstants from '../services/APIConstants';

const OTPscreen = ({ navigation, route }) => {

  const [dialog_visible, setDialogVisible] = useState(false);
  const [dialog_message, setDialogMessage] = useState("");
  const [loader_visible, setLoaderVisibility] = useState(false);
  const [otp, setOtp] = useState("");
  const [email, setEmail] = useState(route.params.email)
  const [type, setType] = useState(route.params.type);
  const [countdownVisible, setCountDownVisible] = useState(true);
  const [otp_validation_msg, setOtpValidationVIsible] = useState(false)

  console.log(email + type)

  const checkType = () => {
    if (type == "0") {
      verifyAccount()
    } else {
      verifyOtp()
    }
  }



  const verifyAccount = () => {

    if (otp.length != 4) {



      setOtpValidationVIsible(true)
    } else {
      setLoaderVisibility(true)
      fetch(ApiConstants.url + "verify_account", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          otp: otp
        })
      }).then(response => response.json())
        .then(responseJson => {
          console.debug("sendEmal", responseJson)
          setLoaderVisibility(false)
          const status = responseJson.status
          if (status == "success") {
            setDialogVisible(true);
            setDialogMessage(responseJson.message)
            navigation.navigate('LoginScreen')
            setLoaderVisibility(false)
          } else {
            setLoaderVisibility(false)
            Toast.show({
              title: 'Error!',
              text: responseJson.message,

              color: '#000',
              timeColor: 'red',
              backgroundColor: COLORS.textbluedark.color,
              timing: 5000,
              position: 'bottom',
            })
          }
        })
    }

  }

  const verifyOtp = () => {

    if (otp.length != 4) {

      setOtpValidationVIsible(true)
    } else {
      setLoaderVisibility(true)
      fetch(ApiConstants.url + "verify_otp", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          otp: otp
        })
      }).then(response => response.json())
        .then(responseJson => {
          console.debug(responseJson)
          setLoaderVisibility(false)
          const status = responseJson.status
          if (status == "success") {
            // setToken(responseJson.access_token)
            const token = responseJson.access_token;
            if (token.length != 0) {
              navigation.navigate('ChangePasswordScreen', { token: token })
            }

            setLoaderVisibility(false)
          } else {
            setLoaderVisibility(false)
            Toast.show({
              title: 'Error!',
              text: responseJson.message,

              color: '#000',
              timeColor: 'red',
              backgroundColor: COLORS.textbluedark.color,
              timing: 5000,
              position: 'bottom',
            })
          }
        })
    }

  }

  const resendCode = () => {
    setLoaderVisibility(true)
    fetch(ApiConstants.url + "resend_otp", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("verifyOtp", responseJson.access_token)
        setLoaderVisibility(false)
        const status = responseJson.status
        if (status == "success") {

          setCountDownVisible(true)
          setLoaderVisibility(false)
        } else {
          setLoaderVisibility(false)
          Toast.show({
            title: 'Error!',
            text: responseJson.value,

            color: '#000',
            timeColor: 'red',
            backgroundColor: COLORS.textbluedark.color,
            timing: 5000,
            position: 'bottom',
          })
        }
      })
  }


  return (
    <View style={styles.mainBody}>
      <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

      </Root>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
        }}><View style={{ alignItems: 'center', position: 'absolute', top: 0, flex: 1, left: 0, right: 0 }}>
          <Image
            source={images.large_logo_icon}
            style={{
              resizeMode: "contain",
              margin: 30,
              height: 30
            }}
          />
        </View>

        <View>
          <KeyboardAvoidingView enabled>
            <Text
              style={[
                font.bold,
                styles.HeadText
              ]}
            >
              OTP Verification
            </Text>
            <Text
              style={{
                ...FONTS.regularsizeRegular,
                color: 'white',
                marginTop: 10,
                marginBottom: 10,
                marginHorizontal: 30
              }}
            >
              We have sent on otp on your email {email}
            </Text>
            <View style={styles.SectionStyle}>
              <TextInput
                style={[styles.inputStyle, font.regular]}
                onChangeText={(text) => {
                  setOtp(text)
                  setOtpValidationVIsible(false)
                }}
                placeholder="Enter Your OTP" //dummy@abc.com
                placeholderTextColor="#747474"
                autoCapitalize="none"
                keyboardType="number-pad"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            {
              otp_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Invalid Otp</Text>
                </View> :
                <View></View>
            }
            <TouchableOpacity
              onPress={() => checkType()}
              style={styles.buttonStyle}
              activeOpacity={0.5}

            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Verfy OTP</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              {countdownVisible ? <Text

                style={[font.bold, styles.registerTextStyle]}
              >
                OTP expires in
                <CountDown

                  until={60}
                  size={30}
                  onFinish={() => setCountDownVisible(false)}
                  digitStyle={{ height: 14, width: 20, marginTop: 10 }}
                  digitTxtStyle={{ color: "#56D6BE", fontSize: 14, position: 'absolute', top: -1 }}
                  timeToShow={['M', 'S']}
                  separatorStyle={{ color: '#1CC625', fontSize: 14, top: 8, position: 'absolute' }}
                  timeLabels={{ m: null, s: '' }}
                  showSeparator
                />
                <Text style={{ color: "#56D6BE", marginRight: 5 }}>

                </Text>


              </Text> : <View></View>}
              {!countdownVisible ?
                <TouchableOpacity
                  onPress={() => resendCode()}
                >
                  <Text style={[font.bold, styles.registerTextStyle]}> Resend OTP</Text>
                </TouchableOpacity> : <View></View>}
            </View>


          </KeyboardAvoidingView>
        </View>
        <View>

        </View>
      </ScrollView>
      {
        loader_visible == true ? <Loader /> : <View></View>
      }

      <Modal
        activeOpacity={1}
        transparent={true}
        visible={dialog_visible}
      >
        <TouchableOpacity
          onPress={() => setDialogVisible(false)}
          activeOpacity={1}
          style={{
            backgroundColor: "#000000aa",
            flexDirection: 'column',
            height: SIZES.height,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <TouchableOpacity
            activeOpacity={1}
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              width: SIZES.width / 1.5,
              backgroundColor: 'white',
              minHeight: 150,
              maxHeight: 150,
              borderRadius: 10,
              shadowColor: '#4e4f72',
              shadowOpacity: 0.2,
              shadowRadius: 30,
              shadowOffset: {
                height: 0,
                width: 0,
              },
              elevation: 30,
            }}>

            <ScrollView
              contentContainerStyle={{
                alignSelf: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                height: "100%"

              }}
            >

              <Text style={{ marginTop: 10, padding: 10, textAlign: 'justify', ...FONTS.regularsizeRegular }}>
                {dialog_message}
              </Text>



            </ScrollView>
            <TouchableOpacity
              style={{
                position: 'absolute',
                top: 0,
                right: 0
              }}
              onPress={() => setDialogVisible(false)}
            >
              <Image source={images.cancel_icon} style={{
                width: 20,
                height: 20,
                tintColor: COLORS.textblue,
                alignSelf: 'flex-end',
                marginTop: 10,
                marginRight: 10
              }} />
            </TouchableOpacity>
          </TouchableOpacity>

        </TouchableOpacity>
      </Modal>

    </View>
  )
}
export default OTPscreen;


const styles = StyleSheet.create({
  colorbg: {
    backgroundColor: "#293272", borderRadius: 8, paddingHorizontal: 15, marginTop: 15
  },
  space: {
    paddingHorizontal: 15,
  },
  HeadText: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 30
  },
  bottomspace: {
    marginBottom: 0
  },
  container: {
    alignItems: 'center',
  },
  innermodal: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    borderRadius: 8
  },
  validationText: {
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',

  },
  modal: {
    backgroundColor: "rgba(0,0,0,0.5)",
    minHeight: 300,
    alignItems: 'center',
    justifyContent: "center",
    height: "100%"
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#56D6BE',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 46,
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    paddingVertical: 13,
    fontSize: 16,
    paddingRight: 15,
    paddingLeft: 15,
  },
  inputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
});
