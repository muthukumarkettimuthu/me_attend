import React, { Component } from 'react';
import {  StyleSheet, Text, Dimensions, View, TouchableOpacity,  Image } from 'react-native';

import { RNCamera } from 'react-native-camera';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';
import ImagePicker from 'react-native-image-crop-picker';


export default class Cameras extends Component {
  constructor(props) {
    super(props);
   
    this.state = {
      takingPic: false,
      cameraType: 'back',
      mirrorMode: false,
      type: 'Photo',
      recording: false,
      flash_on:false,
      flash_state:RNCamera.Constants.FlashMode.off
    }

  }

  switchCamera() {
    if (this.state.cameraType === 'back') {
      this.setState({
        cameraType: 'front',
        mirrorMode: true
      });
    } else {
      this.setState({
        cameraType: 'back',
        mirrorMode: false
      });
    }
  }

  handleFlash = () =>{
    this.setState({flash_on:!this.state.flash_on})
    let tstate = this.state.flash_state;
    if (tstate == RNCamera.Constants.FlashMode.off){
       tstate = RNCamera.Constants.FlashMode.torch;
    } else {
       tstate = RNCamera.Constants.FlashMode.off;
    }
    this.setState({flash_state:tstate})
  }

  takeVideo = async () => {
    if (!this.state.recording) {
      this.setState({ recording: true })
      if (this.camera) {

        try {
          const options = {
            quality: 2,
            videoBitrate: 8000000,
            maxDuration: 30,

          };
          const data = await this.camera.recordAsync(options);
          const file = data.uri;
          if (file.length != 0) {
            this.props.navigation.navigate("Publishscreen", { file: file,type:"0" })
          }
         
        } catch (err) {
          console.info(err);
          alert('Error', 'Failed to take picture: ' + (err.message || err));
          return;
        } finally {
          this.setState({ takingPic: false });
        }
      }
    } else {
      this.setState({ recording: false })
      this.camera.stopRecording();
    }



  }

  takePicture = async () => {
    if (this.camera && !this.state.takingPic) {

      let options = {
        quality: 0.85,
        fixOrientation: true,
        forceUpOrientation: true,
      };

      this.setState({ takingPic: true });

      try {
        const data = await this.camera.takePictureAsync(options);
        const file = data.uri;
        if (file.length != 0) {
          this.props.navigation.navigate("Publishscreen", { file: file,type:"1" })
        }

      } catch (err) {
        alert('Error', 'Failed to take picture: ' + (err.message || err));
        return;
      } finally {
        this.setState({ takingPic: false });
      }
    }
  };




  selectFlashMode(type) {
    if (type === 'auto') {
      this.setState({
        flashMode: RNCamera.Constants.FlashMode.auto,
        flash: 'auto',
        showFlashOptions: false
      })
    } else if (type === 'off') {
      this.setState({
        flashMode: RNCamera.Constants.FlashMode.off,
        flash: 'off',
        showFlashOptions: false
      })
    } else {
      this.setState({
        flashMode: RNCamera.Constants.FlashMode.on,
        flash: 'on',
        showFlashOptions: false
      })
    }
  }

  changeTab = (type) => {
    this.setState({ type: type })
  }

  chooseFile = () => {
    ImagePicker.openPicker({
      mediaType: this.state.type=='Photo'?'photo':'video',
      
    }).then(file => {
      console.log(file);
      const path = file.path
      if (path.length != 0) {
        if(this.state.type=="video"){
          this.props.navigation.navigate("Publishscreen", { file: path,type:"0" })
        }else{
          this.props.navigation.navigate("Publishscreen", { file: path,type:"1" })
        }
        
      }
    }).catch(error => {
      console.log(error);
    });
  }

  render() {
    const { cameraType } = this.state

    return (
      <View style={[styles.container, { backgroundColor: '#000000' }]}>
        <View style={{ flex: 1 }}>
          <RNCamera
            ref={(ref) => {
              this.camera = ref
            }}
            mirrorImage={false}
            style={styles.preview}
            flashMode={this.state.flash_state}
            captureAudio={false}
            type={this.state.cameraType}

            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          >
            <View style={{
              width: SIZES.width,
              height: 50,



            }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  height: 80,
                  width: SIZES.width,

                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    this.changeTab("Photo")
                  }}
                  style={{
                    alignItems: 'flex-end',
                    flex: .5
                  }}
                >
                  <Text style={
                    {
                      color: 'white',
                      ...FONTS.boldsizeMedium,
                      borderBottomWidth: 5,
                      paddingLeft: 25,
                      paddingRight: 25,
                      paddingBottom: 5,

                      borderBottomEndRadius: 60,
                      borderBottomColor: this.state.type == "Photo" ? COLORS.textgreen.color : "transparent"
                    }
                  }>
                    Photo
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    this.changeTab("Video")
                  }}
                  style={{
                    marginLeft: 30,
                    alignItems: 'flex-start',
                    flex: .5
                  }}
                >
                  <Text style={
                    {
                      color: 'white',
                      ...FONTS.boldsizeMedium,
                      borderBottomWidth: 5,
                      paddingLeft: 25,
                      paddingRight: 25,
                      paddingBottom: 5,

                      borderBottomEndRadius: 60,
                      borderBottomColor: this.state.type == "Video" ? COLORS.textgreen.color : "transparent"
                    }
                  }>
                    Video
                  </Text>
                </TouchableOpacity>


              </View>

            </View>
            <View style={{ flexDirection: "row", alignItems: "center", bottom: 20 }}>
              <TouchableOpacity onPress={() => {
                this.chooseFile()
              }

              }>
                <Image source={images.gallery_icon} style={{
                  marginRight: 5,
                  width: 30,
                  height: 30,
                  resizeMode: "contain",
                  tintColor: 'white'
                }} />
              </TouchableOpacity>
              {
                this.state.type == "Photo" ?
                  <TouchableOpacity onPress={this.takePicture.bind(this)}>
                    <View style={styles.capture} />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={this.takeVideo.bind(this)}>
                    <View style={{
                      flex: 0,
                      backgroundColor: this.state.recording ? 'red' : '#fff',
                      borderRadius: 50,
                      color: '#000',
                      padding: 35,
                      margin: 40
                    }} />
                  </TouchableOpacity>
              }
              <TouchableOpacity onPress={this.switchCamera.bind(this)}>
                <Image source={images.rotate_camera_icon_icon} style={{ marginRight: 5, width: 30, height: 30, resizeMode: "contain" }} />
              </TouchableOpacity>
             
            </View>
            <TouchableOpacity 
              activeOpacity={1}
              style={{position:'absolute',top:50,right:10}}
              onPress={this.handleFlash}>
               {
                 this.state.flash_on?<Image source={images.turn_on_icon} style={{ tintColor:"white",marginRight: 5, width: 30, height: 30, resizeMode: "contain" }} />
                :<Image source={images.turn_off_icon} style={{ tintColor:"white",marginRight: 5, width: 30, height: 30, resizeMode: "contain" }} />
                } 
              
              </TouchableOpacity>
          </RNCamera>
        </View>
      </View>
    )

  }





}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  select_tab: {
    color: 'white',
    ...FONTS.boldsizeMedium,
    borderBottomWidth: 5,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 5,

    borderBottomEndRadius: 60,
    borderBottomColor: COLORS.textgreen.color
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 50,
    color: '#000',
    padding: 35,
    margin: 40
  },
  stop: {
    flex: 0,
    backgroundColor: 'red',
    color: '#000',
    padding: 35,
    margin: 40
  }
});