export const about_react_icon = require("../../Image/about_react.png")
export const bell_icon = require("../../Image/bell.png")
export const calender_icon = require("../../Image/calender.png")
export const camera_icon = require("../../Image/camera.png")
export const circle1_icon = require("../../Image/circle1.png")
export const circle2_icon = require("../../Image/circle2.png")
export const close_icon = require("../../Image/close.png")
export const default_image1_icon = require("../../Image/default_image1.png")
export const default_image2_icon = require("../../Image/default_image2.png")
export const default_image3_icon = require("../../Image/default_image3.png")
export const default_image4_icon = require("../../Image/default_image4.png")
export const download_icon = require("../../Image/download.png")
export const ellipse_icon = require("../../Image/ellipse.png")
export const emoji_icon = require("../../Image/emoji.png")
export const filter_icon = require("../../Image/filter.png")
export const gps_icon = require("../../Image/gps.svg")
export const gps1_icon = require("../../Image/gps3.png")
export const gps2_icon = require("../../Image/gps3.png")
export const gps3_icon = require("../../Image/gps3.png")
export const gps4_icon = require("../../Image/gps4.png")
export const group_icon = require("../../Image/group.png")
export const Group25_icon = require("../../Image/Group25.svg")
export const home_icon = require("../../Image/home.png")
export const home_svg_icon = require("../../Image/home.svg")
export const large_logo_icon = require("../../Image/large_logo.png")
export const location_icon = require("../../Image/location.png")
export const logo_icon = require("../../Image/logo.png")
export const logo2_icon = require("../../Image/logo2.png")
export const menu_icon = require("../../Image/menu.png")
export const message_icon = require("../../Image/message.png")
export const notificatoin_icon = require("../../Image/notificatoin.png")
export const Path_55825_icon = require("../../Image/Path_55825.png")
export const Path_55872_icon = require("../../Image/Path_55872.png")
export const Path4_icon = require("../../Image/like.png")
export const Path55870_icon = require("../../Image/share.png")
export const people_icon = require("../../Image/people.png")
export const pin_icon = require("../../Image/pin.png")
export const prevarrow_icon = require("../../Image/prevarrow.png")
export const qr_code_icon = require("../../Image/qr_code.png")
export const rectangle_icon = require("../../Image/rectangle.png")
export const rotate_camera_icon_icon = require("../../Image/rotate_camera_icon.png")
export const search_icon = require("../../Image/search.png")
export const setting_icon = require("../../Image/settings.png")
export const small_logo_icon = require("../../Image/small_logo.png")
export const success_icon = require("../../Image/success.png")
export const tick_icon = require("../../Image/check.png")
export const user5_icon = require("../../Image/user5.png")
export const video_icon = require("../../Image/video.png")
export const user_icon = require("../../Image/user.png")
export const cancel_icon = require("../../Image/cancel.png")
export const information_icon = require("../../Image/information.png")
export const navigation_icon = require("../../Image/navigation.png")
export const all_icon = require("../../Image/all_icon.png")
export const date_pcker_icon = require("../../Image/date_pcker.png")
export const location_pin_icon = require("../../Image/location_pin.png")
export const editing_icon = require("../../Image/editing.png")
export const gallery_icon = require("../../Image/gallery.png")
export const send_icon = require("../../Image/send.png")
export const left_arrow_icon = require("../../Image/left_arrow.png")
export const right_arrow_icon = require("../../Image/right_arrow.png")
export const unselected_icon = require("../../Image/unselected.png")
export const selected_icon = require("../../Image/selected.png")
export const notification_menu_icon = require("../../Image/notification_menu.svg")
export const camera_menu_icon = require("../../Image/camera_menu.svg")
export const play_button_icon = require("../../Image/play_button.png")
export const location_map_pointer_icon = require("../../Image/location_map_pointer.png")
export const map_pointer_icon = require("../../Image/map_pointer.png")
export const default_play_button_icon = require("../../Image/default_play_button.png")
export const turn_on_icon = require("../../Image/turn_on.png")
export const turn_off_icon = require("../../Image/turn_off.png")
export const like_active_icon = require("../../Image/like_active.png")
export const message_tick_icon = require("../../Image/message_tick.png")
export const double_tick_icon = require("../../Image/double_tick.png")
export const image_attachment_icon = require("../../Image/image_attachment.png")
export const video_attachment_icon = require("../../Image/video_attachment.png")
export const document_attachment_icon = require("../../Image/document_attachment_icon.png")
export default{
    about_react_icon,
    bell_icon,
    calender_icon,
    camera_icon,
    circle1_icon,
    circle2_icon,
    close_icon,
    default_image1_icon,
    default_image2_icon,
    default_image3_icon,
    default_image4_icon,
    download_icon,
    ellipse_icon,
    emoji_icon,
    filter_icon,
    gps_icon,
    gps1_icon,
    gps2_icon,
    gps3_icon,
    gps4_icon,
    group_icon,
    Group25_icon,
    home_icon,
    home_svg_icon,
    large_logo_icon,
    location_icon,
    logo_icon,
    logo2_icon,
    menu_icon,
    message_icon,
    notificatoin_icon,
    Path_55825_icon,
    Path_55872_icon,
    Path4_icon,
    Path55870_icon,
    people_icon,
    pin_icon,
    prevarrow_icon,
    qr_code_icon,
    rectangle_icon,
    rotate_camera_icon_icon,
    search_icon,
    setting_icon,
    small_logo_icon,
    success_icon,
    tick_icon,
    user5_icon,
    video_icon,
    user_icon,
    cancel_icon,
    information_icon,
    navigation_icon,
    all_icon,
    date_pcker_icon,
    location_pin_icon,
    editing_icon,
    gallery_icon,
    send_icon,
    left_arrow_icon,
    right_arrow_icon,
    unselected_icon,
    selected_icon,
    notification_menu_icon,
    camera_menu_icon,
    play_button_icon,
    location_map_pointer_icon,
    map_pointer_icon,
    default_play_button_icon,
    turn_on_icon,
    turn_off_icon,
    like_active_icon,
    message_tick_icon,
    double_tick_icon,
    image_attachment_icon,
    video_attachment_icon,
    document_attachment_icon
}