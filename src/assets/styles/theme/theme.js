import {Dimensions} from 'react-native';
const {width,height} = Dimensions.get('window');

export const COLORS = {
    lightgray:{
        color:"#646464"
    },
    white:{
        color:"#fff"
    },
    bgcolorbtn:{
        backgroundColor:"#56D6BE"
    },
    borderbluecolor:{
        borderColor:'#56D6BE'
    },
    textgreen:{
        color:"#56D6BE"
    },
    
    textWhiteColor: {
        color: "#fff"
    },
    textblue:{
        color:"#293272"
    },
    textbluedark:{
        color:"#222857"
    },
    blackcolor:{
        color:"#333333",
    },
    graycolor:{
        color:"#747474"
    },
    lightgraycolor:{
        color: "#cbcbcb"
    },
    backbg:{
        backgroundColor:"#EEFBF9"
    },
    lightbg:{
        backgroundColor:"#EEFBF8"
    },
    opacityrange:{
        opacity:0.7

    },

};

export const SIZES = {
    //Global sizes
    topspaceing:{
        marginTop:5
    },
    borderradiustyle:{
        borderRadius:13.5,
        opacity:0.8
    },
    textSpacing: {
        letterSpacing: 1,
    },
    linevertical:{
        lineHeight:19
    },
    width,
    height

};

export const FONTS = {
    

    regularsizeLittlesmall:{
        fontSize:6,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeVerySmall: {
        fontSize: 8,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeSmall: {
        fontSize: 10,
        fontFamily:"Oxygen-Regular"
    },
    regularfont11: {
        fontSize: 11,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeVeryRegular: {
        fontSize: 12,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeLittleRegular:{
        fontSize: 13,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeRegular: {
        fontSize: 14,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeMedium: {
        fontSize: 16,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeLittleLarger:{
        fontSize: 17,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeLarge: {
        fontSize: 18,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeExtraLarge: {
        fontSize: 20,
        fontFamily:"Oxygen-Regular"
    },
    regularsizeDoubleLarge: {
        fontSize: 25,
        fontFamily:"Oxygen-Regular"
    },

    semiboldsizeLittlesmall:{
        fontSize:6,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeVerySmall: {
        fontSize: 8,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeSmall: {
        fontSize: 10,
        fontFamily: "Oxygen-Light"
    },
    semiboldfont11: {
        fontSize: 11,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeVeryRegular: {
        fontSize: 12,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeLittleRegular:{
        fontSize: 13,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeRegular: {
        fontSize: 14,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeMedium: {
        fontSize: 16,
        fontFamily:"Oxygen-Light"
    },
    semiboldsizeLittleLarger:{
        fontSize: 17,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeLarge: {
        fontSize: 18,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeExtraLarge: {
        fontSize: 20,
        fontFamily: "Oxygen-Light"
    },
    semiboldsizeDoubleLarge: {
        fontSize: 25,
        fontFamily: "Oxygen-Light"
    },
    boldsizeLittlesmall:{
        fontSize:6,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeVerySmall: {
        fontSize: 8,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeSmall: {
        fontSize: 10,
        fontFamily: "Oxygen-Bold"
    },
    boldfont11: {
        fontSize: 11,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeVeryRegular: {
        fontSize: 12,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeLittleRegular:{
        fontSize: 13,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeRegular: {
        fontSize: 14,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeMedium: {
        fontSize: 16,
        fontFamily:"Oxygen-Bold"
    },
    boldsizeLittleLarger:{
        fontSize: 17,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeLarge: {
        fontSize: 18,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeExtraLarge: {
        fontSize: 20,
        fontFamily: "Oxygen-Bold"
    },
    boldsizeDoubleLarge: {
        fontSize: 25,
        fontFamily: "Oxygen-Bold"
    },

    
}

const appTheme = {COLORS,SIZES,FONTS}

export default appTheme